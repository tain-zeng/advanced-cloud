FROM openjdk:17-jre
MAINTAINER advanced_cloud

# 设置时区
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && \
    echo 'Asia/Shanghai' >/etc/timezone \

RUN mvn clean install -DskipTest

COPY ./xxx.jar  /home/goodsence-dev/xxx.jar

EXPOSE 28090

ENTRYPOINT ["java", "-server", "-Xms512M", "-Xmx512M", "-Djava.security.egd=file:/dev/./urandom", "-Dfile.encoding=UTF-8", "-XX:+HeapDumpOnOutOfMemoryError", "-jar", "/home/goodsence-dev/xxx.jar" ]
