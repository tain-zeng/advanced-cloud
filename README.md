# advanced-cloud---个人最佳实践的累积

### 介绍
本项目旨在用本人的最佳实践，去开发想开发的需求。 \
每当本人软件开发认知刷新时，可能会完全重构此项目，以便在实际工作采用最佳设计以减少重构的工作和风险.
### 软件架构
1. 系统架构图：(经过无数次重构, 系统架构已经变样了没空画架构图和部署图 2024.12.25) \
    ![img.png](document/系统架构图.png)
2. 模块说明：
   - advance-cloud
     - [api](framework%2Fapi)-------------------------------------------------服务调用接口
     - [framework](framework)---------------------------------------框架
       - [common](framework%2Fcommon)-----------------------------------通用模块
       - [security](framework%2Fsecurity)-------------------------------------安全模块
     - [gateway](gateway)-------------------------------------------应用网关
     - [services](services)-------------------------------------------业务服务
       - [authentication-server](services%2Fauthentication-server)--------------------------------认证服务器
       - [message-server](services%2Fmessage-server)------------------------------消息推送组件(基于netty, webflux)
       - [oss](services%2Foss)-------------------------------对象存储服务

### 导航
1. SpringSecurity猥琐扩展一次性密码登录请看: \
[OncePasswordStore.java](framework%2Fsecurity%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fsecurity%2Foncepassword%2FOncePasswordStore.java) \
[OncePasswordSender.java](framework%2Fsecurity%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fsecurity%2Foncepassword%2FOncePasswordSender.java) \
[OncePasswordEncoderProxy.java](framework%2Fsecurity%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fsecurity%2Foncepassword%2FOncePasswordEncoderProxy.java)
2. SpringSecurity暴力扩展一次性密码登录请看: \
[OncePasswordAuthenticationFilter.java](framework%2Fsecurity%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fsecurity%2Foncepassword%2FOncePasswordAuthenticationFilter.java) \
[OncePasswordAuthenticationProvider.java](framework%2Fsecurity%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fsecurity%2Foncepassword%2FOncePasswordAuthenticationProvider.java) \
[OncePasswordAuthenticationToken.java](framework%2Fsecurity%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fsecurity%2Foncepassword%2FOncePasswordAuthenticationToken.java)
3. Oauth2暴力扩展一次性密码模式请看: \
[OncePasswordAuthenticationConverter.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FOncePasswordAuthenticationConverter.java) \
[OncePasswordAuthenticationProvider.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FOncePasswordAuthenticationProvider.java) \
[OncePasswordAuthenticationToken.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FOncePasswordAuthenticationToken.java)
4. Oauth2暴力扩展密码模式情请看: \
[PasswordAuthenticationConverter.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FPasswordAuthenticationConverter.java) \
[PasswordAuthenticationProvider.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FPasswordAuthenticationProvider.java) \
[PasswordAuthenticationToken.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FPasswordAuthenticationToken.java)
5. Oauth2暴力扩展认证信息redis存储请看: \
[RedisOAuth2AuthorizationService.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FRedisOAuth2AuthorizationService.java) 
6. Oauth2暴力扩展用户权限请看: \
[CustomOpaqueTokenIntrospector.java](framework%2Fsecurity%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fsecurity%2FCustomOpaqueTokenIntrospector.java) \
[UserAuthorityOAuth2TokenCustomizer.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FUserAuthorityOAuth2TokenCustomizer.java) \
[UserDataAuthorityOAuth2TokenCustomizer.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FUserDataAuthorityOAuth2TokenCustomizer.java)
7. 扩展数据权限请看: \
[DataAuthInterceptors.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FDataAuthInterceptors.java) \
[UserDataAuthorityOAuth2TokenCustomizer.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FUserDataAuthorityOAuth2TokenCustomizer.java)
8. 基于Netty, WebFlux, Rabbitmq的高可用消息推送组件请看: \
[message-server](services%2Fmessage-server)
9. 组合型责任链示例请看: \
[CompositeChainExceptionHandler.java](framework%2Fweb%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fweb%2FCompositeChainExceptionHandler.java) \
[CompositeOAuth2TokenCustomizer.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FCompositeOAuth2TokenCustomizer.java)
10. 模板方法示例请看: \
[AbstractCustomAuthenticationProvider.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FAbstractCustomAuthenticationProvider.java)
11. 建造者模式示例请看: \
[SecurityUserBuilder.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fcore%2FSecurityUserBuilder.java)
12. 代理模式示例请看: \
[OncePasswordEncoderProxy.java](framework%2Fsecurity%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fsecurity%2Foncepassword%2FOncePasswordEncoderProxy.java)
13. 路由数据源(运行时切换数据源)示例请看: \
[RoutingDataSource.java](framework%2Fmybatis%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fmybatis%2FRoutingDataSource.java)
14. 新时代包办婚姻需求构想请切换到modern-arranged-marriage分支，在documents目录下.
15. ListableBeanFactory的使用示例请看: \
[DefaultControllerAdvice.java](framework%2Fweb%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fweb%2FDefaultControllerAdvice.java) \
[AuthSecurityFilterChainConfig.java](services%2Fauthentication-server%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Fauthenticationserver%2Fconfig%2FAuthSecurityFilterChainConfig.java)
16. 自定义池化请看: \
[SessionPooledFactory.java](services%2Foss%2Fsrc%2Fmain%2Fjava%2Forg%2Fgoodsence%2Foss%2Fcore%2FSessionPooledFactory.java)