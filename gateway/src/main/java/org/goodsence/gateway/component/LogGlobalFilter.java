package org.goodsence.gateway.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author zengty
 * @apiNote 网关日志过滤器
 * @date 2024/12/13
 * @project advanced-cloud
 */
@Component
public class LogGlobalFilter implements GlobalFilter {

    private static final Logger log = LoggerFactory.getLogger(LogGlobalFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        String clientIp = fetchClientIp(request);
        log.info("客户端IP: {}", clientIp);
        URI uri = request.getURI();
        log.info("URI: {}", uri);
        HttpMethod method = request.getMethod();
        log.info("请求方式: {}", method);
        HttpHeaders httpHeaders = request.getHeaders();
        log.info("请求头: {}", httpHeaders);
        MultiValueMap<String, String> queryParams = request.getQueryParams();
        log.info("请求参数: {}", queryParams);
        String body = fetchBody(request);
        log.info("请求体: {}", body);
        return chain.filter(exchange);
    }

    private String fetchBody(ServerHttpRequest request) {
        MediaType contentType = request.getHeaders().getContentType();
        if (!MediaType.APPLICATION_JSON.isCompatibleWith(contentType) &&
                !MediaType.APPLICATION_FORM_URLENCODED.isCompatibleWith(contentType)) {
            return "非JSON";
        }
        AtomicReference<String> bodyString = new AtomicReference<>();
        Flux<DataBuffer> bodyFlux = request.getBody();
        DataBufferUtils.join(bodyFlux)
                .flatMap(dataBuffer -> {
                    byte[] bytes = new byte[dataBuffer.readableByteCount()];
                    dataBuffer.read(bytes);
                    DataBufferUtils.release(dataBuffer);
                    bodyString.set(new String(bytes, StandardCharsets.UTF_8));
                    return Mono.empty();
                });
        return bodyString.get();
    }

    private String fetchClientIp(ServerHttpRequest request) {
        InetSocketAddress remoteAddress = request.getRemoteAddress();
        if (remoteAddress != null) {
            InetAddress address = remoteAddress.getAddress();
            if (address != null) {
                return address.getHostAddress();
            }
        }
        return "unknown";
    }
}
