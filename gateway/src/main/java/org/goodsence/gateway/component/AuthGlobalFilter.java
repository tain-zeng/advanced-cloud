package org.goodsence.gateway.component;

import org.goodsence.gateway.core.AuthValidator;
import org.goodsence.gateway.core.ValidateArgs;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

//@Component
public class AuthGlobalFilter implements GlobalFilter {

    private final AuthValidator authValidator;

    public AuthGlobalFilter(AuthValidator authValidator) {
        this.authValidator = authValidator;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        if (hasPermitted(request)){
            return chain.filter(exchange);
        }
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        return response.setComplete();
    }

    private boolean hasPermitted(ServerHttpRequest request) {
        List<String> authorizations = request.getHeaders().get("Authorization");
        if (authorizations == null){
            return false;
        }
        String authorization = authorizations.get(0);
        if(!StringUtils.hasText(authorization)){
            return false;
        }
        String path = request.getURI().getPath();
        ValidateArgs validateArgs = new ValidateArgs();
        validateArgs.setPath(path);
        validateArgs.setToken(authorization);
        return authValidator.validate(validateArgs);
    }
}
