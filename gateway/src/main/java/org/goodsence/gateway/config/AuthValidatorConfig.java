package org.goodsence.gateway.config;

import org.goodsence.gateway.core.AuthValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/13
 * @project advanced-cloud
 */
@Configuration
public class AuthValidatorConfig {

    @Bean
    public AuthValidator authValidator() {
        return validateArgs -> true;
    }
}
