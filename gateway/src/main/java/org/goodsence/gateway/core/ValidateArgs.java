package org.goodsence.gateway.core;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/13
 * @project advanced-cloud
 */
public class ValidateArgs {
    private String path;
    private String token;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}


