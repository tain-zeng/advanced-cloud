package org.goodsence.gateway.core;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/13
 * @project advanced-cloud
 */
public interface AuthValidator {

    boolean validate(ValidateArgs validateArgs);
}
