package org.goodsence.gateway;

import org.goodsence.common.EnvironmentPrinter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author zty
 * @apiNote
 * @project_name advanced-cloud
 * @user tain
 * @create_at 2023/6/13 23:36
 * @create_vio IntelliJ IDEA
 */
@RefreshScope
@ComponentScan(basePackages = {
        "org.goodsence.gateway.config",
        "org.goodsence.gateway.component"
})
@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(GatewayApplication.class, args);
        EnvironmentPrinter.printInfo(context);
    }
}
