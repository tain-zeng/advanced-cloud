package org.goodsence.security.callback;

import java.net.URI;
import java.util.Map;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 回调处理
 * @date 2024-02-06 19:07:58
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public interface CallBackService {

    Map<String, Object> handleCallBack(String state, String code);

    /**
     * 构造授权地址 类似于
     * "http://localhost:18010/oauth2/authorize?client-id=oauth2&redirect_uri=https://www.baidu.com&state=1&response_type=code&scope=user_authority"
     * @param s 客户端id
     * @return 客户端密钥
     */
    URI constructAuthorizationUrl(String s);

    URI constructLogoutUrl();
}
