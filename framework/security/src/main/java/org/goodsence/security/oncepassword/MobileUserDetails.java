package org.goodsence.security.oncepassword;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * <p>
 * created at <b> 2024-04-02 11:10:06 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 加密手机号登录用户
 */
@Deprecated
public interface MobileUserDetails extends UserDetails {

    String getEncryptedMobile();
}
