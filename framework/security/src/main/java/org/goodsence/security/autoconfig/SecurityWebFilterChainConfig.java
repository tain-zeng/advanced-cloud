package org.goodsence.security.autoconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.server.resource.introspection.ReactiveOpaqueTokenIntrospector;
import org.springframework.security.web.server.SecurityWebFilterChain;

import java.util.Set;

@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@EnableWebFluxSecurity
public class SecurityWebFilterChainConfig {

    private final ReactiveOpaqueTokenIntrospector reactiveOpaqueTokenIntrospector;

    private final ResourceProperties resourceProperties;

    @Autowired
    public SecurityWebFilterChainConfig(ReactiveOpaqueTokenIntrospector reactiveOpaqueTokenIntrospector, ResourceProperties resourceProperties) {
        this.reactiveOpaqueTokenIntrospector = reactiveOpaqueTokenIntrospector;
        this.resourceProperties = resourceProperties;
    }

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        http.oauth2ResourceServer(oAuth2ResourceServerSpec ->
                        oAuth2ResourceServerSpec.opaqueToken(opaqueTokenSpec ->
                                opaqueTokenSpec.introspector(reactiveOpaqueTokenIntrospector)))
            .authorizeExchange(exchanges -> {
                Set<String> permits = resourceProperties.getPermits();
                if (!permits.isEmpty()) {
                    exchanges.pathMatchers(permits.toArray(new String[0])).permitAll();
                }
                Set<String> denies = resourceProperties.getDenies();
                if (!denies.isEmpty()) {
                    exchanges.pathMatchers(denies.toArray(new String[0])).denyAll();
                }
                exchanges.anyExchange().authenticated();
            });
        http.csrf(ServerHttpSecurity.CsrfSpec::disable);
        return http.build();
    }
}