package org.goodsence.security.oncepassword;

import org.goodsence.security.AccountType;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author zengty
 * @apiNote 一次性密码猥琐校验器，之所以是说猥琐是因为它名字于密码加密器相背，这是糟糕的命名 <br>
 * 该类用的是代理模式，其它方法直接使用被代理对象的方法，matches方法请看方法注释。
 * @date 2025/1/3
 * @project advanced-cloud
 */
public class OncePasswordEncoderProxy implements PasswordEncoder {

    private final PasswordEncoder passwordEncoder;

    private final OncePasswordStore oncePasswordStore;

    public OncePasswordEncoderProxy(PasswordEncoder passwordEncoder, OncePasswordStore oncePasswordStore) {
        this.passwordEncoder = passwordEncoder;
        this.oncePasswordStore = oncePasswordStore;
    }

    @Override
    public String encode(CharSequence rawPassword) {
        return passwordEncoder.encode(rawPassword);
    }

    /**
     * 校验密码， encodedPassword用作查找一次性密码的key，如果一次性密码存储器中包含用户登录输入的密码(rawPassword)<br>
     * 那么核验通过直接返回成功，否则采用被代理对象的密码校验。<br>
     * 为什么可以用encodedPassword作为一次性密码存储器的key？ 因为 {@link OncePasswordSender} 的实现类<br>
     * 在发送消息时可以先根据手机号或邮箱等找到用户信息，然后以其加密密码作为key，验证码作为value存入{@link OncePasswordStore}中。
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        String oncePassword = rawPassword.toString();
        String oncePasswordFromStore = oncePasswordStore.getOncePassword(AccountType.EMPTY, encodedPassword);
        oncePasswordStore.remove(AccountType.EMPTY, encodedPassword);
        if (!oncePassword.equals(oncePasswordFromStore)) {
            return passwordEncoder.matches(rawPassword, encodedPassword);
        }
        return true;
    }

    @Override
    public boolean upgradeEncoding(String encodedPassword) {
        return passwordEncoder.upgradeEncoding(encodedPassword);
    }

}
