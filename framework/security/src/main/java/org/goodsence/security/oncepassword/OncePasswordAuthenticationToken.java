package org.goodsence.security.oncepassword;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

import java.util.Collection;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/18
 * @project advanced-cloud
 */
@ToString
@EqualsAndHashCode(callSuper = false)
public class OncePasswordAuthenticationToken extends AbstractAuthenticationToken {

    private final Object principal;

    private Object credentials;

    private final String oncePasswordType;

    public OncePasswordAuthenticationToken(Object principal, Object credentials, String oncePasswordType) {
        super(null);
        this.principal = principal;
        this.credentials = credentials;
        this.oncePasswordType = oncePasswordType;
        this.setAuthenticated(false);
    }

    public OncePasswordAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String oncePasswordType) {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        this.oncePasswordType = oncePasswordType;
        super.setAuthenticated(true);
    }

    public static OncePasswordAuthenticationToken unauthenticated(Object principal, Object credentials, String oncePasswordType) {
        return new OncePasswordAuthenticationToken(principal, credentials, oncePasswordType);
    }

    public static OncePasswordAuthenticationToken authenticated(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String oncePasswordType) {
        return new OncePasswordAuthenticationToken(principal, credentials, authorities, oncePasswordType);
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    public String getOncePasswordType(){
        return oncePasswordType;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        Assert.isTrue(!isAuthenticated, "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        super.setAuthenticated(false);
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        this.credentials = null;
    }
}
