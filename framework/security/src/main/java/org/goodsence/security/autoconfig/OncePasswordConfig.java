package org.goodsence.security.autoconfig;

import org.goodsence.common.KvOperations;
import org.goodsence.security.FlexibleUserDetailsService;
import org.goodsence.security.oncepassword.*;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author zengty
 * @apiNote
 * @date 2025/1/3
 * @project advanced-cloud
 */
public class OncePasswordConfig {

    @Bean
    public OncePasswordStore oncePasswordStore(KvOperations kvOperations) {
        return new DefaultOncePasswordStore(kvOperations);
    }

    @Bean
    public OncePasswordSender oncePasswordSender(OncePasswordStore oncePasswordStore, FlexibleUserDetailsService flexibleUserDetailsService) {
        return new DefaultOncePasswordSender(oncePasswordStore, flexibleUserDetailsService);
    }

    @Bean
    public PasswordEncoder passwordEncoder(OncePasswordStore oncePasswordStore) {
        return new OncePasswordEncoderProxy(new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2B),
                oncePasswordStore);
    }
}
