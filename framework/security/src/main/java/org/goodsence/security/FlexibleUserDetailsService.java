package org.goodsence.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collection;

/**
 * <p>
 * created at <b> 2024-04-04 01:18:09 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
public interface FlexibleUserDetailsService extends UserDetailsService {

    /**
     *  这里覆盖loadUsername方法，因为用户输入的不一定是用户名，可能是邮箱，可能是手机号等等 <br>
     *  所以根据实现类 是否支持根据邮箱或者手机号查找用户，如果不能查找就换一种方式。
     * @param username 用户登录账号
     */
    @Override
    default UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        for (AccountType accountType : getSupportedAccountTypes()){
            try {
                UserDetails userDetails = loadUserByAccount(accountType, username);
                if (userDetails != null) {
                    return userDetails;
                }
            }catch (UsernameNotFoundException ignored){}
        }
        throw new UsernameNotFoundException(username);
    }

    /**
     * @param accountType 账户类型用于子类根据类型去把用户信息找出来
     * @param account 账号
     */
    UserDetails loadUserByAccount(AccountType accountType, String account) throws UsernameNotFoundException;

    /**
     * @return 支持的账户类型
     */
    Collection<AccountType> getSupportedAccountTypes();
}
