package org.goodsence.security.oncepassword;

import org.goodsence.security.AccountType;

/**
 * @author zengty
 * @apiNote 一次性密码验证器
 * @date 2024/12/31
 * @project advanced-cloud
 */
public interface OncePasswordStore {

    void save(AccountType accountType, String account, String oncePassword);

    void remove(AccountType accountType, String account);

    String getOncePassword(AccountType accountType, String account);
}
