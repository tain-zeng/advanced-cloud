package org.goodsence.security.autoconfig;

import org.goodsence.security.AccessDeniedExceptionHandler;
import org.goodsence.security.AuthenticationExceptionHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/26
 * @project advanced-cloud
 */
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Configuration
@Import({AccessDeniedExceptionHandler.class, AuthenticationExceptionHandler.class})
public class ExceptionHandlerConfig {
}
