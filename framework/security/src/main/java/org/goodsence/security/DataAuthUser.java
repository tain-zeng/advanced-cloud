package org.goodsence.security;

import java.util.Collection;

public interface DataAuthUser {

    Collection<String> getDataAuthorities();
}
