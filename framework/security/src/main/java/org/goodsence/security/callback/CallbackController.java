package org.goodsence.security.callback;

import org.goodsence.security.SecurityMappingAddr;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Map;

/**
 * 处理回调<br>
 * 访问网站->无权限->跳转到认证服务器授权地址->登录->登录成功->回调到此<br>
 * 处理回调:使用授权码获取令牌->存储令牌->返回令牌
 */
public class CallbackController {

    private final CallBackService callBackService;

    public CallbackController(CallBackService callBackService) {
        this.callBackService = callBackService;
    }

    /**
     * @return 授权地址
     * @param s 客户端id
     * @deprecated 前端不能再用
     */
    @Deprecated
    @GetMapping("/oauth-addr/{s}")
    public String oauthAddr(@PathVariable String s) {
        URI authorizationUrl = callBackService.constructAuthorizationUrl(s);
        return authorizationUrl.toString();
    }

    /**
     * 处理回调
     * @param code 授权码
     * @param state 状态码，防止csrf攻击。
     * @return {@link org.springframework.security.oauth2.common.OAuth2AccessToken}
     */
    @GetMapping(SecurityMappingAddr.CALLBACK)
    public Map<String, Object> callback(@RequestParam(OAuth2ParameterNames.CODE) String code,
                                                        @RequestParam(OAuth2ParameterNames.STATE) String state) {
        return callBackService.handleCallBack(state, code);
    }

    /**
     * @return 登出地址
     * @deprecated 前端不能再用
     */
    @Deprecated
    @GetMapping(SecurityMappingAddr.LOGOUT_ADDR)
    public URI logoutAddr(){
        return callBackService.constructLogoutUrl();
    }
}
