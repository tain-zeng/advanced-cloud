package org.goodsence.security;

import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2TokenIntrospectionClaimNames;
import org.springframework.security.oauth2.server.resource.introspection.BadOpaqueTokenException;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionException;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestOperations;

import java.time.Instant;
import java.util.*;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 伪公开客户端token内省器
 * @date 2024-02-06 19:43:31
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public abstract class CustomOpaqueTokenIntrospector implements OpaqueTokenIntrospector {

    private final String introspectionUri;

    private static final String AUTHORITY_PREFIX = "SCOPE_";

    private final RestOperations restOperations;

    /**
     * Creates a {@code OpaqueTokenAuthenticationProvider} with the provided parameters
     */
    protected CustomOpaqueTokenIntrospector(RestOperations restOperations, OAuth2ResourceServerProperties properties) {
        this.restOperations = restOperations;
        OAuth2ResourceServerProperties.Opaquetoken opaquetoken = properties.getOpaquetoken();
        Assert.notNull(opaquetoken.getIntrospectionUri(), "introspectionUri cannot be null");
        introspectionUri = opaquetoken.getIntrospectionUri();
    }

    @Cacheable(value = "OAuth2AuthenticatedPrincipal", key = "#token")
    @Override
    public OAuth2AuthenticatedPrincipal introspect(String token) {
        ResponseEntity<Map> exchange = getMapResponseEntity(token);
        if (!exchange.getStatusCode().is2xxSuccessful()) {
            throw new OAuth2IntrospectionException("内省失败");
        }
        Map map = exchange.getBody();
        if (map == null) {
            throw new OAuth2IntrospectionException("内省失败");
        }
        Map<String, Object> claims = adaptToNimbusResponse(map);
        return convertClaimsSet(claims);
    }

    private ResponseEntity<Map> getMapResponseEntity(String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        embedAdditionalAuth(httpHeaders);
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("token", token);
        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(body, httpHeaders);
        ResponseEntity<Map> exchange;
        try {
            exchange = restOperations.exchange(introspectionUri, HttpMethod.POST, httpEntity, Map.class);
        }catch (Exception e) {
            if (e instanceof HttpClientErrorException httpClientErrorException
                    && httpClientErrorException.getStatusCode().is4xxClientError()){
                    throw new BadOpaqueTokenException("凭证无效");
                }
            throw new OAuth2IntrospectionException("内省失败", e);
        }
        if (exchange.getStatusCode().is4xxClientError()) {
            throw new BadOpaqueTokenException("凭证无效");
        }
        return exchange;
    }

    protected abstract void embedAdditionalAuth(HttpHeaders httpHeaders);

    private static Map<String, Object> adaptToNimbusResponse(Map map) {
        Map<String, Object> claims = map;
        // relying solely on the authorization server to validate this token (not checking
        // 'exp', for example)
        if (claims == null) {
            return Collections.emptyMap();
        }
        boolean active = (boolean) claims.compute(OAuth2TokenIntrospectionClaimNames.ACTIVE, (k, v) -> {
            if (v instanceof String s) {
                return Boolean.parseBoolean(s);
            }
            if (v instanceof Boolean) {
                return v;
            }
            return false;
        });
        if (!active) {
            throw new BadOpaqueTokenException("Provided token isn't active");
        }
        return claims;
    }

    private static OAuth2AuthenticatedPrincipal convertClaimsSet(Map<String, Object> claims) {
        claims.computeIfPresent(OAuth2TokenIntrospectionClaimNames.AUD, (k, v) -> {
            if (v instanceof String) {
                return Collections.singletonList(v);
            }
            return v;
        });
        claims.computeIfPresent(OAuth2TokenIntrospectionClaimNames.CLIENT_ID, (k, v) -> v.toString());
        claims.computeIfPresent(OAuth2TokenIntrospectionClaimNames.EXP,
                (k, v) -> Instant.ofEpochSecond(((Number) v).longValue()));
        claims.computeIfPresent(OAuth2TokenIntrospectionClaimNames.IAT,
                (k, v) -> Instant.ofEpochSecond(((Number) v).longValue()));
        claims.computeIfPresent(OAuth2TokenIntrospectionClaimNames.ISS, (k, v) -> v.toString());
        claims.computeIfPresent(OAuth2TokenIntrospectionClaimNames.NBF,
                (k, v) -> Instant.ofEpochSecond(((Number) v).longValue()));

        Collection<GrantedAuthority> authorities = new ArrayList<>();

        //scope权限
        claims.computeIfPresent(OAuth2TokenIntrospectionClaimNames.SCOPE, (k, v) -> {
            if (v instanceof String s) {
                Collection<String> scopes = Arrays.asList(s.split(" "));
                scopes.forEach(s1 -> authorities.add(new SimpleGrantedAuthority(AUTHORITY_PREFIX + s1)));
                return scopes;
            }
            return v;
        });

        //新的版本又不用手动嵌入用户权限了？要不然403
        //嵌入用户权限
        Object o1 = claims.get(OAuth2TokenIntrospectionClaimNames.SCOPE);
        if (o1 instanceof Collection<?> scopes && scopes.contains(CustomScopes.USER_AUTHORITY)){//scope 包含自定义用户权限
            Object o2 = claims.get(CustomOAuth2TokenClaimNames.AUTHORITIES);
            if(o2 instanceof Collection<?> stringAuthorities){
                stringAuthorities.forEach(e -> authorities.add(new SimpleGrantedAuthority(Objects.toString(e))));
            }
        }
        return new OAuth2IntrospectionAuthenticatedPrincipal(claims, authorities);
    }

}
