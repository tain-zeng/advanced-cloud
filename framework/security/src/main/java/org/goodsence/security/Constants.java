package org.goodsence.security;

import org.goodsence.common.CanNotInstantiateReason;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/26
 * @project advanced-cloud
 */
public final class Constants {

    public static final int RESOURCE_SERVER_FILTER_ORDER = 100;

    public static final String FAKE_PUBLIC_CLIENT_AUTHORIZATION = "Fake-Public-Client-Authorization";

    private Constants() {
        throw new IllegalStateException(CanNotInstantiateReason.ENUM_CLASS.descriptions());
    }

}
