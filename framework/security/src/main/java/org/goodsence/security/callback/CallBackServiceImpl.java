package org.goodsence.security.callback;

import org.goodsence.common.BusinessException;
import org.goodsence.common.KvOperations;
import org.goodsence.common.InMemoryKvOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationResponseType;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestOperations;

import java.net.URI;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 回调处理
 * @date 2024-02-06 19:08:21
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@EnableConfigurationProperties(OAuth2ClientProperties.class)
public class CallBackServiceImpl implements CallBackService {

    private static final Logger log = LoggerFactory.getLogger(CallBackServiceImpl.class);

    private final KvOperations cache = new InMemoryKvOperations();

    private static final String FORMAT = "%s=%s";

    private static final String FORMAT_1 = "?" + FORMAT;

    private static final String FORMAT_2 = "&" + FORMAT;

    /**
     * 登出地址
     */
    @Value("${spring.security.oauth2.logout-uri}")
    private String logoutUri;

    /**
     * 认证中心信息 认证提供者
     */
    private final OAuth2ClientProperties.Provider provider;

    /**
     * 应用自身在认证中心的注册信息
     */
    private final OAuth2ClientProperties.Registration registration;

    private final RestOperations restOperations;

    @Autowired
    public CallBackServiceImpl(OAuth2ClientProperties oAuth2ClientProperties,RestOperations restOperations) {
        Optional<OAuth2ClientProperties.Provider> any = oAuth2ClientProperties.getProvider()
                .values()
                .stream()
                .filter(Objects::nonNull)
                .findAny();
        if (any.isEmpty()){
            throw new IllegalStateException("没有配置指向认证中心的授权信息!");
        }
        provider = any.get();
        Optional<OAuth2ClientProperties.Registration> any1 = oAuth2ClientProperties.getRegistration()
                .values()
                .stream()
                .filter(Objects::nonNull)
                .findAny();
        if (any1.isEmpty()){
            throw new IllegalStateException("没有配置在认证中心的注册信息!");
        }
        registration = any1.get();
        this.restOperations = restOperations;
    }

    @Override
    public Map<String, Object> handleCallBack(String state, String code) {
        log.info("{} is {}", OAuth2ParameterNames.STATE, state);
        if (!cache.contains(state)){
            throw new BusinessException("请重新获取授权码！");
        }
        cache.remove(state);
        log.info("{} is {}", OAuth2ParameterNames.CODE, code);
        return getToken(code);
    }

    @Override
    public URI constructAuthorizationUrl(String s) {
        if (!Objects.equals(registration.getClientId(), s)) {
            throw new BusinessException(String.format("此应用配置的客户端不包含%s", s));
        }
        cache.put(s, s, 60);
        String scopesStr = registration.getScope()
                .stream()
                .filter(StringUtils::hasText)
                .map(str -> str + "%20")
                .collect(Collectors.joining());
        String scope = scopesStr.substring(0, scopesStr.lastIndexOf("%20"));
        URI redirectUri = URI.create(registration.getRedirectUri());
        String uri = provider.getAuthorizationUri()
                + String.format(FORMAT_1, OAuth2ParameterNames.CLIENT_ID, registration.getClientId())
                + String.format(FORMAT_2, OAuth2ParameterNames.REDIRECT_URI, redirectUri)
                + String.format(FORMAT_2, OAuth2ParameterNames.STATE, s)
                + String.format(FORMAT_2, OAuth2ParameterNames.RESPONSE_TYPE, OAuth2AuthorizationResponseType.CODE.getValue())
                + String.format(FORMAT_2, OAuth2ParameterNames.SCOPE, scope);
        return URI.create(uri);
    }

    private Map<String, Object> getToken(String code) {
        String authorizationGrantType = registration.getAuthorizationGrantType();
        if (authorizationGrantType == null){
            throw new IllegalStateException("注册信息未配置授权模式!");
        }
        if (!authorizationGrantType.contains(AuthorizationGrantType.AUTHORIZATION_CODE.getValue())){
            throw new BusinessException("客户端不包含授权码模式");
        }
        final MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add(OAuth2ParameterNames.CLIENT_ID, registration.getClientId());
        params.add(OAuth2ParameterNames.REDIRECT_URI, registration.getRedirectUri());
        params.add(OAuth2ParameterNames.CLIENT_SECRET, registration.getClientSecret());
        params.add(OAuth2ParameterNames.CODE, code);
        params.add(OAuth2ParameterNames.GRANT_TYPE, AuthorizationGrantType.AUTHORIZATION_CODE.getValue());
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth(registration.getClientId(), registration.getClientSecret());
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(params, headers);
        ResponseEntity<Object> response = restOperations.exchange(provider.getTokenUri(), HttpMethod.POST,
                httpEntity, Object.class);
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() instanceof Map<?, ?> mapObj){
            return (Map<String, Object>) mapObj;
        }
        throw new IllegalStateException("获取token失败");
    }

    @Override
    public URI constructLogoutUrl() {
        return URI.create(logoutUri);
    }
}
