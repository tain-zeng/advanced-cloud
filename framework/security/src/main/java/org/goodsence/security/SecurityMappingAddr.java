package org.goodsence.security;

import org.goodsence.common.CanNotInstantiateReason;

/**
 * @author zty
 * @apiNote Security模块的相关地址
 * @user nav1c
 * @date 2023/12/1 11:59
 * @project advanced-cloud
 */
public final class SecurityMappingAddr {

    /**
     * 获取授权地址
     */
    public static final String OAUTH_ADDR = "/oauth-addr";

    /**
     * 回调地址
     */
    public static final String CALLBACK = "/callback";

    /**
     * 获取登出地址
     */
    public static final String LOGOUT_ADDR = "/logout-addr";

    private SecurityMappingAddr(){
        throw new IllegalStateException(CanNotInstantiateReason.ENUM_CLASS.descriptions());
    }
}
