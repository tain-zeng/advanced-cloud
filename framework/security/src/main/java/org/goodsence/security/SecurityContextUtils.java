package org.goodsence.security;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2TokenIntrospectionClaimNames;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zty
 * @apiNote {@link SecurityContextHolder}
 * @user nav1c
 * @date 2023/12/8 23:49
 * @project advanced-cloud
 */
public final class SecurityContextUtils {

    private SecurityContextUtils() {
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static SecurityContext getSecurityContext(){
        return SecurityContextHolder.getContext();
    }

    public static void setAuthentication(Authentication authentication) {
        SecurityContext securityContext = getSecurityContext();
        if (securityContext != null) {
            securityContext.setAuthentication(authentication);
        }
        throw new IllegalStateException("登录上下文为空！");
    }

    /**
     * 获取当前登录用户的身份认证对象
     * @return 当前登录用户的身份认证对象，如果未登录则返回null
     */
    public static Authentication getCurrentAuthentication() {
        SecurityContext securityContext = getSecurityContext();
        if (securityContext != null) {
            return securityContext.getAuthentication();
        }
        throw new IllegalStateException();
    }

    /**
     * 获取当前登录用户的用户名
     * @return 当前登录用户的用户名，如果未登录则返回null
     */
    public static String getCurrentUsername() {
        final Authentication authentication = getCurrentAuthentication();
        if (authentication == null) {
            return null;
        }
        if (!authentication.isAuthenticated()) {
            throw new IllegalStateException("未授权：" + authentication.getName());
        }
        return authentication.getName();
    }

    /**
     * 返回类型不能改为UserDetails! 有可能是Oauth2
     * @deprecated Authentication 本身就是principal
     */
    @Deprecated
    public static Object getPrincipal() {
        final Authentication authentication = getCurrentAuthentication();
        if (authentication == null) {
            return null;
        }
        if (!authentication.isAuthenticated()) {
            throw new IllegalStateException("你他妈为什么能访问?");
        }
        return authentication.getPrincipal();
    }

    public static UserDetails getCurrentUserDetails() {
        Authentication authentication = getCurrentAuthentication();
        Set<String> dataAuthorities = getDataAuthorities();
        return new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return authentication.getAuthorities();
            }

            @Override
            public String getPassword() {
                return null;
            }

            @Override
            public String getUsername() {
                return authentication.getName();
            }

            public Collection<String> getDataAuthorities() {
                return dataAuthorities;
            }
        };
    }

    @Deprecated
    public static OAuth2AuthenticatedPrincipal getOAuth2AuthenticatedPrincipal() {
        Object principal = getPrincipal();
        if (principal instanceof OAuth2AuthenticatedPrincipal oauth2AuthenticatedPrincipal) {
            return oauth2AuthenticatedPrincipal;
        }
        return null;
    }

    public static Collection<? extends GrantedAuthority> getAuthorities() {
        Authentication authentication = getCurrentAuthentication();
        return authentication.getAuthorities();
    }

    public static Map<String, Object> getAttributes() {
        Authentication authentication = getCurrentAuthentication();
        if (authentication instanceof BearerTokenAuthentication bearerTokenAuthentication) {
            return bearerTokenAuthentication.getTokenAttributes();
        }
        return Collections.emptyMap();
    }

    public static Object getAttribute(String key) {
        return getAttributes().get(key);
    }

    public static Set<String> getDataAuthorities() {
        Object attribute = getAttribute(CustomOAuth2TokenClaimNames.DATA_AUTHORITIES);
        if (attribute instanceof Collection<?> collection) {
            return collection.stream().map(Object::toString).collect(Collectors.toSet());
        }
        return Collections.emptySet();
    }
}
