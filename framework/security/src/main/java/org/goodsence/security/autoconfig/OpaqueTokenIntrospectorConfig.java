package org.goodsence.security.autoconfig;

import org.goodsence.security.FakePublicOpaqueTokenIntrospector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.web.client.RestOperations;

/**
 * 应当在security模块自动配置
 */
@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class OpaqueTokenIntrospectorConfig {

    @Bean
    @Autowired
    @ConditionalOnMissingBean(OpaqueTokenIntrospector.class)
    public OpaqueTokenIntrospector opaqueTokenIntrospector(RestOperations restOperations,
                                                           OAuth2ResourceServerProperties properties) {
        return new FakePublicOpaqueTokenIntrospector(restOperations, properties);
    }

}
