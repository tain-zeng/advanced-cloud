package org.goodsence.security;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.web.ExceptionHandler;
import org.goodsence.web.Results;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;


/**
 * @author tain
 * @apiNote
 * @date 2024/10/16
 * @project modern-arranged-marriage
 */
@Slf4j
public class AuthenticationExceptionHandler implements ExceptionHandler {

    @Override
    public boolean isSupported(Exception e) {
        return AuthenticationException.class.isAssignableFrom(e.getClass());
    }

    @Override
    public ResponseEntity<Object> handleException(Exception ex) {
        log.error("授权异常: {}", ex.getMessage(), ex);
        return Results.unauthorized();
    }
}
