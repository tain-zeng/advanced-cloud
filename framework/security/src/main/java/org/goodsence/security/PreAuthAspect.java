package org.goodsence.security;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.goodsence.common.aspect.CommonAspectPattern;
import org.goodsence.common.utils.AspectUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-11 09:37:47
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Component
@Aspect
public class PreAuthAspect implements CommonAspectPattern {

    @Override
    @Pointcut("@annotation(org.goodsence.security.PreAuth)")
    public void pointcut(){
    }

    @Override
    @Before("pointcut()")
    public void before(JoinPoint joinPoint){
        Collection<? extends GrantedAuthority> authorities = SecurityContextUtils.getAuthorities();
        PreAuth permission = AspectUtils.getMethodAnnotationObject(joinPoint, PreAuth.class);
        boolean permitted = authorities.contains(new SimpleGrantedAuthority(permission.value()));
        if (!permitted) {
            throw new AccessDeniedException("无权访问!");
        }
    }
}
