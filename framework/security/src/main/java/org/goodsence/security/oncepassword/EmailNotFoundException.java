package org.goodsence.security.oncepassword;

import org.springframework.security.core.AuthenticationException;

/**
 * <p>
 * created at <b> 2024-04-04 05:55:01 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
@Deprecated
public class EmailNotFoundException extends AuthenticationException {
    public EmailNotFoundException(String msg) {
        super(msg);
    }
}
