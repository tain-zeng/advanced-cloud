package org.goodsence.security;

/**
 * @author tain
 * @apiNote 核验类型
 * @date 2024/12/5
 * @project newsportal
 */
public interface PermissionType {

    String AUTHORITY = "authority";
}
