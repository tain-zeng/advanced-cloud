/**
 * @apiNote 每个后端服务都是资源服务器
 * @version
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @date 2024-03-20 18:12:27
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
package org.goodsence.security;