package org.goodsence.security.oncepassword;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * @author zengty
 * @apiNote
 * @date 2025/1/3
 * @project advanced-cloud
 */
public class OncePasswordPreAuthenticatedProcessingFilter extends AbstractPreAuthenticatedProcessingFilter {

    public OncePasswordPreAuthenticatedProcessingFilter(AuthenticationProvider authenticationProvider) {
        setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/once-password-login", "GET"));
        setAuthenticationManager(new ProviderManager(authenticationProvider));
    }

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return request.getParameter("account");
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return request.getParameter("credentials");
    }
}
