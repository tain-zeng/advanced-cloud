package org.goodsence.security;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.web.ExceptionHandler;
import org.goodsence.web.Results;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;

/**
 * @author tain
 * @apiNote
 * @date 2024/11/28
 * @project advanced-cloud
 */
@Slf4j
public class AccessDeniedExceptionHandler implements ExceptionHandler {
    @Override
    public boolean isSupported(Exception e) {
        return AccessDeniedException.class.isAssignableFrom(e.getClass());
    }

    @Override
    public ResponseEntity<Object> handleException(Exception ex) {
        return Results.forbidden();
    }
}
