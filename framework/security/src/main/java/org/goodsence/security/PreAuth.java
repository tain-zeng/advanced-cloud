package org.goodsence.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-11 09:38:56
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Retention(RetentionPolicy.RUNTIME) //运行时可获取
@Target({ElementType.METHOD})
public @interface PreAuth {

    String value();

    String type() default PermissionType.AUTHORITY;
}
