package org.goodsence.security.oncepassword;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.common.utils.GenerateUtils;
import org.goodsence.security.AccountType;
import org.goodsence.security.FlexibleUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Objects;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/31
 * @project advanced-cloud
 */
@Slf4j
public class DefaultOncePasswordSender implements OncePasswordSender {
    
    private final OncePasswordStore oncePasswordStore;

    private final FlexibleUserDetailsService flexibleUserDetailsService;

    public DefaultOncePasswordSender(OncePasswordStore oncePasswordStore, FlexibleUserDetailsService flexibleUserDetailsService) {
        this.oncePasswordStore = oncePasswordStore;
        this.flexibleUserDetailsService = flexibleUserDetailsService;
    }

    @Override
    public void send(AccountType accountType, String account) {
        UserDetails userDetails = flexibleUserDetailsService.loadUserByAccount(accountType, account);
        if (userDetails == null) {
            return;
        }
        String oncePassword = generateOncePassword();
        oncePasswordStore.save(AccountType.EMPTY, userDetails.getPassword(), oncePassword);
        send(accountType, account, oncePassword);
    }

    /**
     * 生成一次性密码。
     */
    protected String generateOncePassword(){
        return Objects.toString(GenerateUtils.generateRandomNumber((byte) 6));
    }

    /**
     * 将一次性密码发送给用户，可能是邮箱也可能是手机号等其它
     */
    protected void send(AccountType accountType, String account, String oncePassword) {
        log.info("类型{}, 账号{}的一次性密码是:{}", accountType, account, oncePassword);
    }
}
