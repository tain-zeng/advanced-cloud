package org.goodsence.security.autoconfig;

import org.goodsence.security.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.AbstractRequestMatcherRegistry;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2023-12-18 19:57:48
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@EnableMethodSecurity(prePostEnabled = true, securedEnabled = false, jsr250Enabled = false)
public class SecurityFilterChainConfig {


    private final OpaqueTokenIntrospector opaqueTokenIntrospector;

    private final ResourceProperties resourceProperties;

    @Autowired
    public SecurityFilterChainConfig(OpaqueTokenIntrospector opaqueTokenIntrospector,
                                     ResourceProperties resourceProperties) {
        this.opaqueTokenIntrospector = opaqueTokenIntrospector;
        this.resourceProperties = resourceProperties;
    }

    @Bean
    @Order(Constants.RESOURCE_SERVER_FILTER_ORDER)
    public SecurityFilterChain resourceServerFilterChain(HttpSecurity http) throws Exception {
        http.oauth2ResourceServer(rsConfig -> rsConfig.opaqueToken(
                tokenConfig -> tokenConfig.introspector(opaqueTokenIntrospector)
                )
        );
        http.securityMatchers(AbstractRequestMatcherRegistry::anyRequest)
                .authorizeHttpRequests(rmRegistry ->
                        rmRegistry.requestMatchers(resourceProperties.getPermits().toArray(new String[0]))
                                .permitAll()
                                .requestMatchers(resourceProperties.getDenies().toArray(new String[0]))
                                .denyAll()
                                .requestMatchers(resourceProperties.getRemembers().toArray(new String[0]))
                                .rememberMe()
                                .requestMatchers(resourceProperties.getAnonymous().toArray(new String[0]))
                                .anonymous()
                                .anyRequest()
                                .authenticated()
                );
        http.csrf(AbstractHttpConfigurer::disable);
        http.sessionManagement(smConfig -> smConfig.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        return http.build();
    }
}
