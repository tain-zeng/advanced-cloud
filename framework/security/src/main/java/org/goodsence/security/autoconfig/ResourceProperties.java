package org.goodsence.security.autoconfig;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.Set;

/**
 * @User tain
 * @Author zty
 * @Date 2023/4/6 23:38
 * @ProjectName modern-arranged-marriage
 * @Version 1.0.0
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "spring.security.resource-properties")
public class ResourceProperties {

    /**不能不赋值  because the return value of "org.goodsence.authority.security.ResourceProperties.getPermits()" is null */
    private Set<String> permits = Collections.emptySet();

    private Set<String> denies = Collections.emptySet();

    private Set<String> anonymous = Collections.emptySet();

    private Set<String> remembers = Collections.emptySet();
}
