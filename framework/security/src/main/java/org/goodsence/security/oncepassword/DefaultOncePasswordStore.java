package org.goodsence.security.oncepassword;

import org.goodsence.common.KvOperations;
import org.goodsence.security.AccountType;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/31
 * @project advanced-cloud
 */
public class DefaultOncePasswordStore implements OncePasswordStore {

    private final KvOperations kvOperations;

    public DefaultOncePasswordStore(KvOperations kvOperations) {
        this.kvOperations = kvOperations;
    }

    @Override
    public void save(AccountType oncePasswordType, String account, String oncePassword) {
        kvOperations.put(generateKey(oncePasswordType, account), oncePassword);
    }

    @Override
    public void remove(AccountType oncePasswordType, String account) {
        kvOperations.remove(generateKey(oncePasswordType, account));
    }

    @Override
    public String getOncePassword(AccountType oncePasswordType, String account) {
        return (String) kvOperations.get(generateKey(oncePasswordType, account));
    }

    private String generateKey(AccountType accountType, String account) {
        return accountType.getType() + "_" + account;
    }
}
