package org.goodsence.security;

import org.goodsence.common.CanNotInstantiateReason;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-02-07 00:57:33
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public final class CustomOAuth2TokenClaimNames {

    public static final String AUTHORITIES = "authorities";

    public static final String DATA_AUTHORITIES = "data_authorities";

    private CustomOAuth2TokenClaimNames () {
        throw new IllegalStateException(CanNotInstantiateReason.ENUM_CLASS.descriptions());
    }
}
