package org.goodsence.security;

import cn.hutool.core.lang.Assert;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author zengty
 * @apiNote
 * @date 2025/1/3
 * @project advanced-cloud
 */
@Data
public class AccountType implements Serializable {

    private final String type;

    public static final AccountType USERNAME = new AccountType("username");

    public static final AccountType EMPTY = new AccountType("");

    private AccountType(String type) {
        this.type = type;
    }

    public static AccountType of(String type) {
        Assert.notNull(type, "type must not be null");
        return new AccountType(type);
    }

    @Serial
    private static final long serialVersionUID = 100330089000000000L;
}
