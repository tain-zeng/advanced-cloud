package org.goodsence.security;

import org.goodsence.common.utils.HashUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Objects;

/**
 * @author tain
 * @apiNote 自定义密码编码器
 * @date 2024/11/28
 * @project advanced-cloud
 */
public class HashPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence rawPassword) {
        return HashUtils.hash(Objects.toString(rawPassword));
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return HashUtils.verify(Objects.toString(rawPassword), encodedPassword);
    }
}
