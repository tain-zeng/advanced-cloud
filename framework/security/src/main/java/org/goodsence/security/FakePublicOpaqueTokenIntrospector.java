package org.goodsence.security;

import jakarta.servlet.http.HttpServletRequest;
import org.goodsence.web.HttpServletUtils;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.server.resource.introspection.BadOpaqueTokenException;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionException;
import org.springframework.web.client.RestOperations;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/30
 * @project advanced-cloud
 */
public class FakePublicOpaqueTokenIntrospector extends CustomOpaqueTokenIntrospector {
    /**
     * Creates a {@code OpaqueTokenAuthenticationProvider} with the provided parameters
     *
     * @param restOperations
     * @param properties
     */
    public FakePublicOpaqueTokenIntrospector(RestOperations restOperations, OAuth2ResourceServerProperties properties) {
        super(restOperations, properties);
    }

    @Override
    protected void embedAdditionalAuth(HttpHeaders httpHeaders) {
        HttpServletRequest httpServletRequest = HttpServletUtils.getHttpServletRequest();
        if (httpServletRequest == null) {
            throw new OAuth2IntrospectionException("httpServletRequest cannot be null");
        }
        Enumeration<String> headers = httpServletRequest.getHeaders(Constants.FAKE_PUBLIC_CLIENT_AUTHORIZATION);
        Iterator<String> iterator = headers.asIterator();
        boolean isContainFakePublicClientAuthorization = false;
        while (iterator.hasNext()) {
            String header = iterator.next();
            if (header.startsWith("Basic ")) {
                httpHeaders.add(HttpHeaders.AUTHORIZATION, header);
                isContainFakePublicClientAuthorization = true;
                break;
            }
        }
        if (!isContainFakePublicClientAuthorization) {
            throw new BadOpaqueTokenException("没有包含请求头: " + Constants.FAKE_PUBLIC_CLIENT_AUTHORIZATION);
        }
    }
}
