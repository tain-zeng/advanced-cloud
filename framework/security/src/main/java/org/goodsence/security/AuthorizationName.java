package org.goodsence.security;

import org.goodsence.common.CanNotInstantiateReason;

/**
 * @author zty
 * @apiNote 认证请求头名字
 * @user nav1c
 * @date 2023/9/7 5:19
 * @project advanced-cloud
 */
public final class AuthorizationName {

    public static final String AUTHORIZATION = "Authorization";

    private AuthorizationName(){
        throw new IllegalStateException(CanNotInstantiateReason.ENUM_CLASS.descriptions());
    }
}
