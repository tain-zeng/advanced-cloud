package org.goodsence.security.oncepassword;

@Deprecated
public interface MobileUserDetailsService {

    /**
     * 根据加密的手机号寻找认证用户
     * @param encryptedMobile 不可逆加密手机号
     * @return User
     */
    MobileUserDetails loadUserByEncryptedMobile(String encryptedMobile) throws EncryptedMobileNotFoundException;
}
