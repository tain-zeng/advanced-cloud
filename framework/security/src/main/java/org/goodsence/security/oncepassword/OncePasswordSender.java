package org.goodsence.security.oncepassword;

import org.goodsence.security.AccountType;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/31
 * @project advanced-cloud
 */
public interface OncePasswordSender {
    void send(AccountType accountType, String account);
}
