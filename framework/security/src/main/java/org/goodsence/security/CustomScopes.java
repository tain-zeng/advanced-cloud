package org.goodsence.security;

import org.goodsence.common.CanNotInstantiateReason;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-02-07 19:53:48
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public class CustomScopes {

    public static final String USER_AUTHORITY = "user_authority";

    public static final String USER_DATA_AUTHORITY = "user_data_authority";

    private CustomScopes() {
        throw new IllegalStateException(CanNotInstantiateReason.ENUM_CLASS.descriptions());
    }
}
