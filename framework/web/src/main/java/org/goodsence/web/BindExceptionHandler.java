package org.goodsence.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;

/**
 * @author tain
 * @apiNote
 * @date 2024/11/28
 * @project advanced-cloud
 */
@Slf4j
public class BindExceptionHandler implements ExceptionHandler {

    @Override
    public boolean isSupported(Exception e) {
       return BindException.class.isAssignableFrom(e.getClass());
    }

    @Override
    public ResponseEntity<Object> handleException(Exception ex) {
        final FieldError fieldError = ((BindException)ex).getFieldError();
        assert fieldError != null;
        log.warn(fieldError.getDefaultMessage());
        return Results.badRequest(fieldError.getDefaultMessage());
    }
}
