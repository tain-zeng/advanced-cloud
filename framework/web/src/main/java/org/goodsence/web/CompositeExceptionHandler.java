package org.goodsence.web;

import org.springframework.http.ResponseEntity;

import java.util.Collection;

/**
 * @author zengty
 * @apiNote
 * @date 2025/1/2
 * @project advanced-cloud
 */
public class CompositeExceptionHandler implements ExceptionHandler {

    private final Collection<ExceptionHandler> handlers;

    private final ExceptionHandler defaultHandler;

    public CompositeExceptionHandler(Collection<ExceptionHandler> handlers, ExceptionHandler defaultHandler) {
        this.handlers = handlers;
        this.defaultHandler = defaultHandler;
    }

    @Override
    public boolean isSupported(Exception e) {
        return false;
    }

    @Override
    public ResponseEntity<Object> handleException(Exception ex) {
        for (ExceptionHandler handler : handlers) {
            try {
                ResponseEntity<Object> response = handler.handleException(ex);
                if (response != null) {
                    return response;
                }
            }catch (Exception ignored) {
                // do nothing
            }
        }
        return defaultHandler.handleException(ex);
    }
}
