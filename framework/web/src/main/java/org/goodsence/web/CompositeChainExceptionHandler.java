package org.goodsence.web;

import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;

public class CompositeChainExceptionHandler implements ExceptionHandler {

    private final Collection<ExceptionHandler> exceptionHandlers;

    public CompositeChainExceptionHandler(){
        this.exceptionHandlers = new ArrayList<>();
    }

    public CompositeChainExceptionHandler(Collection<ExceptionHandler> exceptionHandlers) {
        Assert.notNull(exceptionHandlers, "exceptionHandlers must not be null");
        this.exceptionHandlers = exceptionHandlers;
    }

    public void addExceptionHandlers(Collection<ExceptionHandler> exceptionHandlers) {
        this.exceptionHandlers.addAll(exceptionHandlers);
    }

    public void addExceptionHandler(ExceptionHandler exceptionHandler) {
        this.exceptionHandlers.add(exceptionHandler);
    }

    @Override
    public boolean isSupported(Exception e) {
        return exceptionHandlers.stream()
                .anyMatch(exceptionHandler -> exceptionHandler.isSupported(e));
    }

    @Override
    public ResponseEntity<Object> handleException(Exception ex) {
        for (ExceptionHandler exceptionHandler : exceptionHandlers) {
            if (exceptionHandler.isSupported(ex)) {
                return exceptionHandler.handleException(ex);
            }
        }
        throw new IllegalStateException("Could not handle exception", ex);
    }
}
