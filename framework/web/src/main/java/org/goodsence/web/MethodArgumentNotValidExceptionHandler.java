package org.goodsence.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;

/**
 * @author tain
 * @apiNote
 * @date 2024/11/28
 * @project advanced-cloud
 */
@Slf4j
public class MethodArgumentNotValidExceptionHandler implements ExceptionHandler {
    @Override
    public boolean isSupported(Exception e) {
        return MethodArgumentNotValidException.class.isAssignableFrom(e.getClass());
    }

    @Override
    public ResponseEntity<Object> handleException(Exception ex) {
        log.warn(ex.getMessage());
        StringBuilder message = new StringBuilder();
        final List<FieldError> fieldErrors = ((MethodArgumentNotValidException) ex).getBindingResult().getFieldErrors();
        for (FieldError error : fieldErrors) {
            message.append(error.getField()).append(error.getDefaultMessage()).append(",");
        }
        final String msg = message.substring(0, message.length() - 1);
        log.warn(msg);
        return Results.badRequest(msg);
    }
}
