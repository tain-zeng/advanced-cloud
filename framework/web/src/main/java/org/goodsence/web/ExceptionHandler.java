package org.goodsence.web;

import org.springframework.http.ResponseEntity;

/**
 * @author tain
 * @apiNote {@link DefaultControllerAdvice#afterPropertiesSet()}
 * @date 2024/10/16
 * @project modern-arranged-marriage
 */
public interface ExceptionHandler {

    boolean isSupported(Exception e);

    ResponseEntity<Object> handleException(Exception ex);
}
