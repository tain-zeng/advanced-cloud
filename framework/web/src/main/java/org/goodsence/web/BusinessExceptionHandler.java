package org.goodsence.web;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.common.BusinessException;
import org.springframework.http.ResponseEntity;

/**
 * @author tain
 * @apiNote
 * @date 2024/11/28
 * @project advanced-cloud
 */
@Slf4j
public class BusinessExceptionHandler implements ExceptionHandler {

    @Override
    public boolean isSupported(Exception e) {
        return BusinessException.class.isAssignableFrom(e.getClass());
    }

    @Override
    public ResponseEntity<Object> handleException(Exception ex) {
        log.warn(ex.getMessage());
        return Results.badRequest(ex.getMessage());
    }
}
