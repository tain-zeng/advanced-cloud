package org.goodsence.web;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-02-24 23:58:48
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public final class RestOperationsUtils implements BeanFactoryAware {

    private static final String URL_REQUIRE_HAS_TEXT_MSG = "请求地址不能为空!";

    private static final String RESPONSE_ENTITY_REQUIRE_NOT_NULL_MSG = "ResponseEntity不能为空";

    private static RestOperations restOperations;

    private RestOperationsUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static boolean isSuccessful(@NonNull ResponseEntity<?> responseEntity) {
        Assert.notNull(responseEntity, RESPONSE_ENTITY_REQUIRE_NOT_NULL_MSG);
        return responseEntity.getStatusCode().is2xxSuccessful();
    }

    public static boolean isError(@NonNull ResponseEntity<?> responseEntity) {
        Assert.notNull(responseEntity, RESPONSE_ENTITY_REQUIRE_NOT_NULL_MSG);
        return responseEntity.getStatusCode().isError();
    }

    public static boolean isServerError(@NonNull ResponseEntity<?> responseEntity) {
        Assert.notNull(responseEntity, RESPONSE_ENTITY_REQUIRE_NOT_NULL_MSG);
        return responseEntity.getStatusCode().is5xxServerError();
    }

    public static boolean isClientError(@NonNull ResponseEntity<?> responseEntity) {
        Assert.notNull(responseEntity, RESPONSE_ENTITY_REQUIRE_NOT_NULL_MSG);
        return responseEntity.getStatusCode().is4xxClientError();
    }

    public static void downloadFile(@NonNull HttpServletResponse response, @NonNull String url, @NonNull String filename) {
        downloadFile(response, url, filename, HttpMethod.GET);
    }

    public static void downloadFile(@NonNull HttpServletResponse response, @NonNull String url, @NonNull String filename,
                                    @NonNull HttpMethod httpMethod) {
        downloadFile(response, url, filename, httpMethod, HttpEntity.EMPTY);
    }

    public static void downloadFile(@NonNull HttpServletResponse response, @NonNull String url, @NonNull String filename,
                                    @NonNull HttpMethod httpMethod, HttpEntity<?> httpEntity) {
        Assert.notNull(response, "response不能为空");
        Assert.hasText(url, URL_REQUIRE_HAS_TEXT_MSG);
        Assert.hasText(filename, "文件名不能为空");
        Assert.notNull(httpMethod, "httpMethod不能为空!");
        Assert.state(filename.contains("."), "文件名格式不正确");
        ResponseEntity<byte[]> responseEntity = restOperations.exchange(url, httpMethod, httpEntity, byte[].class);
        byte[] body;
        if (!responseEntity.getStatusCode().is2xxSuccessful() || (body = responseEntity.getBody()) == null) {
            throw new IllegalStateException("发起请求失败:" + responseEntity.getStatusCode());//spring6没有HttpResponseException
        }
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ContentDisposition.attachment()
                .filename(filename, StandardCharsets.UTF_8).build().toString());
        try(ServletOutputStream outputStream = response.getOutputStream()){
            outputStream.write(body);
            outputStream.flush();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static ResponseEntity<Object> uploadFile(@NonNull String url, @NonNull MultipartFile file) throws IOException {
        Assert.notNull(file, "文件不能为空");
        Assert.state(!file.isEmpty(), "文件不能为空");
        ByteArrayResource resource = new ByteArrayResource(file.getBytes()) {
            @Override
            public String getFilename() {
                return file.getOriginalFilename();
            }
        };
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", resource);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
        return doPost(url, httpEntity);
    }

    public static ResponseEntity<Object> doPostWithUrlEncoded(@NonNull String url, MultiValueMap<String, String> params) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(params, headers);
        return doPost(url, httpEntity);
    }

    public static ResponseEntity<Object> doPostWithSoap(@NonNull String url, @NonNull String soapRequest) {
        Assert.hasText(soapRequest, "soap报文不能为空");
        Assert.state(soapRequest.startsWith("<") && soapRequest.endsWith(">"), "soap报文格式不正确");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
        HttpEntity<String> httpEntity = new HttpEntity<>(soapRequest, headers);
        return doPost(url, httpEntity);
    }

    public static ResponseEntity<Object> doPostWithRequestParam(@NonNull String url, MultiValueMap<String, String> requestParams){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(requestParams, headers);
        return doPost(url, httpEntity);
    }

    public static ResponseEntity<Object> doPutWithJsonRequestBody(@NonNull String url, String jsonBody) {
        Assert.state(jsonBody.startsWith("{") && jsonBody.endsWith("}"), "json格式不正确");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return doPut(url, headers, jsonBody);
    }

    public static ResponseEntity<Object> doPostWithJsonRequestBody(@NonNull String url, String jsonBody) {
        Assert.state(jsonBody.startsWith("{") && jsonBody.endsWith("}"), "json格式不正确");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return doPost(url, headers, jsonBody);
    }

    public static ResponseEntity<Object> doPost(@NonNull String url, HttpHeaders headers, Object body) {
        return doPost(url, new HttpEntity<>(body,headers));
    }

    public static ResponseEntity<Object> doPost(@NonNull String url, HttpEntity<?> httpEntity) {
        return exchange(url, HttpMethod.POST, httpEntity);
    }

    public static ResponseEntity<Object> doGet(@NonNull String url, HttpHeaders headers, Object body) {
        return doGet(url, new HttpEntity<>(body,headers));
    }

    public static ResponseEntity<Object> doGet(@NonNull String url, HttpEntity<?> httpEntity) {
        return exchange(url, HttpMethod.GET, httpEntity);
    }

    public static ResponseEntity<Object> doDelete(@NonNull String url, HttpHeaders headers, Object body) {
        return doDelete(url, new HttpEntity<>(body,headers));
    }

    public static ResponseEntity<Object> doDelete(@NonNull String url, HttpEntity<?> httpEntity){
        return exchange(url, HttpMethod.DELETE, httpEntity);
    }

    public static ResponseEntity<Object> doPut(@NonNull String url, HttpHeaders headers, Object body) {
        return doPut(url, new HttpEntity<>(body,headers));
    }

    public static ResponseEntity<Object> doPut(@NonNull String url, HttpEntity<?> httpEntity){
        return exchange(url, HttpMethod.PUT, httpEntity);
    }

    public static ResponseEntity<Object> exchange(@NonNull String url, @NonNull HttpMethod httpMethod, HttpEntity<?> httpEntity) {
        Assert.hasText(url, URL_REQUIRE_HAS_TEXT_MSG);
        Assert.notNull(httpMethod, "请求方式不能为空");
        return restOperations.exchange(url, httpMethod, httpEntity, Object.class);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        RestOperationsUtils.restOperations = beanFactory.getBean(RestOperations.class);
    }
}
