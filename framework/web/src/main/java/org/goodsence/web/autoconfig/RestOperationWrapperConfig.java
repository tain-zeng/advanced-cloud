package org.goodsence.web.autoconfig;

import org.goodsence.web.RestOperationsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Configuration
public class RestOperationWrapperConfig {

    @Bean
    @Autowired
    @ConditionalOnMissingBean(RestOperationsWrapper.class)
    public RestOperationsWrapper restOperationsWrapper(RestOperations restOperations) {
        return new RestOperationsWrapper(restOperations);
    }
}
