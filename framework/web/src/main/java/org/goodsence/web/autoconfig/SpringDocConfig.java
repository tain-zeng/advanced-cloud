package org.goodsence.web.autoconfig;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

/**
 * @author zty
 * @apiNote SpringDoc配置
 * @user nav1c
 * @date 2023/8/25 14:17
 * @project advanced-cloud
 */
@OpenAPIDefinition(
        info = @Info(
                title = "authority",
                version = "1.0",
                description = "authority",
                contact = @Contact(name = "tain")
        ),
        security = @SecurityRequirement(name = "Authorization"),
        externalDocs = @ExternalDocumentation(
                description = "参考文档",
                url = "https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations"
        )
)
@SecurityScheme(type = SecuritySchemeType.HTTP, name = "Authorization", scheme = "Bearer",
        in = SecuritySchemeIn.HEADER)
public class SpringDocConfig {

}
