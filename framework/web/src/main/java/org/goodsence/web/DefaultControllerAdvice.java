package org.goodsence.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collection;

/**
 * @author zty
 * @apiNote 全局异常处理器
 * {@link org.springframework.boot.autoconfigure.web.reactive.ProblemDetailsExceptionHandler}
 * {@link org.springframework.boot.autoconfigure.web.servlet.ProblemDetailsExceptionHandler}
 * @project_name the-revolt
 * @user tain
 * @create_at 2023/6/14 20:41
 * @create_vio IntelliJ IDEA
 */
@RestControllerAdvice
public class DefaultControllerAdvice extends ResponseEntityExceptionHandler implements ApplicationContextAware {

    private final Logger log = LoggerFactory.getLogger(DefaultControllerAdvice.class);

    private final CompositeChainExceptionHandler exceptionHandler;

    public DefaultControllerAdvice() {
        this.exceptionHandler = new CompositeChainExceptionHandler();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Collection<ExceptionHandler> values = applicationContext.getBeansOfType(ExceptionHandler.class).values();
        this.exceptionHandler.addExceptionHandlers(values);
    }

    /**
     * 未知异常处理， 优先级最低?
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception e){
        try{
            return exceptionHandler.handleException(e);
        }catch (Exception ignore){
            log.error("发生异常: {}", e.getMessage(), e);
            return Results.error();
        }
    }



}
