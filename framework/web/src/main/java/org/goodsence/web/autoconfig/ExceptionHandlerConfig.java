package org.goodsence.web.autoconfig;

import org.goodsence.web.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/26
 * @project advanced-cloud
 */
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Configuration
@Import({BindExceptionHandler.class, BusinessExceptionHandler.class, MethodArgumentNotValidExceptionHandler.class,
        MissingServletRequestParameterExceptionHandler.class, DefaultControllerAdvice.class})
public class ExceptionHandlerConfig {

}
