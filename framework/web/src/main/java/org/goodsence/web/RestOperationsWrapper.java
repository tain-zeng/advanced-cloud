package org.goodsence.web;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author tain
 * @apiNote 简单包装一下RestOperations, 有些写法经常忘记
 * @date 2024/10/18
 * @project modern-arranged-marriage
 */
public class RestOperationsWrapper {

    private final RestOperations restOperations;

    public RestOperationsWrapper(RestOperations restOperations) {
        this.restOperations = restOperations;
    }

    public ResponseEntity<Object> doPost(final String url, final Object body) {
        return restOperations.postForEntity(url, body, Object.class);
    }

    public ResponseEntity<Object> doGet(final String url) {
        return restOperations.getForEntity(url, Object.class);
    }

    public ResponseEntity<Resource> download(String url) {
        ResponseEntity<byte[]> responseEntity = restOperations.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, byte[].class);
        byte[] body = responseEntity.getBody();
        if (!responseEntity.getStatusCode().is2xxSuccessful() || body == null) {
            throw new RuntimeException("Could not download file: " + url);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDisposition(ContentDisposition.attachment().build());
        Resource resource = new ByteArrayResource(body);
        return ResponseEntity.ok().headers(headers).body(resource);
    }

    /**
     * @deprecated 这是旧的写法, 请使用{@link RestOperationsWrapper#download(String)}
     */
    @Deprecated
    public void download(HttpServletResponse response, final String url) {
        ResponseEntity<byte[]> responseEntity = restOperations.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, byte[].class);
        byte[] body = responseEntity.getBody();
        if (!responseEntity.getStatusCode().is2xxSuccessful() || body == null) {
            return;
        }
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, ContentDisposition.attachment()
                .filename("filename", StandardCharsets.UTF_8).build().toString());
        try (OutputStream outputStream = response.getOutputStream()) {
            outputStream.write(body);
            outputStream.flush();
        } catch (IOException ignored) {
        }
    }

    public ResponseEntity<Object> upload(final String url, MultipartFile file) {
        byte[] bytes;
        try {
            bytes = file.getBytes();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Resource resource = new ByteArrayResource(bytes) {
            @Override
            public String getFilename() {
                return file.getOriginalFilename();
            }
        };
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", resource);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<Object> httpEntity = new HttpEntity<>(body, headers);
        return restOperations.exchange(url, HttpMethod.POST, httpEntity, Object.class);
    }

}
