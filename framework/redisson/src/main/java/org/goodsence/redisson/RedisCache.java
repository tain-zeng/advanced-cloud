package org.goodsence.redisson;

import org.goodsence.common.KvOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Set;

/**
 * @author tain
 * @apiNote
 * @date 2024/10/14
 * @project modern-arranged-marriage
 */
@Component
public class RedisCache implements KvOperations {

    private final RedisOperations<String, Object> redisOperations;

    private final ValueOperations<String, Object> valueOperations;

    private final String cacheName;

    @Autowired
    public RedisCache(RedisOperations<String, Object> redisOperations) {
        this(redisOperations, "CACHE_");
    }

    public RedisCache(RedisOperations<String, Object> redisOperations, String cacheName) {
        this.valueOperations = redisOperations.opsForValue();
        this.redisOperations = redisOperations;
        this.cacheName = cacheName;
    }

    @Override
    public Object get(String key) {
        return valueOperations.get(generateKey(key));
    }

    @Override
    public void put(String key, Object value) {
        valueOperations.set(generateKey(key), value);
    }

    @Override
    public void put(String key, Object value, long ttl) {
        valueOperations.set(generateKey(key), value, ttl);
    }

    @Override
    public void remove(String key) {
        redisOperations.delete(generateKey(key));
    }

    @Override
    public boolean contains(String key) {
        return Boolean.TRUE.equals(redisOperations.hasKey(generateKey(key)));
    }

    @Override
    public void clear() {
        Set<String> keys = redisOperations.keys(generateKey("*"));
        if (keys != null) {
            keys.stream().filter(StringUtils::hasText).forEach(redisOperations::delete);
        }
    }

    private String generateKey(String key) {
        return cacheName + ":" + key;
    }
}
