package org.goodsence.redisson.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.goodsence.common.aspect.AroundAspectPattern;
import org.goodsence.common.utils.AspectUtils;
import org.goodsence.redisson.RedisCache;
import org.goodsence.redisson.aspect.annotation.CustomCache;
import org.goodsence.redisson.aspect.annotation.CustomCacheInvalid;
import org.goodsence.redisson.aspect.annotation.CustomCacheKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 缓存切面
 * @date 2024-03-11 10:15:44
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Slf4j
@Component
@Aspect
public class CustomCacheAspect implements AroundAspectPattern {

    private static final String CACHE_PREFIX = CustomCacheAspect.class + "_";

    private final RedisCache redisCache;

    @Autowired
    public CustomCacheAspect(RedisCache redisCache) {
        this.redisCache = redisCache;
    }

    @Override
    @Pointcut("@annotation(org.goodsence.redisson.aspect.annotation.CustomCache)")
    public void pointcut() {
    }

    @Pointcut("@annotation(org.goodsence.redisson.aspect.annotation.CustomCacheInvalid)")
    public void invalid() {
    }

    @Override
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) {
        CustomCache customCache = AspectUtils.getMethodAnnotationObject(joinPoint, CustomCache.class);
        Object keySuffixObj = AspectUtils.getArgWithAnnotation(joinPoint, CustomCacheKey.class);
        String keySuffix = Objects.toString(keySuffixObj, "");
        if (!StringUtils.hasText(keySuffix)){
            keySuffix = customCache.value();
        }
        if (!StringUtils.hasText(keySuffix)){
            try {
                return joinPoint.proceed();
            } catch (Throwable e) {
                throw new IllegalStateException(e);
            }
        }
        String key = CACHE_PREFIX + keySuffix;
        if (redisCache.contains(key)){
            return redisCache.get(key);
        }else {
            Object result;
            try {
                result = joinPoint.proceed();
            } catch (Throwable e) {
                throw new IllegalStateException(e);
            }
            redisCache.put(key, result, customCache.expire());
            return result;
        }
    }

    @Around("invalid()")
    public Object invalid(ProceedingJoinPoint joinPoint) {
        CustomCacheInvalid customCacheInvalid = AspectUtils.getMethodAnnotationObject(joinPoint, CustomCacheInvalid.class);
        Object keySuffixObj = AspectUtils.getArgWithAnnotation(joinPoint, CustomCacheKey.class);
        String keySuffix = Objects.toString(keySuffixObj, "");
        if (!StringUtils.hasText(keySuffix)){
            keySuffix = customCacheInvalid.value();
        }
        if (StringUtils.hasText(keySuffix)){
            String key = CACHE_PREFIX + keySuffix;
            redisCache.remove(key);
        }
        try {
            return joinPoint.proceed();
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }
}
