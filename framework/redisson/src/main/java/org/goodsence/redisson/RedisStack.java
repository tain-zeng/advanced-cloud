package org.goodsence.redisson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.stereotype.Component;

/**
 * @author tain
 * @apiNote
 * @date 2024/10/15
 * @project modern-arranged-marriage
 */
@Component
public class RedisStack extends RedisListOps {

    @Autowired
    public RedisStack(RedisOperations<String, Object> redisOperations) {
        this(redisOperations, "REDIS_STACK");
    }

    public RedisStack(RedisOperations<String, Object> redisOperations, String key) {
        super(redisOperations, key);
    }
}
