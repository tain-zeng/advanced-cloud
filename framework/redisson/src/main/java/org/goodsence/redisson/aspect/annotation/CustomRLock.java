package org.goodsence.redisson.aspect.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-14 11:57:10
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CustomRLock {
    String value() default "";

    long waitTime();

    long timeout();
}
