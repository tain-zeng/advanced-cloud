package org.goodsence.redisson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.stereotype.Component;

/**
 * @apiNote 
 * @author tain
 * @date 2024/10/15
 * @project modern-arranged-marriage
 */
@Component
public class RedisQueue extends RedisListOps{

    @Autowired
    public RedisQueue(RedisOperations<String, Object> redisOperations) {
        this(redisOperations, "REDIS_QUEUE");
    }

    public RedisQueue(RedisOperations<String, Object> redisOperations, String key) {
        super(redisOperations, key);
    }
}
