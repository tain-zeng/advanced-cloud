package org.goodsence.redisson.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.goodsence.common.aspect.AroundAspectPattern;
import org.goodsence.common.utils.AspectUtils;
import org.goodsence.redisson.aspect.annotation.CustomRLock;
import org.goodsence.redisson.aspect.annotation.CustomRLockKey;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-14 11:55:51
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Slf4j
@Component
@Aspect
public class CustomRLockAspect implements AroundAspectPattern {

    private static final String CUSTOM_R_LOCK_PREFIX = "CUSTOM_R_LOCK_";

    private final RedissonClient redissonClient;

    @Autowired
    public CustomRLockAspect(RedissonClient redissonClient) {
        this.redissonClient = redissonClient;
    }

    @Around("pointcut()")
    @Override
    public Object around(ProceedingJoinPoint joinPoint) {
        CustomRLock customRLock = AspectUtils.getMethodAnnotationObject(joinPoint, CustomRLock.class);
        Object lockNameObj = AspectUtils.getArgWithAnnotation(joinPoint, CustomRLockKey.class);
        String lockNameSuffix = Objects.toString(lockNameObj, "");
        if (!StringUtils.hasText(lockNameSuffix)){
            lockNameSuffix = customRLock.value();
        }
        String lockName = CUSTOM_R_LOCK_PREFIX + lockNameSuffix;
        RLock rLock = redissonClient.getLock(lockName);
        Object result = null;
        try {
            rLock.tryLock(customRLock.waitTime(), customRLock.timeout(), TimeUnit.SECONDS);
            if (rLock.isLocked()){
                log.info("已锁: {}", rLock.getName());
                result = joinPoint.proceed();
            }
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        } finally {
            if (rLock.isLocked() && rLock.isHeldByCurrentThread()){
                rLock.unlock();
                log.info("已释放锁: {}", rLock.getName());
            }
        }
        return result;
    }

    @Pointcut("@annotation(org.goodsence.framework.redisson.aspect.annotation.CustomRLock)")
    @Override
    public void pointcut() {
    }
}
