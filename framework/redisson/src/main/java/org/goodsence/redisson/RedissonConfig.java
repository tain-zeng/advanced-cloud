package org.goodsence.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @user tain
 * @author  zty
 * @date  2023/7/1 7:54
 * @project  redis-demo
 * @version
 * redis配置。 如果不配置 在使用lettuce下会使用默认的RedisTemplate
 */
@Configuration
public class RedissonConfig {

    @Value("${spring.data.redis.host}")
    private String host;

    @Value("${spring.data.redis.port}")
    private int port;

    @Value("${spring.data.redis.database}")
    private int database;

    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + host + ":" + port).setDatabase(database);
        return Redisson.create(config);
    }


    /* 查看是否使用jedispool 可以查看redis.clients.jedis.JedisPoolConfig 以及org.springframework.data.redis.connection.jedis.JedisClientConfiguration */

//    @ConditionalOnMissingBean(value = "redisTemplate")
//    @Bean
//    @Autowired
//    public RedisTemplate<String, Serializable> redisTemplate(RedisConnectionFactory connectionFactory) {
//        //这个connectionFactory 会根据redisProperty的 connect-type产生jedis 或 letture的配置。
//        //如果引入redisson, 那么用的是RedissonConnectFactory
//        final RedisTemplate<String, Serializable> redisTemplate = new RedisTemplate<>();
//        redisTemplate.setKeySerializer(new StringRedisSerializer());
//        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
//        redisTemplate.setConnectionFactory(connectionFactory);
//        return redisTemplate;
//    }

//    @Bean
//    public RedissonClient redissonClient(){
//        Config config = new Config();
//        RedissonClient redissonClient = Redisson.create(config);
//    }

}
