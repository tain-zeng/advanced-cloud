package org.goodsence.redisson;

import org.goodsence.common.IStack;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisOperations;

import java.util.Objects;
import java.util.Queue;

/**
 * @author tain
 * @apiNote
 * @date 2024/10/14
 * @project modern-arranged-marriage
 */
public abstract class RedisListOps implements IStack<Object>, Queue<Object> {

    private final ListOperations<String, Object> listOps;

    private final RedisOperations<String, Object> redisOperations;

    private final String key;

    protected RedisListOps(RedisOperations<String, Object> redisOperations, String key) {
        this.listOps = redisOperations.opsForList();
        this.redisOperations = redisOperations;
        this.key = key;
    }

    /*
    栈操作
     */

    @Override
    public void push(Object o) {
        listOps.rightPush(key, o);
    }

    @Override
    public Object pop() {
        return listOps.rightPop(key);
    }

    /*
        *队列操作
    */

    @Override
    public boolean offer(Object o) {
        Long l = listOps.leftPush(key, o);
        return l != null && l > 0;
    }

    @Override
    public Object poll() {
        return listOps.rightPop(key);
    }

    /*
     * 公共操作
     */

    @Override
    public Object peek() {
        return listOps.index(key, 0);
    }

    @Override
    public boolean isEmpty() {
        return Objects.equals(listOps.size(key), 0L);
    }

    @Override
    public boolean contains(Object o) {
        return Boolean.TRUE.equals(redisOperations.hasKey(key));
    }

    @Override
    public int size() {
        return Integer.parseInt(String.valueOf(listOps.size(key)));
    }

    @Override
    public void clear() {
        redisOperations.delete(key);
    }

    /**
     * 这个方法必须覆盖
     */
    @Override
    public boolean add(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object element() {
        throw new UnsupportedOperationException();
    }

}
