package org.goodsence.redisson.aspect.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-14 14:49:54
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Retention(RetentionPolicy.RUNTIME)//很重要 否则运行时获取不到注解
@Target(ElementType.PARAMETER)
public @interface CustomCacheKey {

    String value() default "";
}
