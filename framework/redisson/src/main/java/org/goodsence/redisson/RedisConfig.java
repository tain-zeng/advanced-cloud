package org.goodsence.redisson;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author tain
 * @apiNote
 * @date 2024/10/14
 * @project modern-arranged-marriage
 */
@Configuration
public class RedisConfig {

    @Bean
    @ConditionalOnMissingBean(RedisOperations.class)
    @ConditionalOnBean(RedisConnectionFactory.class)
    public RedisOperations<String, Object> redisOperations(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        return redisTemplate;
    }
}
