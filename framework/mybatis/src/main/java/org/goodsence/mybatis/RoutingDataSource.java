package org.goodsence.mybatis;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @author zty
 * @apiNote 这个用于<b>运行时</b>切换数据源
 * @project_name mybatis-demo
 * @user tain
 * @create_at 2023/6/23 9:02
 * @create_vio IntelliJ IDEA
 * 将RoutingDataSource替换DataSource后Druid不生效，走的是Hikari连接池.
 */
public class RoutingDataSource extends AbstractRoutingDataSource {

    /**
     * 用于记录当前线程的数据源的lookupKey. {@link #determineCurrentLookupKey()} 以此改变数据源时不会影响到其它线程。<br>
     * 不能用static 因为系统可能要多个RoutingDataSource
     */
    private final ThreadLocal<String> lookupKey;

    public RoutingDataSource(DataSource defaultDataSource) {
        String datasourceId = defaultDataSource.toString();
        setTargetDataSources(Map.of(datasourceId, defaultDataSource)); //throw new IllegalArgumentException("Property 'targetDataSources' is required")
        setDefaultTargetDataSource(defaultDataSource);
        lookupKey = ThreadLocal.withInitial(() -> datasourceId);
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return lookupKey.get();
    }

    /**
     * 切换数据源
     */
    public void switchTo(String lookupKey) {
        this.lookupKey.set(lookupKey);
    }

    public void reset() {
        lookupKey.remove();
    }
}