package org.goodsence.mybatis;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author zty
 * @apiNote 拦截器， 可参考{@link com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor}
 * @user nav1c
 * @date 2023/9/1 11:51
 * @project advanced-cloud
 */
@Intercepts({
        @Signature(
                type = Executor.class,
                method = LogInterceptor.UPDATE,
                args = {MappedStatement.class, Object.class}
        ),
        @Signature(
                type = Executor.class, //org.apache.ibatis.executor.Executor 别搞错
                method = LogInterceptor.QUERY, //这是Executor的方法，点进去可以看到
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class} //这是query的参数，不能错。
        )
})
@Profile("dev")
public class LogInterceptor implements Interceptor {

    public static final String QUERY = "query";

    public static final String UPDATE = "update";

    private static final Logger log = LoggerFactory.getLogger(LogInterceptor.class);

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        final Object target = invocation.getTarget();
        log.info("target is : {}", target);
        final Method method = invocation.getMethod();
        log.info("method is : {}", method);
        final Object[] args = invocation.getArgs();
        log.info("args is : {}", Arrays.asList(args));
        final String methodName = method.getName();
        if (QUERY.equals(methodName) || UPDATE.equals(methodName)){
            final MappedStatement mappedStatement = (MappedStatement) args[0];//第一个参数是sql?
            log.info("mapperStatement is : {}", mappedStatement);
            final Object parameter = args[1]; //第二个参数是预编译的占位符参数？
            log.info("parameter is : {}", parameter);
            if (QUERY.equals(methodName)){
                final RowBounds rowBounds = (RowBounds) args[2];
                log.info("rowRounds is : {}", rowBounds); //？
                final ResultHandler<?> resultHandler = (ResultHandler<?>) args[3]; //？
                log.info("resultHandler is : {}", resultHandler);
                if (args.length == 6){
                    final CacheKey cacheKey = (CacheKey) args[4];
                    log.info("cacheKey is : {}", cacheKey);
                    final BoundSql boundSql = (BoundSql) args[5];
                    log.info("boundSql is : {}", boundSql);
                }
            }
            final String sql = mappedStatement.getBoundSql(parameter).getSql();
            log.info("Executing SQL : {}", sql);
        }
        return invocation.proceed();
    }
}
