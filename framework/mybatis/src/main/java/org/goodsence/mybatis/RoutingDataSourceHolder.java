package org.goodsence.mybatis;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zty
 * @apiNote
 * @project_name mybatis-demo
 * @user tain
 * @create_at 2023/6/23 9:56
 * @create_vio IntelliJ IDEA
 */
public final class RoutingDataSourceHolder implements BeanFactoryAware {

    /**
     *维持对一个动态数据源的引用
     */
    private static RoutingDataSource routingDataSource;

    /**
     * 数据源属性map, 不包含默认数据源。 一定要用ConcurrentHashMap, 否则不是线程安全。
     */
    private static final Map<String, DataSourceProperties> DATA_SOURCE_PROPERTIES_MAP = new ConcurrentHashMap<>();

    /**
     * 防止创建实例
     */
    private RoutingDataSourceHolder(){
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        RoutingDataSourceHolder.routingDataSource = beanFactory.getBean(RoutingDataSource.class);
    }

    /**
     * 数据源键集合
     * @return 数据源键集合
     */
    public static Set<String> lookupKeySet(){
        return DATA_SOURCE_PROPERTIES_MAP.keySet();
    }

    /**
     * 新增数据源
     * @param dataSourceProperties dataSourceProperties
     */
    public static void add(DataSourceProperties dataSourceProperties) {
        DATA_SOURCE_PROPERTIES_MAP.put(dataSourceProperties.getName(), dataSourceProperties);
        initialize();
    }

    private static void initialize() {
        final Map<Object, Object> targetDataSources = new ConcurrentHashMap<>(5);
        DATA_SOURCE_PROPERTIES_MAP.forEach((key, properties) -> targetDataSources.put(key, buildDataSource(properties)));
        routingDataSource.setTargetDataSources(targetDataSources);
        routingDataSource.initialize(); // getConnect()之前不会不会调用此方法吗？ 这里还需要吗 //不会，要
    }

    private static DataSource buildDataSource(DataSourceProperties dataSourceProperties) {
        return DataSourceBuilder.create()
                .driverClassName(dataSourceProperties.getDriverClassName())
                .url(dataSourceProperties.getUrl())
                .username(dataSourceProperties.getUsername())
                .password(dataSourceProperties.getPassword())
                .build();
    }

    /**
     * 删除数据源。
     * @param lookupKey lookupKey
     */
    public static void remove(String lookupKey){
        DATA_SOURCE_PROPERTIES_MAP.remove(lookupKey);
        initialize();
    }

    /**
     * 判断数据源是否存在
     * @param lookupKey lookupKey
     * @return 是否存在
     */
    public static boolean isExist(String lookupKey) {
        return DATA_SOURCE_PROPERTIES_MAP.containsKey(lookupKey);
    }

    /**
     * 切换数据源
     * @param lookupKey lookupKey
     */
    public static void determine(String lookupKey){
        routingDataSource.switchTo(lookupKey);
    }

    /**
     * 获得特定数据源属性
     * @param lookupKey lookupKey
     * @return DataSourceProperties
     * @deprecated 为什么要返回?
     */
    @Deprecated
    public static DataSourceProperties get(String lookupKey) {
        if (isExist(lookupKey)) {
            return DATA_SOURCE_PROPERTIES_MAP.get(lookupKey);
        }
        return null;
    }

    /**
     * 重置数据源为默认。
     */
    public static void reset(){
        routingDataSource.reset();
    }

}