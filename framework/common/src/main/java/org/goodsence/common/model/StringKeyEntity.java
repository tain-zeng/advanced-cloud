package org.goodsence.common.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * @author zty
 * @apiNote 基础实体类
 * @user nav1c
 * @date 2023/12/9 12:07
 * @project advanced-cloud
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StringKeyEntity extends Entity<String> {

    @Serial
    private static final long serialVersionUID = 520L;
}
