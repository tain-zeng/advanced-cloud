package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-01-26 21:26:37
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public final class CustomResourceUtils implements ResourceLoaderAware {

    private static ResourceLoader resourceLoader;

    private CustomResourceUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static Resource getResource(String resourceName){
        if (!StringUtils.hasText(resourceName)){
            return null;
        }
        if (!resourceName.contains(ResourceLoader.CLASSPATH_URL_PREFIX)){
            resourceName = ResourceLoader.CLASSPATH_URL_PREFIX + resourceName;
        }
        return resourceLoader.getResource(resourceName);
    }

    public static String getResourceContent(String resourceName) throws IOException {
        if (!StringUtils.hasText(resourceName)){
            return "";
        }
        Resource resource = getResource(resourceName);
        if (resource == null || !resource.exists()){
            return "";
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(resource.getInputStream())))) {
            StringBuilder scriptBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                scriptBuilder.append(line).append("\n");
            }
            return scriptBuilder.toString();
        }
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        CustomResourceUtils.resourceLoader = resourceLoader;
    }
}
