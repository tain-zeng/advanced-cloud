package org.goodsence.common;

import jakarta.annotation.Nonnull;

import java.util.*;

/**
 * @author tain
 * @apiNote 自定义严格的栈
 * @date 2024/10/10
 * @project modern-arranged-marriage
 */
public class CustomStack<E> implements IStack<E> {

    private final Deque<E> deque;

    public CustomStack() {
        this.deque = new LinkedList<>();
    }

    @Override
    public void push(E e) {
        deque.push(e);
    }

    @Override
    public E pop() {
        return deque.pop();
    }

    @Override
    public E peek() {
        return deque.peek();
    }

    @Override
    public boolean isEmpty() {
        return deque.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return deque.contains(o);
    }

    @Override
    public int size() {
        return deque.size();
    }

    @Override
    public void clear() {
        deque.clear();
    }

    @Nonnull
    @Override
    public Iterator<E> iterator() {
        return deque.iterator();
    }

}
