package org.goodsence.common;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-01-26 17:59:48
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public enum CustomProfiles implements CustomEnum {

    DEVELOPMENT(Constant.DEVELOP),

    PRO(Constant.PRO);

    private final String value;

    CustomProfiles(String value) {
        this.value = value;
    }

    @Override
    public String value() {
        return value;
    }

    public static class Constant {

        private Constant() {
            throw new IllegalStateException(CanNotInstantiateReason.ENUM_CLASS.descriptions());
        }

        public static final String DEVELOP = "dev";

        public static final String PRO = "pro";
    }
}
