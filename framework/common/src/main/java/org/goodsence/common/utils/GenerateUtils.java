package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.util.CollectionUtils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

/**
 * @author ChatGPT
 * @apiNote
 * @user nav1c
 * @date 2023/12/3 14:20
 * @project advanced-cloud
 */
public final class GenerateUtils {

    private static final Random RANDOM;

    static {
        try {
            RANDOM = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }

    private static final int DEFAULT_LENGTH = 10;

    private GenerateUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static String generateRandomCombination(Set<String> inputSet) {
        if (CollectionUtils.isEmpty(inputSet)){
            return "";
        }
        return generateRandomCombination(inputSet, DEFAULT_LENGTH);
    }

    public static String generateRandomCombination(Set<String> inputSet, int length) {
        if (CollectionUtils.isEmpty(inputSet) || length <= 0) {
            return "";
        }
        final StringBuilder randomCombination = new StringBuilder();

        // 确保至少包含20个元素
        while (randomCombination.length() < length) {
            // 从Set中随机选择一个元素
            final String randomElement = getRandomElement(inputSet);
            // 将选择的元素添加到随机组合中
            randomCombination.append(randomElement);
        }
        return randomCombination.toString();
    }

    public static String getRandomElement(Set<String> set) {
        if (CollectionUtils.isEmpty(set)){
            return "";
        }
        final int randomIndex = GenerateUtils.RANDOM.nextInt(set.size());
        int currentIndex = 0;
        for (String element : set) {
            if (currentIndex == randomIndex) {
                return element;
            }
            currentIndex++;
        }
        return ""; // 如果Set为空，可以返回空字符串或者其他适当的默认值
    }

    public static int generateRandomNumber(byte n) {
        if (n <= 0) {
            throw new IllegalArgumentException("位数必须是正整数");
        }
        final StringBuilder sb = new StringBuilder();
        // 生成n-1位的随机数字
        for (int i = 1; i < n; i++) {
            sb.append(GenerateUtils.RANDOM.nextInt(10)); // 生成0到9之间的随机数字
        }

        // 生成最高位，不能为0
        sb.append(GenerateUtils.RANDOM.nextInt(9) + 1); // 生成1到9之间的随机数字

        return Integer.parseInt(sb.toString());
    }

    public static String uuid(){
        return UUID.randomUUID().toString().replace("-", "");
    }
}
