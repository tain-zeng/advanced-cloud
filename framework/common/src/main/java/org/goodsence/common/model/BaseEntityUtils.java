package org.goodsence.common.model;

import org.goodsence.common.CanNotInstantiateReason;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

/**
 * <p>
 * created at <b> 2024-04-01 16:32:48 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @deprecated 直接放到BaseEntity就好了
 */
@Deprecated
public final class BaseEntityUtils {

    private static final String DEFAULT_OPERATOR = "administrator";

    private BaseEntityUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static void initBaseFieldsAtCreation(StringKeyEntity stringKeyEntity) {
        initBaseFieldsAtCreation(stringKeyEntity, DEFAULT_OPERATOR);
    }
    
    @Deprecated
    public static <T extends StringKeyEntity> T createBaseEntityAtCreation(Class<T> clazz) {
        T baseEntity = createBaseEntity(clazz);
        initBaseFieldsAtCreation(baseEntity);
        return baseEntity;
    }

    @Deprecated
    public static <T extends StringKeyEntity> T createBaseEntityAtUpdate(Class<T> clazz) {
        T baseEntity = createBaseEntity(clazz);
        initBaseFieldsAtUpdate(baseEntity);
        return baseEntity;
    }

    private static <T extends StringKeyEntity> T createBaseEntity(Class<T> clazz){
        T baseEntity;
        try {
            baseEntity = clazz.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
        return baseEntity;
    }

    private static void initBaseFieldsAtCreation(StringKeyEntity stringKeyEntity, String creator) {
        stringKeyEntity.setCreatedBy(creator);
        stringKeyEntity.setUpdatedBy(creator);
        final Date creationTime = new Date();
        stringKeyEntity.setCreatedAt(creationTime);
        stringKeyEntity.setUpdatedAt(creationTime);
        stringKeyEntity.setStatus(Status.ACTIVE.value());
        stringKeyEntity.setVersion(0);
    }

    @Deprecated
    public static <T extends StringKeyEntity> T createBaseEntity(){
        return createBaseEntity(DEFAULT_OPERATOR);
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public static <T extends StringKeyEntity> T createBaseEntity(String creator){
        StringKeyEntity stringKeyEntity = new StringKeyEntity();
        initBaseFieldsAtCreation(stringKeyEntity, creator);
        return (T) stringKeyEntity;
    }

    public static void initBaseFieldsAtUpdate(StringKeyEntity stringKeyEntity) {
        initBaseFieldsAtUpdate(stringKeyEntity, DEFAULT_OPERATOR);
    }

    @Deprecated
    public static <T extends StringKeyEntity> T createBaseEntityAtUpdate() {
        return createBaseEntityAtUpdate(DEFAULT_OPERATOR);
    }

    @Deprecated
    @SuppressWarnings("unchecked")
    public static <T extends StringKeyEntity> T createBaseEntityAtUpdate(String updater) {
        StringKeyEntity stringKeyEntity = new StringKeyEntity();
        initBaseFieldsAtCreation(stringKeyEntity, updater);
        return (T) stringKeyEntity;
    }

    public static void initBaseFieldsAtUpdate(StringKeyEntity stringKeyEntity, String updater) {
        if (stringKeyEntity.getStatus() != null){
            throw new IllegalStateException("不允许更新状态!");
        }
        stringKeyEntity.setUpdatedBy(updater);
        stringKeyEntity.setUpdatedAt(new Date());
        stringKeyEntity.setCreatedAt(null);
        stringKeyEntity.setCreatedBy(null);
    }
}
