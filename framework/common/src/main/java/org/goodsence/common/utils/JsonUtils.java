package org.goodsence.common.utils;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Objects;
import java.util.function.BinaryOperator;

/**
 * @author tain
 * @apiNote
 * @date 2024/10/11
 * @project modern-arranged-marriage
 */
public final class JsonUtils {

    private JsonUtils() {
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static String traverse(JSONObject jsonObject, BinaryOperator<String> operator) {
        if (CollectionUtils.isEmpty(jsonObject) || operator == null){
            return "";
        }
        return traverse(jsonObject, operator, "item");
    }

    /**
     * 将json转换为xml
     * @param jsonObject 任意json对象
     * @param operator 对json中的每对key-value的构建xml的操作，比如最简单构建xml的操作是:<br>
     *                 <pre>{@code (key, value) -> "<" + key + ">" + value + "</"+ key + ">" } </pre>
     * @param subArrayKeyName 如果json里面包含jsonArray, jsonArray里面又是一个jsonArray——即多维json数组, 那么第二维以后的数组转换为xml后它的标签名字是什么呢？<br>
     *                   请用这个参数<i>subArrayName</i>来告诉程序。
     * @return xml
     */
    public static String traverse(JSONObject jsonObject, BinaryOperator<String> operator, String subArrayKeyName) {
        if (CollectionUtils.isEmpty(jsonObject) || operator == null){
            return "";
        }
        final StringBuilder target = new StringBuilder();
        traverse(jsonObject, target, operator, subArrayKeyName);
        return target.toString();
    }

    private static void traverse(JSONObject jsonObject, StringBuilder sb, BinaryOperator<String> operator, String subArrayKeyName) {
        if (CollectionUtils.isEmpty(jsonObject) || sb == null || operator == null){
            return;
        }
        for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof JSONObject jsonObject1) {
                StringBuilder sb1 = new StringBuilder();
                traverse(jsonObject1, sb1, operator, subArrayKeyName);
                sb.append(operator.apply(key, sb1.toString()));
            } else if (value instanceof JSONArray jsonArray) {
                traverse(key, jsonArray, sb, operator, subArrayKeyName);
            }else {
                sb.append(operator.apply(key, Objects.toString(value, "")));
            }
        }
    }

    private static void traverse(String key, JSONArray jsonArray, StringBuilder sb, BinaryOperator<String> operator, String subArrayKeyName) {
        if (key == null || CollectionUtils.isEmpty(jsonArray) || sb == null || operator == null){
            return;
        }
        subArrayKeyName = StringUtils.hasText(subArrayKeyName) ? subArrayKeyName : key;
        for (Object item : jsonArray){
            StringBuilder sb1 = new StringBuilder();
            if (item instanceof JSONObject jsonObject){
                traverse(jsonObject, sb1, operator, subArrayKeyName);
            } else if (item instanceof JSONArray jsonArray1){
                traverse(subArrayKeyName, jsonArray1, sb1, operator, subArrayKeyName);
            }else {
                sb1.append(Objects.toString(item, ""));
            }
            sb.append(operator.apply(key, sb1.toString()));
        }
    }
}
