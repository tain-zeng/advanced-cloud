package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;

import java.io.*;

/**
 * @author ChatGPT
 * @apiNote 序列化工具类
 * @user nav1c
 * @date 2023/11/29 23:22
 * @project advanced-cloud
 */
public final class SerializedUtils {

    private SerializedUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static <T> T bytesToObject(byte[] bytes, Class<T> tClass) throws IOException, ClassNotFoundException {
        Object o = bytesToObject(bytes);
        if (tClass.isInstance(o)) {
            return tClass.cast(o);
        }
        throw new ClassCastException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static Object bytesToObject(byte[] bytes) throws IOException, ClassNotFoundException {
        if (bytes == null) {
            return null;
        }
        Object obj;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInputStream ois = new ObjectInputStream(bis)) {
            obj = ois.readObject();
        }
        return obj;
    }

    public static byte[] objectToBytes(Serializable obj) throws IOException {
        if (obj == null) {
            return new byte[0];
        }
        byte[] bytes;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray();
        }
        return bytes;
    }
}
