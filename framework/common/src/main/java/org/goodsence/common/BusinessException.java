package org.goodsence.common;

/**
 * @Author zty
 * @apiNote 业务异常
 * @Version 1.0.0
 * @Date 2023 02 15 14 05
 **/
public class BusinessException extends RuntimeException {

    public BusinessException(String msg){
        super(msg);
    }
}
