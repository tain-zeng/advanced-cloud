package org.goodsence.common.model;

/**
 * <p>
 * created at <b> 2024-04-04 02:29:32 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
public interface StringKeyDao<T extends StringKeyEntity> extends BaseDao<String, T> {
}
