package org.goodsence.common.aspect;

import org.aspectj.lang.JoinPoint;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-11 09:46:02
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public interface CommonAspectPattern extends AspectPattern {

    /**
     * {@link org.springframework.aop.framework.adapter.MethodBeforeAdviceInterceptor}
     *0
     */
    default void before(JoinPoint joinPoint){
    }

    /**
     * 1-1
     */
    default Object afterReturning(JoinPoint joinPoint, Object result) {
        return result;
    }

    /**
     * 1-2
     */
    default Object afterThrowing(JoinPoint joinPoint, Throwable e) throws Throwable {
        throw e;
    }

    /**
     * 2
     */
    default void after(JoinPoint joinPoint) {
    }

}
