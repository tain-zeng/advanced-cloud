package org.goodsence.common;

/**
 * @author tain
 * @apiNote 自定义缓存
 * @date 2024/10/12
 * @project modern-arranged-marriage
 */
public interface KvOperations {

    Object get(String key);

    void put(String key, Object value);

    void put(String key, Object value, long ttl);

    void remove(String key);

    boolean contains(String key);

    void clear();
}
