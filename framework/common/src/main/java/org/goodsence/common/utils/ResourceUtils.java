package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;

@Deprecated
public final class ResourceUtils {

    private ResourceUtils() {
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static Set<String> fetchPermitList(String resourcesFilename, String key) throws IOException {
        if (!StringUtils.hasText(resourcesFilename) || !resourcesFilename.endsWith(".yaml") || !StringUtils.hasText(key)) {
            return Collections.emptySet();
        }
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        final Resource[] resources = resolver.getResources( ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resourcesFilename);
        return getPropertiesList(key, resources);
    }

    private static Set<String> getPropertiesList(String key, Resource... resources){
        if (!StringUtils.hasText(key) || resources == null){
            return Collections.emptySet();
        }
        final Set<String> list = new LinkedHashSet<>();
        for(Resource resource : resources) {
            final Properties properties = loadYamlProperties(resource);
            if (properties == null){
                continue;
            }
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                //todo 换工具
                final String tmpKey = "StringUtils.substringBefore(entry.getId().toString(), " + "[" +")";
                if(tmpKey.equalsIgnoreCase(key)){
                    list.add(entry.getValue().toString());
                }
            }
        }
        return list;
    }

    private static Properties loadYamlProperties(Resource... resources) {
        if (resources == null) {
            return new Properties();
        }
        final YamlPropertiesFactoryBean factory = new YamlPropertiesFactoryBean();
        factory.setResources(resources);
        /* factory.setSingleton(true); */ //NOSONAR
        factory.afterPropertiesSet();
        return factory.getObject();
    }
}
