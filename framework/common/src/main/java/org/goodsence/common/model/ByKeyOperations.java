package org.goodsence.common.model;

import jakarta.validation.constraints.NotNull;

import java.io.Serializable;

/**
 * <p>
 * created at <b> 2024-04-07 03:50:53 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 根据id进行操作
 */
public interface ByKeyOperations<K extends Serializable> {

    /**
     * 物理删除
     * @param id 实体id
     */
    void delete(@NotNull K id);

    /**
     * 失效
     * @param id 实体id
     */
    default void deactivate(@NotNull K id) {
        changeStatus(id, Status.DEACTIVATED.value());
    }

    /**
     * 激活
     * @param id 实体id
     */
    default void activate(@NotNull K id) {
        changeStatus(id, Status.ACTIVE.value());
    }

    /**
     * 逻辑删除
     * @param id 实体id
     */
    default void logicalDelete(@NotNull K id) {
        changeStatus(id, Status.DELETED.value());
    }

    /**
     * 恢复逻辑删除
     * @param id 实体id
     */
    default void recover(@NotNull K id) {
        changeStatus(id, Status.ACTIVE.value());
    }

    void changeStatus(K id, Integer status);
}
