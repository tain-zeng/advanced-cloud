package org.goodsence.common.annotation;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/13
 * @project advanced-cloud
 */
public @interface PageSelect {
}
