package org.goodsence.common.utils;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.util.StringUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

/**
 * @author ChatGPT
 * @apiNote
 * @date 2024/10/28
 * @project advanced-cloud
 */
public final class JSONSchemaUtils {

    private static final String PROPERTIES_KEY = "properties";

    private JSONSchemaUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static void convertJsonSchemaToCsv(String jsonSchemaStr, String csvFilePath) {
        JSONObject jsonSchema = JSONUtil.parseObj(jsonSchemaStr);
        try (FileWriter csvWriter = new FileWriter(csvFilePath)) {
            // 写入 CSV 标头
            csvWriter.append("字段名,数据类型,字段说明\n");
            JSONObject properties = jsonSchema.getJSONObject(PROPERTIES_KEY);
            // 递归处理 JSON Schema 中的字段
            processProperties(properties, "", csvWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processProperties(JSONObject properties, String parentField, FileWriter csvWriter) throws IOException {
        Set<String> keys = properties.keySet();

        for (String fieldName : keys) {
            JSONObject fieldProperties = properties.getJSONObject(fieldName);

            // 构建完整字段名（包括父路径）
            String fullFieldName = StringUtils.hasText(parentField) ? parentField + "." + fieldName : fieldName;

            // 如果字段类型是对象，则递归处理子属性   "object".equals(fieldType) &&
            if (fieldProperties.containsKey(PROPERTIES_KEY)) {
                JSONObject nestedProperties = fieldProperties.getJSONObject(PROPERTIES_KEY);
                processProperties(nestedProperties, fullFieldName, csvWriter);
            }else {
                String fieldType = fieldProperties.getStr("type", "unknown");
                String fieldDescription = fieldProperties.getStr("description", "");
                csvWriter.append(fullFieldName).append(",");
                csvWriter.append(fieldType).append(",");
                csvWriter.append(fieldDescription).append("\n");
            }
        }
    }
}
