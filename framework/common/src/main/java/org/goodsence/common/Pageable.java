package org.goodsence.common;

import lombok.Data;

/**
 * <p>
 * created at <b> 2024-04-04 03:24:29 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
@Data
public abstract class Pageable { //设置为抽象类是为了不让创建实例

    private Integer pageNum;

    private Integer pageSize;
}
