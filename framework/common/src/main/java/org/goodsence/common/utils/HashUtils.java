package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.util.StringUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author ChatGPT
 * @apiNote 哈希工具
 * @user nav1c
 * @date 2023/12/5 19:45
 * @project advanced-cloud
 */
public final class HashUtils {

    private HashUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    /**
     * 使用SHA-256进行不可逆加密
     * @param plainText 明文
     * @return 密文
     */
    public static String hash(String plainText) {
        if (!StringUtils.hasText(plainText)){
            return "";
        }
        final MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
        byte[] hashedBytes = md.digest(plainText.getBytes());
        // 转换为十六进制表示
        final StringBuilder sb = new StringBuilder();
        for (byte b : hashedBytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    /**
     * 验证明文加密后是否与密文相等
     * @param plainText 明文
     * @param hashedText 密文
     * @return 明文加密后是否与密文相等
     */
    public static boolean verify(String plainText, String hashedText) {
        if (!StringUtils.hasText(plainText)){
            return !StringUtils.hasText(hashedText);
        }
        final String newHashedPassword = hash(plainText);
        return Arrays.equals(hashedText.getBytes(), newHashedPassword.getBytes());
    }
}
