package org.goodsence.common.model;

import org.goodsence.common.CustomEnum;

/**
 * <p>
 * created at <b> 2024-03-29 16:35:19 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 实体状态
 */
public enum Status implements CustomEnum {
    /**
     * 激活，正常使用
     */
    ACTIVE(0, "正常"),
    /**
     * 删除
     */
    DELETED(1, "逻辑删除"),
    /**
     * 停用
     */
    DEACTIVATED(2, "停用");

    private final int value;

    private final String descriptions;

    Status(int value, String descriptions) {
        this.value = value;
        this.descriptions = descriptions;
    }

    @Override
    public Integer value() {
        return value;
    }

    @Override
    public String descriptions() {
        return descriptions;
    }

    public static Status valueOf(Object value){
        return CustomEnum.valueOf(Status.values(), value);
    }
}
