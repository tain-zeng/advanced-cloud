package org.goodsence.common.utils;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.StringReader;
import java.util.Objects;
import java.util.Set;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-02-29 19:32:09
 * @project advance-cloud
 * @kit IntelliJ IDEA
 */
public final class XmlUtils {

    private static final String KEY_REQUIRE_HAS_TEXT_MSG = "键不能为空!";

    private XmlUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static String wrap(@NonNull String key,  String value) {
        Assert.hasText(key, KEY_REQUIRE_HAS_TEXT_MSG);
        return wrap(null, key, value);
    }

    public static String wrap(String label, @NonNull String key, String value) {
        Assert.hasText(key, KEY_REQUIRE_HAS_TEXT_MSG);
        return wrap(label, key, value, null);
    }

    public static String wrap(String label, @NonNull String key, String value, String addition) {
        Assert.hasText(key, KEY_REQUIRE_HAS_TEXT_MSG);
        label = StringUtils.hasText(label) ? label + ":" : "";
        addition = StringUtils.hasText(addition) ? " " + addition : "";
        return "<" + label + key + addition + ">" + Objects.toString(value, "") + "</" + label + key + ">";
    }

    public static String convertToXml(JSONObject jsonObject) {
        if (CollectionUtils.isEmpty(jsonObject)){
            return "";
        }
        return JsonUtils.traverse(jsonObject, XmlUtils::wrap);
    }

    /**
     * 将xml转换为json
     * @param xml xml
     * @param arrayFields 数组转换配置，xml转换为json的过程中，程序无法识别xml中的某个元素是否是数组，如果该元素是数组，请用该参数告诉程序 如:</br>
     *                    <pre>
     *                    {@code
     *                    Set<String> arrayFields = Collections.singleton("wives");
     *                    }</pre>
     * @return JSONObject
     */
    public static JSONObject convertToJson(String xml, Set<String> arrayFields) {
        if (!StringUtils.hasText(xml)){
            return new JSONObject();
        }
        Assert.state(xml.startsWith("<") && xml.endsWith(">"), "xml格式不正确: " + xml);
        final Document document;
        try {
            SAXReader reader = SAXReader.createDefault();
            document = reader.read(new StringReader(xml));
        } catch (DocumentException e) {
            throw new IllegalStateException("读取xml失败:" + e.getMessage());
        }
        final Element root = document.getRootElement();
        return convertToJson(root, arrayFields);
    }

    private static JSONObject convertToJson(Element element, Set<String> arrayFields) {
        final JSONObject jsonObject = new JSONObject();
        if (element == null){
            return jsonObject;
        }
        for (Element child : element.elements()) {
            final String key = child.getName();
            final Object value = child.isTextOnly() ? child.getText() : convertToJson(child, arrayFields);
            final boolean isArray = arrayFields != null && arrayFields.contains(key);
            if (isArray){
                if (jsonObject.containsKey(key)){
                    JSONArray jsonArray = (JSONArray)jsonObject.get(key);
                    jsonArray.add(value);
                    jsonObject.replace(key, jsonArray);
                } else {
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.add(value);
                    jsonObject.set(key, jsonArray);
                }
            } else {
                jsonObject.set(key, value);
            }
        }
        return jsonObject;
    }
}
