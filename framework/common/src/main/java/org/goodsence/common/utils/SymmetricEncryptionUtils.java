package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * @author zty
 * @apiNote 对称加密工具类
 * @user nav1c
 * @date 2023/12/4 17:49
 * @project advanced-cloud
 */
public final class SymmetricEncryptionUtils {

    private static final String ALGORITHM = "AES";

    private static final String TRANSFORMATION = "AES/ECB/PKCS5Padding";

    private SymmetricEncryptionUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    /**
     * 加密字符串
     * @param data 待加密的字符串
     * @param key  加密密钥
     * @return 加密后的字符串
     */
    public static String encrypt(String data, String key) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        final byte[] encryptedBytes = cipher.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    /**
     * 解密字符串
     * @param encryptedData 加密后的字符串
     * @param key           加密密钥
     * @return 解密后的字符串
     */
    public static String decrypt(String encryptedData, String key) throws NoSuchPaddingException,
            NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        final SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), ALGORITHM);
        final Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        final byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(encryptedData));
        return new String(decryptedBytes);
    }
}
