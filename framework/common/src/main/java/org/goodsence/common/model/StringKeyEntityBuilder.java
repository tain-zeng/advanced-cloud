package org.goodsence.common.model;

/**
 * @author tain
 * @apiNote 实体建造器
 * @date 2024/9/29
 * @project modern-arranged-marriage
 */
public class StringKeyEntityBuilder<T extends StringKeyEntity> extends EntityBuilder<String, T> {

    public StringKeyEntityBuilder(Class<T> tClazz) {
        super(tClazz);
    }
}
