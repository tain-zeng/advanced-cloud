package org.goodsence.common.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-11 21:38:47
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Component
@Aspect
public class ExecuteTimeCalculateAspect implements AroundAspectPattern{

    private static final Logger log = LoggerFactory.getLogger(ExecuteTimeCalculateAspect.class);

    @Pointcut("@annotation(org.goodsence.common.annotation.ExecuteTimeCalculate)")
    @Override
    public void pointcut() {

    }

    @Around("pointcut()")
    @Override
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch stopWatch = new StopWatch(); //非线程安全!
        stopWatch.start(joinPoint.getSignature().getName());
        Object result;
        try {
            result = joinPoint.proceed();
        }finally {
            stopWatch.stop();
            log.info("执行[{}]花费时间{}秒!", stopWatch.lastTaskInfo().getTaskName(), stopWatch.getTotalTimeSeconds());
        }
        return result;
    }

}
