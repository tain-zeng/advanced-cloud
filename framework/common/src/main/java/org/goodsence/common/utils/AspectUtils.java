package org.goodsence.common.utils;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.goodsence.common.CanNotInstantiateReason;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-14 14:20:05
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public final class AspectUtils {

    private AspectUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static <T extends Annotation> T getMethodAnnotationObject(JoinPoint joinPoint, Class<T> annotationClass){
        return getMethodSignature(joinPoint).getMethod().getAnnotation(annotationClass);
    }

    /**
     * 获取方法入参, 注意不是获取注解的值
     */
    public static Map<String, Object> getMethodParameter(JoinPoint joinPoint){
        MethodSignature methodSignature = getMethodSignature(joinPoint);
        String[] parameterNames = methodSignature.getParameterNames();
        Object[] args = joinPoint.getArgs();
        Map<String, Object> params = new LinkedHashMap<>();
        for (int i = 0; i < parameterNames.length; i++){
            params.put(parameterNames[i], args[i]);
        }
        return Collections.unmodifiableMap(params);
    }

    public static <T extends Annotation> Object getArgWithAnnotation(JoinPoint joinPoint, Class<T> annotationClass) {
        MethodSignature methodSignature = getMethodSignature(joinPoint);
        Parameter[] parameters = methodSignature.getMethod().getParameters();
        Object[] args = joinPoint.getArgs();
        for (int i = 0; i < parameters.length; i++){
            if (parameters[i].isAnnotationPresent(annotationClass)){
                return args[i];
            }
        }
        return null;
    }

    public static MethodSignature getMethodSignature(JoinPoint joinPoint){
        Signature signature = joinPoint.getSignature();
        if (signature instanceof MethodSignature methodSignature) {
            return methodSignature;
        }
        throw new IllegalStateException();
    }
}
