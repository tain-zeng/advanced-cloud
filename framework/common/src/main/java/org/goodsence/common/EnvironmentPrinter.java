package org.goodsence.common;

import org.goodsence.common.utils.IpUtils;
import org.springframework.core.env.Environment;
import org.springframework.core.env.EnvironmentCapable;

import java.net.SocketException;
import java.util.Objects;

/**
 * @author zty
 * @apiNote
 * @user nav1c
 * @date 2023/11/29 20:24
 * @project advanced-cloud
 */
public final class EnvironmentPrinter {

    private EnvironmentPrinter(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static void printInfo(EnvironmentCapable environmentCapable) {
        final Environment env = environmentCapable.getEnvironment();
        final String protocol = "http://";
        final String ip;
        try {
            ip = IpUtils.getLocalIp();
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        final String port = env.getProperty("server.port");
        final String contextPath = env.getProperty("server.servlet.context-path");
        final String path = Objects.equals("/", contextPath) ? "" : contextPath;
        final String basePath = protocol + ip + ":" + port + path ;
        final String printInfo = "\n\t" +
                "----------------------------------------------------------\n\t" +
                "Application is running! Access URLs:\n\t" +
                "Local: \t\t\t" + protocol + "localhost:" + port + path + "/\n\t" +
                "External: \t\t" + basePath + "/\n\t" +
                "swagger-ui: \t" + basePath + "/swagger-ui/index.html\n\t" +
                "------------------------------------------------------------";
        System.out.println(printInfo);//NOSONAR
    }
}
