package org.goodsence.common;

/**
 * @author zty
 * @apiNote 不能实例化的原因
 * @user nav1c
 * @date 2023/9/6 14:03
 * @project advanced-cloud
 */
public enum CanNotInstantiateReason implements CustomEnum {

    ENUM_CLASS("Enum Class", "枚举类不能创建实例！"),

    UTILITY_CLASS("Utility Class", "工具类不能创建实例！");

    private final String value;
    private final String descriptions;

    CanNotInstantiateReason(String value, String descriptions) {
        this.value = value;
        this.descriptions = descriptions;
    }

    @Override
    public String descriptions() {
        return descriptions;
    }

    @Override
    public String value() {
        return value;
    }

    public static CanNotInstantiateReason valueOf(Object value){
        return CustomEnum.valueOf(CanNotInstantiateReason.values(), value);
    }
}
