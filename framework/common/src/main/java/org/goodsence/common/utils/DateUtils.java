package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.util.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author zty
 * @apiNote 日期工具类
 * @user nav1c
 * @date 2023/11/30 19:17
 * @project advanced-cloud
 */
public final class DateUtils {
    private static final String TIME_FORMAT_REQUIRED_HAS_TEXT_MSG = "时间格式不能为空!";

    private static final String TIME_REQUIRED_HAS_TEXT_MSG = "时间不能为空!";
    //前向赋值
    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
    public static final String DEFAULT_DATETIME_PATTERN = DEFAULT_DATE_PATTERN + " " + "HH:mm:ss";
    public static final String DEFAULT_PATTERN = DEFAULT_DATETIME_PATTERN;


    private DateUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static Date toDate(String str) throws ParseException {
        Assert.hasText(str, TIME_REQUIRED_HAS_TEXT_MSG);
        return toDate(str, DEFAULT_PATTERN);
    }

    public static Date toDate(String str, String pattern) throws ParseException {
        Assert.hasText(pattern, TIME_FORMAT_REQUIRED_HAS_TEXT_MSG);
        Assert.hasText(str, TIME_REQUIRED_HAS_TEXT_MSG);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.parse(str);
    }

    public static String now(){
        return now(DEFAULT_PATTERN);
    }

    public static String now(String pattern){
        Assert.hasText(pattern, TIME_FORMAT_REQUIRED_HAS_TEXT_MSG);
        return toString(new Date(), pattern);
    }

    public static Date daysAgo(int n) {
        // 获取当前日期
        LocalDate currentDate = LocalDate.now();
        // 往前推n天
        LocalDate resultDate = currentDate.minusDays(n);
        // 将LocalDate转换为Date
        return Date.from(resultDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static String toString(Date date, String pattern) {
        Assert.notNull(date, TIME_REQUIRED_HAS_TEXT_MSG);
        Assert.hasText(pattern, TIME_FORMAT_REQUIRED_HAS_TEXT_MSG);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }
}
