package org.goodsence.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * <p>
 * created at <b> 2024-04-04 04:58:25 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
public interface BaseDao<K extends Serializable, T extends ModelBase<K>> extends ByKeyOperations<K> {

    /**
     * 选择性插入
     * @param entity 实体
     */
    void insertSelective(T entity);

    /**
     * 选择性更新
     * @param entity 实体
     */
    void updateByPrimaryKeySelective(T entity);

    /**
     * 根据主键查找记录
     * @param key 主键
     * @return 实体
     */
    T selectByPrimaryKey(K key);

    /**
     * 批量删除
     * @param entities 主键
     */
    default void insertSelectiveBatch(Set<T> entities) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * 批量更新
     * @param entities 主键
     */
    default void updateByPrimaryKeySelectiveBatch(Set<T> entities) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * 批量删除
     * @param key 主键集合
     */
    default void deleteByPrimaryKeyBatch(Set<K> key){
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @SuppressWarnings("unchecked")
    @Override
    default void changeStatus(K id, Integer status) {
        ModelBase<K> modelBase = new ModelBase<>() {
            @Override
            public K getId() {
                return id;
            }

            @Override
            public Integer getStatus() {
                return status;
            }

            @Override
            public Date getUpdatedAt() {
                return new Date();
            }
        };
        updateByPrimaryKeySelective((T) modelBase);
    }

    default T selectByName(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
