package org.goodsence.common;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author tain
 * @apiNote
 * @date 2024/10/14
 * @project modern-arranged-marriage
 */
public interface IStack<E> extends Collection<E> {

    void push(E e);

    E pop();

    E peek();

    @Override
    default <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException();
    }

    @Override
    default boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @Override
    default boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    default boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    default boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    default boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    default boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    default Iterator<E> iterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    default Object[] toArray() {
        throw new UnsupportedOperationException();
    }
}
