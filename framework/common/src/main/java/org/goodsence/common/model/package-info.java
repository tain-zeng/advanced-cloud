/**
 * @apiNote 实体相关基类
 * @author zengty
 * @date 2024/12/16
 * @project advanced-cloud
 */
package org.goodsence.common.model;