package org.goodsence.common.aspect;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-11 21:33:21
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public interface AroundAspectPattern extends AspectPattern {

    default Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        return joinPoint.proceed();
    }

}
