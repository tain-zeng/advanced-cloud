package org.goodsence.common;

import java.util.Objects;

/**
 * <p>
 * created at <b> 2024-04-15 01:24:43 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @deprecated 直接放到CustomEnum就好了
 */
@Deprecated
public final class EnumUtils {

    private EnumUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static <T extends CustomEnum> T valueOf(T[] customEnums, Object value) {
        if (Objects.isNull(value)){
            return null;
        }
        for (T t : customEnums) {
            if (Objects.equals(t.value(), value)) {
                return t;
            }
        }
        return null;
    }
}
