package org.goodsence.common.aspect;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.Banner;
import org.springframework.boot.DefaultBootstrapContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-11 21:34:58
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public interface AspectPattern {

    /**
     * 切点表达式可以包含参数, 满足特点条件的参数才会进入切点?
     * spring在启动时创建bean的时候已经生成代理对象了？<br>
     * {@link org.springframework.boot.SpringApplication#prepareContext(DefaultBootstrapContext, ConfigurableApplicationContext, ConfigurableEnvironment, SpringApplicationRunListeners, ApplicationArguments, Banner)}
     * {@link org.springframework.boot.SpringApplication#applyInitializers(ConfigurableApplicationContext)}
     * {@link org.springframework.boot.context.ContextIdApplicationContextInitializer#initialize(ConfigurableApplicationContext)}
     * {@link org.springframework.beans.factory.support.DefaultListableBeanFactory#registerSingleton(String, Object)}
     * {@link org.springframework.beans.factory.support.DefaultSingletonBeanRegistry#registerSingleton(String, Object)}
     */
    void pointcut();

}
