package org.goodsence.common.utils;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-02-07 19:21:27
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public class InjectionException extends RuntimeException {
    public InjectionException(String keyword) {
        super("存在sql注入: " + keyword);
    }
}
