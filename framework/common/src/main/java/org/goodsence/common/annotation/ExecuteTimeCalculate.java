package org.goodsence.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-11 21:40:00
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ExecuteTimeCalculate {
}
