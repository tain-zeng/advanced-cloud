package org.goodsence.common.autoconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/16
 * @project advanced-cloud
 */
@Configuration
public class TaskConfig {

    @Value("${thread-pool.core-pool-size:0}")
    private Integer corePoolSize;

    @Value("${thread-pool.max-pool-size:0}")
    private Integer maxPoolSize;

    @Value("${thead-pool.queue-capacity:0}")
    private Integer queueCapacity;

    @Value("${thead-pool.keep-alive-seconds:60}")
    private Integer keepAliveSeconds;

    private final int availableProcessors;

    public TaskConfig() {
        availableProcessors = Runtime.getRuntime().availableProcessors();
    }

    /**
     * Spring 的 SchedulingTaskExecutor 通常内部会使用 ScheduledExecutorService 作为实际的任务执行器?
     * 原生的
     */
    @Deprecated
    public ScheduledExecutorService scheduledExecutorService() {
        return new ScheduledThreadPoolExecutor(5);
    }

    /**
     * 定时任务
     */
    @Bean
    @ConditionalOnMissingBean(TaskScheduler.class)
    public TaskScheduler threadPoolTaskScheduler() {
        if (corePoolSize == 0) {
            corePoolSize = availableProcessors;
        }
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(corePoolSize);
        scheduler.setThreadNamePrefix("ThreadPoolTaskScheduler-");
        scheduler.initialize();
        return scheduler;
    }

    /**
     * 没有任务分配时，核心线程会被挂起，不会占用cpu <br>
     * 线程池配置应根据任务是cpu密集型还是io密集型来配置
     */
    @Bean
    @ConditionalOnMissingBean(TaskExecutor.class)
    public TaskExecutor threadPoolTaskExecutor() {
        if (corePoolSize == 0){
            corePoolSize = availableProcessors;
        }
        if (maxPoolSize == 0){
            maxPoolSize = availableProcessors*2;
        }
        if (queueCapacity == 0){
            queueCapacity = availableProcessors*256;
        }
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setKeepAliveSeconds(keepAliveSeconds);//非核心线程池的空闲存活时间
        executor.setThreadNamePrefix("ThreadPoolTaskExecutor-");
        executor.initialize();
        return executor;
    }

}
