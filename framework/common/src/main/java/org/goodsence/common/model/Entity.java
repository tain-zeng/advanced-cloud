package org.goodsence.common.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>
 * created at <b> 2024-04-04 04:57:21 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
@EqualsAndHashCode(callSuper = true)
@Data
public abstract class Entity<K extends Serializable> extends ModelBase<K> {

    /**
     * 名字
     */
    private String name;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 组织id
     */
    private Integer orgId;

    @Serial
    private static final long serialVersionUID = 520L;
}
