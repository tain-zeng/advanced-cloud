package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-02-07 14:08:00
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public final class InjectionCheckUtils {

    private static final Set<String> DDL_KEYWORD;

    private static final Set<String> TCL_KEYWORD;

    private static final Set<String> DML_KEYWORD;

    private static final Set<String> DQL_KEYWORD;

    private static final Set<String> OTHER_KEYWORD;

    private static final Set<String> SQL_KEYWORD;

    static {
        DML_KEYWORD = Set.of("INSERT", "UPDATE", "DELETE");
        TCL_KEYWORD = Set.of("GRANT", "ROLLBACK", "COMMIT", "SAVEPOINT");
        DDL_KEYWORD = Set.of("CREATE", "ALTER", "DROP", "TRUNCATE");
        DQL_KEYWORD = Set.of("SELECT");
        OTHER_KEYWORD = Set.of(",", "--", "AND", "WHERE", "OR", "IN", ";", "'", "\"", "JOIN");
        final Set<String> sqlKeyword = new HashSet<>();
        sqlKeyword.addAll(DDL_KEYWORD);
        sqlKeyword.addAll(TCL_KEYWORD);
        sqlKeyword.addAll(DML_KEYWORD);
        sqlKeyword.addAll(DQL_KEYWORD);
        sqlKeyword.addAll(OTHER_KEYWORD);
        SQL_KEYWORD = Collections.unmodifiableSet(sqlKeyword);
    }

    private InjectionCheckUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static void check(String text) throws InjectionException {
        if (!StringUtils.hasText(text)) {
            return;
        }
        final String upperText = text.toUpperCase(Locale.ROOT);
        for (String keyword : SQL_KEYWORD) {
            if (upperText.contains(keyword)){
                throw new InjectionException(keyword);
            }
        }
    }
}
