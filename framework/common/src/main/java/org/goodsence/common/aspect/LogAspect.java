package org.goodsence.common.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.goodsence.common.utils.AspectUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-07 19:50:51
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Slf4j
@Component
@Aspect
public class LogAspect implements CommonAspectPattern {
    //切面对于静态方法是无效的。
    //@annotation(org.springframework.transaction.annotation.Transactional)//匹配特定注解
    //execution(* com.example.service.MyService.*(String)) 匹配特定参数类型的方法：
    //execution(public * com.example.service.*.*(..)) 匹配特定返回类型的方法：
    //execution(* com.example.service.MyService.*(..)) 匹配特定类中的所有方法
    //execution(* com.example.service.*.*(..)) 匹配特定包下的所有方法

    @Override
    @Pointcut("@annotation(org.goodsence.common.annotation.CustomLog)")
    public void pointcut(){
    }

    @Override
    @Before("pointcut()")
    public void before(JoinPoint joinPoint){
        Map<String, Object> methodParameter = AspectUtils.getMethodParameter(joinPoint);
        log.info("入参: {}", methodParameter);
    }

    @Override
    @AfterReturning(pointcut = "pointcut()", returning = "result")
    public Object afterReturning(JoinPoint joinPoint, Object result){
        log.info("返回结果: {}", result);
        return result;
    }

    @Override
    @AfterThrowing(pointcut = "pointcut()", throwing = "e")
    public Object afterThrowing(JoinPoint joinPoint, Throwable e) throws Throwable {
        log.error("发生异常!", e);
        throw e;
    }

    @Override
    @After("pointcut()")
    public void after(JoinPoint joinPoint){
        log.info("结束!");
    }

}
