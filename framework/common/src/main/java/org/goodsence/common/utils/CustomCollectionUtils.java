package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-20 14:06:47
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public final class CustomCollectionUtils {

    private CustomCollectionUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static <T> List<T> toList(Set<T> set){
        return set.stream().toList();
//        if (set instanceof LinkedHashSet<T> linkedHashSet){
//            return new LinkedList<>(linkedHashSet);
//        }
//        return new ArrayList<>(set);
    }

    public static <T> Set<T> toSet(List<T> list) {
        return new HashSet<>(list);
//        if (list instanceof LinkedList<T> linkedList) {
//            return new LinkedHashSet<>(linkedList);
//        }
//        return new HashSet<>(list);
    }

    public static String[] toStrArray(Collection<String> collection){
        return collection.toArray(new String[0]);
    }

    public static <T> Set<T> toSet(T[] ts){
        return Arrays.stream(ts).collect(Collectors.toSet());
//        Set<T> tSet = new LinkedHashSet<>(ts.length);
//        Collections.addAll(tSet, ts);
//        return tSet;
    }

    public static <T> List<T> toList(T[] ts) {
        return Arrays.stream(ts).toList();
//        List<T> list = new LinkedList<>();
//        Collections.addAll(list, ts);
//        return list;
    }
}
