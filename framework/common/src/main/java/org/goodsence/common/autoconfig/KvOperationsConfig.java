package org.goodsence.common.autoconfig;

import org.goodsence.common.KvOperations;
import org.goodsence.common.InMemoryKvOperations;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tain
 * @apiNote
 * @date 2024/12/2
 * @project advanced-cloud
 */
@Configuration
public class KvOperationsConfig {

    @Bean
    @ConditionalOnMissingBean(KvOperations.class)
    public KvOperations customCache() {
        return new InMemoryKvOperations();
    }
}
