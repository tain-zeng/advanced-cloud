package org.goodsence.common.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class Relation<K extends Serializable> extends ModelBase<K> {
}
