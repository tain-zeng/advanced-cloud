package org.goodsence.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 学习
 * @date 2024-03-12 16:56:58
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Learn {
}
