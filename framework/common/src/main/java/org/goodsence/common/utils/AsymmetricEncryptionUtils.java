package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.util.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.util.Base64;

/**
 * @author zty
 * @apiNote 非对称加密工具类
 * @user nav1c
 * @date 2023/12/4 18:08
 * @project advanced-cloud
 */
public final class AsymmetricEncryptionUtils {

    private static final String ALGORITHM = "RSA";

    private static final int KEY_SIZE = 2048;

    private AsymmetricEncryptionUtils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    /**
     * 生成密钥对
     * @return KeyPair 密钥对
     * @throws NoSuchAlgorithmException NoSuchAlgorithmException
     */
    public static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        final KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
        keyPairGenerator.initialize(KEY_SIZE);
        return keyPairGenerator.generateKeyPair();
    }

    /**
     * 公钥加密
     * @param plaintext 明文
     * @param key 公钥
     * @return 密文
     */
    public static String encrypt(String plaintext, Key key) throws NoSuchPaddingException,
            NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        if (!StringUtils.hasText(plaintext)){
            return "";
        }
        final Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        final byte[] encryptedBytes = cipher.doFinal(plaintext.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    /**
     * 私钥解密
     * @param ciphertext 密文
     * @param key 私钥
     * @return 明文
     */
    public static String decrypt(String ciphertext, Key key) throws NoSuchPaddingException,
            NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        if (!StringUtils.hasText(ciphertext)){
            return "";
        }
        final byte[] encryptedBytes = Base64.getDecoder().decode(ciphertext);
        final Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);
        final byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
        return new String(decryptedBytes);
    }
}
