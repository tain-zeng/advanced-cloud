package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Enumeration;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/26
 * @project advanced-cloud
 */
public final class IpUtils {

    private IpUtils() {
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static String getExternalIp() {
        HttpURLConnection connection;
        try {
            connection = getHttpURLConnection();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try(InputStream inputStream = connection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream))) {
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            connection.disconnect();
            return content.toString().trim();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static HttpURLConnection getHttpURLConnection() throws IOException {
        URL url = new URL("https://ipecho.net/plain");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        return connection;
    }

    public static String getLocalIp() throws SocketException {
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface networkInterface = networkInterfaces.nextElement();
            Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
            while (inetAddresses.hasMoreElements()) {
                InetAddress inetAddress = inetAddresses.nextElement();
                if (!inetAddress.isLoopbackAddress() && inetAddress.isSiteLocalAddress() && inetAddress.getHostAddress().indexOf(':') == -1) {
                    return inetAddress.getHostAddress();  // 返回本机的有效 IP 地址
                }
            }
        }
        return null;  // 如果没有找到有效的 IP 地址
    }
}
