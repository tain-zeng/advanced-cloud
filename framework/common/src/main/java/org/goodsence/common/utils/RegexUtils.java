package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;
import org.goodsence.common.RegexPattern;
import org.springframework.util.Assert;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class RegexUtils {

    private static final String TEXT_REQUIRED_NOT_NULL_MSG = "输入文本不能为空!";

    /**
     * 不允许创建对象
     */
    private RegexUtils() {
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    /**
     * 是否手机号
     * @param str str
     * @return 是否手机号
     */
    public static boolean isMobilePhone(String str){
        Assert.notNull(str, TEXT_REQUIRED_NOT_NULL_MSG);
        return isMatches(RegexPattern.MOBILE_PHONE.value(), str);
    }

    /**
     * 是否邮箱
     * @param str str
     * @return 是否邮箱
     */
    public static boolean isEmail(String str) {
        Assert.notNull(str, TEXT_REQUIRED_NOT_NULL_MSG);
        return isMatches(RegexPattern.EMAIL.value(), str);
    }

    /**
     * 是否身份证
     * @param str str
     * @return 是否身份证
     */
    public static boolean isIdentity(String str) {
        Assert.notNull(str, TEXT_REQUIRED_NOT_NULL_MSG);
        return isMatches(RegexPattern.IDENTITY.value(), str);
    }

    /**
     * 是否密码4
     * @param str str
     * @return 是否密码4
     */
    public static boolean isPassword4(String str){
        Assert.notNull(str, TEXT_REQUIRED_NOT_NULL_MSG);
        return isMatches(RegexPattern.COMPLEX_4.value(), str);
    }

    public static boolean isMatches(String regex, String target){
        Assert.hasText(regex, "表达式不能为空!");
        Assert.notNull(target, TEXT_REQUIRED_NOT_NULL_MSG);
        final Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(target);
        return matcher.matches();
    }

}
