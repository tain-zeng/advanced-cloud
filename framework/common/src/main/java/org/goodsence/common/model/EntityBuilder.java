package org.goodsence.common.model;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

/**
 * @author tain
 * @apiNote 实体建造器
 * @date 2024/9/29
 * @project modern-arranged-marriage
 */
public class EntityBuilder<K extends Serializable, T extends Entity<K>> {

    private final Class<T> typeClass;

    private K id;

    private Date createdAt;

    private Date updatedAt;

    private String createdBy;

    private String updatedBy;

    private Integer version;

    private Integer status;

    public EntityBuilder(Class<T> typeClass) {
        this.typeClass = typeClass;
    }

    public EntityBuilder<K, T> id(final K id) {
        this.id = id;
        return this;
    }

    public EntityBuilder<K, T> createdAt(final Date createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public EntityBuilder<K, T> updatedAt(final Date updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public EntityBuilder<K, T> createdBy(final String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public EntityBuilder<K, T> updatedBy(final String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public EntityBuilder<K, T> version(final Integer version) {
        this.version = version;
        return this;
    }

    public EntityBuilder<K, T> status(final Integer status) {
        this.status = status;
        return this;
    }

    public static <K extends Serializable, T extends Entity<K>> EntityBuilder<K, T> withClass(Class<T> tClass) {
        return new EntityBuilder<>(tClass);
    }

    public T build() {
        if (typeClass == null) {
            throw new IllegalArgumentException("Class cannot be null");
        }
        T entity;
        try {
            entity = typeClass.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
        entity.setId(id);
        entity.setCreatedAt(createdAt == null ? new Date(): createdAt);
        entity.setCreatedBy(createdBy == null ? "administrator" : createdBy);
        entity.setStatus(status == null ? Status.ACTIVE.value() : status);
        entity.setVersion(version == null ? 0 : version);
        entity.setUpdatedBy(updatedBy);
        entity.setUpdatedAt(updatedAt);
        return entity;
    }
}
