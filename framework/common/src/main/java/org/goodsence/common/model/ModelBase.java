package org.goodsence.common.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public abstract class ModelBase<K extends Serializable> implements Serializable {

    private K id;

    /**
     * 创建者 可以用于数据鉴权
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新者
     */
    private String updatedBy;

    /**
     * 创建人
     */
    private Date updatedAt;

    /**
     * 是否删除
     */
    private Integer status;
}
