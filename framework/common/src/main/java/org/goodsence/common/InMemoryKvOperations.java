package org.goodsence.common;

import lombok.Data;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author tain
 * @apiNote
 * @date 2024/10/12
 * @project modern-arranged-marriage
 */
public class InMemoryKvOperations implements KvOperations {

    private final Map<String, DataInfo> cache;

    private static final int MAX_CACHE_SIZE = 100;

    private final int cacheSize;

    public InMemoryKvOperations() {
        this(MAX_CACHE_SIZE);
    }

    private InMemoryKvOperations(int cacheSize) {
        if (cacheSize > MAX_CACHE_SIZE) {
            throw new IllegalArgumentException("Cache size cannot be greater than " + MAX_CACHE_SIZE);
        }
        this.cacheSize = cacheSize;
        cache = new ConcurrentHashMap<>();
    }

    @Override
    public Object get(String key) {
        if (!contains(key)) {
            return null;
        }
        DataInfo dataInfo = cache.get(key);
        if (dataInfo.isExpired()) {
            cache.remove(key);
            return null;
        }
        return dataInfo.getValue();
    }

    @Override
    public void put(String key, Object value) {
        put(key, new DataInfo(value));
    }

    @Override
    public void put(String key, Object value, long ttl) {
        put(key, new DataInfo(value, new Date(System.currentTimeMillis() + ttl)));
    }

    private void put(String key, DataInfo dataInfo) {
        if (cache.size() >= cacheSize){
            evictExpired();
        }
        if (cache.size() >= cacheSize) {
            throw new UnsupportedOperationException("缓存已满!");
        }
        cache.put(key, dataInfo);
    }

    @Override
    public void remove(String key) {
        cache.remove(key);
    }

    @Override
    public boolean contains(String key) {
        return cache.get(key) != null;
    }

    @Override
    public void clear() {
        cache.clear();
    }

    private void evictExpired() {
        cache.entrySet().removeIf(entry -> entry.getValue().isExpired());
    }

    @Data
    private static class DataInfo{

        private Object value;

        private Date expireDate;

        protected DataInfo(Object value) {
            this(value, null);
        }

        protected DataInfo(Object value, Date expireDate) {
            this.value = value;
            this.expireDate = expireDate;
        }

        public boolean isExpired() {
            if (expireDate == null) {
                return false;
            }
            return System.currentTimeMillis() > expireDate.getTime();
        }
    }
}
