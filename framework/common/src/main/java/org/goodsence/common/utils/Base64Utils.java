package org.goodsence.common.utils;

import org.goodsence.common.CanNotInstantiateReason;

import java.util.Base64;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-03-18 07:00:15
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 * @deprecated use {@link Base64#getEncoder()} instead
 */
@Deprecated
public final class Base64Utils {

    private Base64Utils(){
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static String encode(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes());
    }
}
