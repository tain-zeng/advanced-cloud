package org.goodsence.common;

import java.util.Arrays;
import java.util.Objects;

/**
 * <p>
 * created at <b> 2024-04-15 00:20:44 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
public interface CustomEnum {

    Object value();

    default String descriptions() {
        return Objects.toString(value());
    }

    static <E extends CustomEnum> E valueOf(E[] customEnums, Object value) {
        if (Objects.isNull(customEnums) || Objects.isNull(value)){
            return null;
        }
        return Arrays.stream(customEnums)
                .filter(Objects::nonNull)
                .filter(e -> Objects.equals(e.value(), value))
                .findAny()
                .orElse(null);
    }
}
