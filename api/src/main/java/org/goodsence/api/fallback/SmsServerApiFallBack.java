package org.goodsence.api.fallback;

import org.goodsence.api.SmsServerApi;
import org.goodsence.web.Results;
import org.springframework.http.ResponseEntity;

/**
 * @author tain
 * @apiNote
 * @date 2024/10/25
 * @project advanced-cloud
 */
public class SmsServerApiFallBack implements SmsServerApi {
    @Override
    public ResponseEntity<String> sendSms(String phoneNumber, String message) {
        return Results.error();
    }
}
