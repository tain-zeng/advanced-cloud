package org.goodsence.api.fallback;

import org.goodsence.api.MessageServerApi;
import org.goodsence.api.param.MessageArgs;
import org.goodsence.web.Results;
import org.springframework.http.ResponseEntity;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public class MessageServerApiFallback implements MessageServerApi {

    @Override
    public ResponseEntity<Void> sendMessage(String c, String a, MessageArgs message) {
        return Results.error();
    }
}
