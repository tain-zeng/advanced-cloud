package org.goodsence.api.fallback;

import org.goodsence.api.param.ChatCompletionsArgs;
import org.goodsence.api.OpenAIApi;
import org.goodsence.web.Results;
import org.springframework.http.ResponseEntity;

/**
 * <p>
 * created at <b> 2024-03-30 14:42:32 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
public class OpenAIApiFallback implements OpenAIApi {
    @Override
    public ResponseEntity<String> chatCompletions(String authorization, ChatCompletionsArgs chatCompletionsArgs) {
        return Results.error();
    }
}
