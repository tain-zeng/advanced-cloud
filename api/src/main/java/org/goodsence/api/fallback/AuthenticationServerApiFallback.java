package org.goodsence.api.fallback;

import org.goodsence.api.AuthenticationServerApi;
import org.goodsence.api.param.CheckTokenArgs;
import org.goodsence.api.param.RegisterByMobileParam;
import org.goodsence.web.Results;
import org.springframework.http.ResponseEntity;

import java.util.Map;

/**
 * @author zty
 * @apiNote
 * @user nav1c
 * @date 2023/8/25 16:49
 * @project advanced-cloud
 */
public class AuthenticationServerApiFallback implements AuthenticationServerApi {

    @Override
    public ResponseEntity<Map<String, Object>> introspect(CheckTokenArgs args) {
        return Results.error();
    }

    @Override
    public ResponseEntity<Void> phoneRegister(RegisterByMobileParam registerByMobileParam) {
        return Results.error();
    }
}
