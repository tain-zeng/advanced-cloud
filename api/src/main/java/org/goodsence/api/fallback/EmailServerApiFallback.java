package org.goodsence.api.fallback;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.api.EmailServerApi;
import org.goodsence.api.param.EmailArgs;
import org.goodsence.web.Results;
import org.springframework.http.ResponseEntity;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@Slf4j
public class EmailServerApiFallback implements EmailServerApi {
    @Override
    public ResponseEntity<Void> sendEmail(EmailArgs emailArgs) {
        log.info("没钱买email服务，请到控制台取: {}", emailArgs);
        return Results.error();
    }
}
