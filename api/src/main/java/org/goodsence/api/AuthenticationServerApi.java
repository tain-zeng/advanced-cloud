package org.goodsence.api;

import org.goodsence.api.fallback.AuthenticationServerApiFallback;
import org.goodsence.api.param.CheckTokenArgs;
import org.goodsence.api.param.RegisterByMobileParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

/**
 * @User nav1c
 * @Author zty
 * @Date 2023/2/23 21:09
 * @ProjectName modern-arranged-marriage
 * @Version 1.0.0
 */
/*
因为MODERN_ARRANGED_MARRIAGE有context-path 必须加上path 不然404.
fallback是ModernArrangedMarriageApi 的实现类。以@Component修饰。
*/
@FeignClient(name = "authentication-server", fallback = AuthenticationServerApiFallback.class)
public interface AuthenticationServerApi {

    @PostMapping("/oauth2/introspect")
    ResponseEntity<Map<String, Object>> introspect(@SpringQueryMap @Validated CheckTokenArgs args);

    @PostMapping("/register/phone")
    ResponseEntity<Void> phoneRegister(@RequestBody RegisterByMobileParam registerByMobileParam);
}
