package org.goodsence.api;

import jakarta.servlet.http.HttpServletRequest;
import org.goodsence.api.fallback.MessageServerApiFallback;
import org.goodsence.api.param.MessageArgs;
import org.goodsence.web.HttpServletUtils;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@FeignClient(name = "Message-server", fallback = MessageServerApiFallback.class)
public interface MessageServerApi {

    default ResponseEntity<Void> sendMessage(MessageArgs messageArgs) {
        HttpServletRequest request = HttpServletUtils.getHttpServletRequest();
        String clientCredentials;
        String authorization;
        if(request != null){
            clientCredentials = request.getHeader("Fake-Public-Client-Authorization");
            authorization = request.getHeader("Authorization");
        }else{
            clientCredentials = null;
            authorization = null;
        }
        return sendMessage(clientCredentials, authorization, messageArgs);
    }

    @PostMapping("/messages")
    ResponseEntity<Void> sendMessage(@RequestHeader("Fake-Public-Client-Authorization") String clientCredentials,
                                     @RequestHeader("Authorization") String authorization, @RequestBody MessageArgs message);

}
