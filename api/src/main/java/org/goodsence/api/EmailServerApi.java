package org.goodsence.api;

import org.goodsence.api.fallback.EmailServerApiFallback;
import org.goodsence.api.param.EmailArgs;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author tain
 * @apiNote
 * @date 2024/9/27
 * @project modern-arranged-marriage
 */
@FeignClient(name = "email-server", url = "https://unknown", fallback = EmailServerApiFallback.class)
public interface EmailServerApi {

    @PostMapping("/sendEmail")
    ResponseEntity<Void> sendEmail(EmailArgs emailArgs);

}
