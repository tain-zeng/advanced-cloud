package org.goodsence.api;

import org.goodsence.api.fallback.SmsServerApiFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author tain
 * @apiNote
 * @date 2024/9/27
 * @project modern-arranged-marriage
 */
@FeignClient(name = "sms-server", url = "https://unknown", fallback = SmsServerApiFallBack.class)
public interface SmsServerApi {

    @PostMapping("/sendSms")
    ResponseEntity<String> sendSms(String phoneNumber, String message);
}
