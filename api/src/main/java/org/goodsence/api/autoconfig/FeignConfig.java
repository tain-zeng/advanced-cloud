package org.goodsence.api.autoconfig;

import org.goodsence.api.fallback.*;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/26
 * @project advanced-cloud
 */
@EnableFeignClients(basePackages = "org.goodsence.api")
@Import({AuthenticationServerApiFallback.class, EmailServerApiFallback.class, MessageServerApiFallback.class,
        OpenAIApiFallback.class, SmsServerApiFallBack.class})
public class FeignConfig {
}
