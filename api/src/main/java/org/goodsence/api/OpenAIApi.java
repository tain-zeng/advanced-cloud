package org.goodsence.api;

import org.goodsence.api.fallback.OpenAIApiFallback;
import org.goodsence.api.param.ChatCompletionsArgs;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Collections;
import java.util.Set;

/**
 * <p>
 * created at <b> 2024-03-30 03:18:58 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
@FeignClient(name = "OpenAIApi", url = "${outer-api.openai.uri}", fallback = OpenAIApiFallback.class)
public interface OpenAIApi {

    String DEFAULT_TOKEN = ""; //todo
//            CustomApplicationContextAware.getEnvironment().getProperty("openai.default-token");

    String DEFAULT_MODEL = ""; //todo
//    CustomApplicationContextAware.getEnvironment().getProperty("openai.default-model");

    default ResponseEntity<String> chatCompletions(String content) {
        return chatCompletions(DEFAULT_TOKEN, content);
    }

    default ResponseEntity<String> chatCompletions(String authorization, String content) {
        return chatCompletions(authorization, DEFAULT_MODEL, content);
    }

    default ResponseEntity<String> chatCompletions(String authorization, String model, String content) {
        ChatCompletionsArgs.Message message = new ChatCompletionsArgs.Message();
        message.setRole("user");
        message.setContent(content);
        Set<ChatCompletionsArgs.Message> messages = Collections.singleton(message);
        ChatCompletionsArgs chatCompletionsArgs = new ChatCompletionsArgs();
        chatCompletionsArgs.setMessages(messages);
        chatCompletionsArgs.setTemperature(0.7);
        chatCompletionsArgs.setModel(model);
        return chatCompletions(authorization, chatCompletionsArgs);
    }

    @PostMapping("/chat/completions")
    ResponseEntity<String> chatCompletions(@RequestHeader("Authorization") String authorization,
                                           @RequestBody ChatCompletionsArgs chatCompletionsArgs);

}
