package org.goodsence.api.param;

import lombok.Data;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2023-12-16 22:00:49
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Data
public class RegisterByMobileParam {
    private String mobile;

    private String contractWay;
}
