package org.goodsence.api.param;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2023-12-28 11:13:27
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Data
public class CheckTokenArgs {

    @NotBlank(message = "token不能为空")
    private String token;

    @NotBlank(message = "client_id不能为空")
    private String client_id;//NOSONAR

    @NotBlank(message = "client_id不能为空")
    private String client_secret;//NOSONAR
}
