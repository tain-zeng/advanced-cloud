package org.goodsence.api.param;

import lombok.Data;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@Data
public class EmailArgs {
    private String email;

    private String subject;

    private String content;
}
