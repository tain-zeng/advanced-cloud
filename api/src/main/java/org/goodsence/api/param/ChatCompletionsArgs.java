package org.goodsence.api.param;

import lombok.Data;

import java.util.Set;

/**
 * <p>
 * created at <b> 2024-03-30 03:25:54 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
@Data
public class ChatCompletionsArgs {
    private String model;

    private Set<Message> messages;

    private Double temperature;

    @Data
    public static class Message {
        private String role;
        private String content;
    }
}
