package org.goodsence.oss.service.impl;

import org.goodsence.common.BusinessException;
import org.goodsence.oss.core.BucketGenerator;
import org.goodsence.oss.core.FileOperations;
import org.goodsence.oss.dao.FileDao;
import org.goodsence.oss.dao.domain.OssFile;
import org.goodsence.oss.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@Service
public class FileServiceImpl implements FileService {

    private final FileDao fileDao;

    private final FileOperations fileOperations;

    private final BucketGenerator bucketGenerator;

    @Value("${jsch.base-dir:/home/tain/}")
    private String baseDir;

    @Autowired
    public FileServiceImpl(FileDao fileDao, FileOperations fileOperations, BucketGenerator bucketGenerator) {
        this.fileDao = fileDao;
        this.fileOperations = fileOperations;
        this.bucketGenerator = bucketGenerator;
    }

    @Override
    public Resource download(String id) {
        OssFile ossFile = fileDao.selectByPrimaryKey(id);
        if(ossFile == null) {
            throw new BusinessException("File not found");
        }
        return fileOperations.getResource(ossFile.getBucket() + ossFile.getSubPath());
    }

    @Override
    public OssFile fileInfo(String id) {
        return fileDao.selectByPrimaryKey(id);
    }

    @Override
    public void delete(String id) {
        OssFile ossFile = fileDao.selectByPrimaryKey(id);
        if(ossFile == null) {
            return;
        }
        fileDao.delete(ossFile.getId());
        fileOperations.remove(ossFile.getBucket() + ossFile.getSubPath());
    }

    @Override
    public String upload(MultipartFile file) {
        String bucket = baseDir + bucketGenerator.generate();
        OssFile ossFile = new OssFile();
        ossFile.setBucket(bucket);
        String name = file.getOriginalFilename();
        String ext = name.substring(name.lastIndexOf(".") + 1);
        ossFile.setSubPath(name);
        ossFile.setName(name);
        String id = UUID.randomUUID().toString();
        ossFile.setId(id);
        ossFile.setSize(Math.toIntExact(file.getSize()));
        ossFile.setExtensive(ext);
        fileOperations.put(bucket + name, file.getResource());
        fileDao.insertSelective(ossFile);
        return id;
    }

    @Override
    public String uploadOverride(MultipartFile file) {
        throw new BusinessException("Not implemented yet");
    }
}
