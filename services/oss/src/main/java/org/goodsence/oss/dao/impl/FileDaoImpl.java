package org.goodsence.oss.dao.impl;

import org.goodsence.oss.dao.FileDao;
import org.goodsence.oss.dao.domain.OssFile;
import org.goodsence.oss.dao.mapper.OssFileMapper;
import org.springframework.stereotype.Repository;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@Repository
public class FileDaoImpl implements FileDao {

    private final OssFileMapper ossFileMapper;

    public FileDaoImpl(OssFileMapper ossFileMapper) {
        this.ossFileMapper = ossFileMapper;
    }

    @Override
    public void insertSelective(OssFile entity) {
        ossFileMapper.insertSelective(entity);
    }

    @Override
    public void updateByPrimaryKeySelective(OssFile entity) {
        ossFileMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public OssFile selectByPrimaryKey(String key) {
        return ossFileMapper.selectByPrimaryKey(key);
    }

    @Override
    public void delete(String id) {
        ossFileMapper.deleteByPrimaryKey(id);
    }
}
