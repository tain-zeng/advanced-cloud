package org.goodsence.oss.core;

import org.springframework.core.io.Resource;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public interface FileOperations {

    Resource getResource(String src);

    void put(String src, Resource resource);

    void remove(String src);

    boolean exists(String src);

    void mkdirIfNotExists(String src);
}
