package org.goodsence.oss.controller;

import org.goodsence.common.BusinessException;
import org.goodsence.oss.dao.domain.OssFile;
import org.goodsence.oss.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@RestController
@RequestMapping("/files")
public class FileController {

    private final FileService fileService;

    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping
    public String upload(MultipartFile file) {
        return fileService.upload(file);
    }

    @GetMapping("/{id}/download")
    public ResponseEntity<Resource> download(@PathVariable String id) {
        OssFile ossFile = fileService.fileInfo(id);
        Resource file = fileService.download(id);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDisposition(ContentDisposition.attachment().filename(ossFile.getName()).build());
        return ResponseEntity.ok().headers(headers).body(file);
    }

    @GetMapping("/{id}/preview")
    public ResponseEntity<Resource> preview(@PathVariable String id) {
        OssFile ossFile = fileService.fileInfo(id);
        if (ossFile == null) {
            throw new BusinessException("File not found");
        }
        String extensive = ossFile.getExtensive();
        if (extensive == null) {
            throw new BusinessException("Extensive file not found");
        }
        MediaType mediaType = switch (extensive.toLowerCase()) {
            case "jpeg", "jpg" -> MediaType.IMAGE_JPEG;
            case "png" -> MediaType.IMAGE_PNG;
            case "gif" -> MediaType.IMAGE_GIF;
            case "md", "txt" -> MediaType.TEXT_PLAIN;
            case "html" -> MediaType.TEXT_HTML;
            case "pdf" -> MediaType.APPLICATION_PDF;
            default -> throw new BusinessException("Extensive file not supported");
        };
        Resource file = fileService.download(id);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);
        headers.setContentDisposition(ContentDisposition.inline().build());
        return ResponseEntity.ok().headers(headers).body(file);
    }

    @GetMapping("/{id}")
    public OssFile get(@PathVariable String id) {
        return fileService.fileInfo(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        fileService.delete(id);
    }
}
