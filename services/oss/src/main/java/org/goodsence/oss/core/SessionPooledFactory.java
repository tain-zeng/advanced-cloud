package org.goodsence.oss.core;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import java.util.Properties;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public class SessionPooledFactory extends BasePooledObjectFactory<Session> {

    private final String host;

    private final Integer port;

    private final String username;

    private final String password;

    private final Integer timeout;

    private final JSch jsch;

    public SessionPooledFactory(String host, Integer port, String username, String password, Integer timeout) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.timeout = timeout;
        jsch = new JSch();
    }

    @Override
    public Session create() throws JSchException {
        Session session = jsch.getSession(username, host, port);
        session.setPassword(password);
        session.setTimeout(timeout);
        final Properties properties = new Properties();
        properties.put("StrictHostKeyChecking", "no");
        session.setConfig(properties);
        return session;
    }

    @Override
    public PooledObject<Session> wrap(Session session) {
        return new DefaultPooledObject<>(session);
    }

    @Override
    public void activateObject(PooledObject<Session> p) throws JSchException {
        Session session = p.getObject();
        if (!session.isConnected()){
            session.connect();
        }
    }

    @Override
    public boolean validateObject(PooledObject<Session> p) {
        return p.getObject().isConnected();
    }

    @Override
    public void passivateObject(PooledObject<Session> p) {
        //不要关闭连接
    }

    @Override
    public void destroyObject(PooledObject<Session> p) {
        Session session = p.getObject();
        if (session.isConnected()){
            session.disconnect();
        }
    }

}
