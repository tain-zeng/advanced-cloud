package org.goodsence.oss.dao.domain;

import java.io.Serial;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.goodsence.common.model.Entity;

/**
 * 
 * @TableName oss_file
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class OssFile extends Entity<String> {

    /**
     * 文件夹
     */
    private String bucket;

    /**
     * 一级文件夹以后的路径
     */
    private String subPath;

    /**
     * 文件大小
     */
    private Integer size;

    /**
     * 文件后缀
     */
    private String extensive;

    @Serial
    private static final long serialVersionUID = 1L;
}