package org.goodsence.oss.core;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public interface BucketGenerator {

    String generate();
}
