package org.goodsence.oss.config;

import com.jcraft.jsch.Session;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.goodsence.oss.core.SftpFileOperations;
import org.goodsence.oss.core.DateBucketGenerator;
import org.goodsence.oss.core.BucketGenerator;
import org.goodsence.oss.core.FileOperations;
import org.goodsence.oss.core.SessionPooledFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author zty
 * @Date 2023/4/4
 * @ProjectName modern-arranged-marriage
 * @Version 1.0.0
 **/
@Configuration
public class JSCHConfig implements InitializingBean {

    @Value("${jsch.host}")
    private String host;

    private Integer port;

    @Value("${jsch.protocol}")
    private String protocol;

    @Value("${jsch.username}")
    private String username;

    @Value("${jsch.password}")
    private String password;

    @Value("${jsch.timeout}")
    private Integer timeout;

    @Value("${jsch.pool.max-total:4}")
    private Integer maxTotal;

    @Value("${jsch.pool.max-idle:2}")
    private Integer maxIdle;

    @Value("${jsch.pool.min-idle:1}")
    private Integer minIdle;

    @Override
    public void afterPropertiesSet() {
        if (protocol.equalsIgnoreCase("sftp")){
            this.port = 22;
        }else if (protocol.equalsIgnoreCase("ftp")){
            this.port = 21;
        }else{
            throw new IllegalArgumentException("Unsupported protocol: " + protocol);
        }
    }

    @Bean
    public ObjectPool<Session> sessionPool() {
        GenericObjectPoolConfig<Session> config = new GenericObjectPoolConfig<>();
        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);
        config.setJmxEnabled(false);
        return new GenericObjectPool<>(new SessionPooledFactory(host, port, username, password, timeout), config);
    }

    @Bean
    public BucketGenerator bucketGenerator(){
        return new DateBucketGenerator();
    }

    @Bean
    public FileOperations fileOperations(ObjectPool<Session> sessionPool){
        return new SftpFileOperations(protocol, timeout, sessionPool);
    }
}
