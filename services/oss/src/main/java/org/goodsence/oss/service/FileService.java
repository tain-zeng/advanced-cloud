package org.goodsence.oss.service;

import org.goodsence.oss.dao.domain.OssFile;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public interface FileService {

    Resource download(String id);

    OssFile fileInfo(String id);

    void delete(String id);

    String upload(MultipartFile file);

    String uploadOverride(MultipartFile file);
}
