package org.goodsence.oss.core;

import org.goodsence.common.utils.DateUtils;
import org.springframework.stereotype.Component;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@Component
public class DateBucketGenerator implements BucketGenerator {

    @Override
    public String generate() {
        return DateUtils.now("YYYY/MM/dd/");
    }
}
