package org.goodsence.oss.dao;

import org.goodsence.common.model.BaseDao;
import org.goodsence.oss.dao.domain.OssFile;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public interface FileDao extends BaseDao<String, OssFile> {
}
