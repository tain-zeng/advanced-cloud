package org.goodsence.oss;

import org.goodsence.common.EnvironmentPrinter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("org.goodsence.oss.dao.mapper")
@ComponentScan(basePackages = {
        "org.goodsence.oss.config",
        "org.goodsence.oss.dao.mapper",
        "org.goodsence.oss.dao.impl",
        "org.goodsence.oss.service.impl",
        "org.goodsence.oss.controller"
})
public class OssApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(OssApplication.class, args);
        EnvironmentPrinter.printInfo(context);
    }
}
