package org.goodsence.oss.core;

import com.jcraft.jsch.*;
import org.apache.commons.pool2.ObjectPool;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public class SftpFileOperations implements FileOperations {

    private final ObjectPool<Session> sessionPool;

    private final ThreadLocal<ChannelSftp> channelSftpThreadLocal;

    private final ThreadLocal<Session> sessionThreadLocal;

    private final Integer connectTimeout;

    private final String protocol;

    public SftpFileOperations(String protocol, Integer connectTimeout, ObjectPool<Session> sessionPool) {
        this.connectTimeout = connectTimeout;
        this.protocol = protocol;
        channelSftpThreadLocal = ThreadLocal.withInitial(() -> null);
        sessionThreadLocal = ThreadLocal.withInitial(() -> null);
        this.sessionPool = sessionPool;
    }

    @Override
    public Resource getResource(String src) {
        ChannelSftp channelSftp = openChannel();
        try {
            InputStream inputStream = channelSftp.get(src);
            return new ByteArrayResource(inputStream.readAllBytes());
        } catch (SftpException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            closeChannel();
        }
    }

    @Override
    public void put(String src, Resource resource) {
        String dir = src.substring(0, src.lastIndexOf('/'));
        ChannelSftp channelSftp = openChannel();
        try {
            mkdir(channelSftp, dir);
            InputStream inputStream = resource.getInputStream();
            channelSftp.put(inputStream, src);
        } catch (IOException | SftpException e) {
            throw new RuntimeException(e);
        } finally {
            closeChannel();
        }
    }

    @Override
    public void remove(String src) {
        ChannelSftp channelSftp = openChannel();
        try {
            channelSftp.rm(src);
        } catch (SftpException e) {
            throw new RuntimeException(e);
        }finally {
            closeChannel();
        }
    }

    private ChannelSftp openChannel() {
        ChannelSftp channelSftp = channelSftpThreadLocal.get();
        if (channelSftp != null) {
            if (channelSftp.isConnected()) {
                return channelSftp;
            }
        }
        Session session;
        try {
            session = sessionPool.borrowObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        sessionThreadLocal.set(session);
        try {
            channelSftp = (ChannelSftp) session.openChannel(protocol);
        } catch (JSchException e) {
            sessionThreadLocal.remove();
            try {
                sessionPool.returnObject(session);
                sessionPool.invalidateObject(session);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        }
        try {
            channelSftp.connect(connectTimeout);
        } catch (JSchException e) {
            sessionThreadLocal.remove();
            try {
                sessionPool.returnObject(session);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        }
        channelSftpThreadLocal.set(channelSftp);
        return channelSftp;
    }

    private void closeChannel() {
        ChannelSftp channelSftp = channelSftpThreadLocal.get();
        if (channelSftp != null) {
            channelSftp.disconnect();
            channelSftpThreadLocal.remove();
        }
        Session session = sessionThreadLocal.get();
        if (session == null) {
            return;
        }
        try {
            sessionPool.returnObject(session);
        } catch (Exception e) {
            try {
                sessionPool.invalidateObject(session);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            throw new RuntimeException(e);
        }finally {
            sessionThreadLocal.remove();
        }
    }

    @Override
    public boolean exists(String src) {
        return false;
    }

    @Override
    public void mkdirIfNotExists(String src) {
        ChannelSftp channelSftp = openChannel();
        try {
            mkdir(channelSftp, src);
        }catch (SftpException e){
            throw new RuntimeException(e);
        }finally {
            closeChannel();
        }
    }

    private void mkdir(ChannelSftp channelSftp, String src) throws SftpException {
        try {
            channelSftp.cd(src);
        } catch (SftpException e) {
            // If we can't change to the directory, it might not exist. Let's try to create it.
            if (e.id != ChannelSftp.SSH_FX_NO_SUCH_FILE && e.id != ChannelSftp.SSH_FX_PERMISSION_DENIED){
                throw e;
            }
            // We need to create the directory step by step, since some SFTP servers don't support creating nested directories at once.
            String[] dirs = src.split("/");
            StringBuilder currentPath = new StringBuilder("/");
            for (String dir : dirs) {
                if (dir.isEmpty()){
                    continue;
                }
                currentPath.append(dir).append("/");
                try {
                    channelSftp.cd(currentPath.substring(0, currentPath.length() - 1));
                } catch (SftpException ex) {
                    // If we can't change to this directory, it doesn't exist, so we create it.
                    try {
                        channelSftp.mkdir(currentPath.substring(0, currentPath.length() - 1));
                    } catch (SftpException exc) {
                        throw new RuntimeException(exc);
                    }
                }
            }
        }
    }
}
