package org.goodsence.oss.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.goodsence.oss.dao.domain.OssFile;

/**
* @author mengtain
* @description 针对表【oss_file】的数据库操作Mapper
* @createDate 2024-12-25 21:08:40
* @Entity generator.domain.OssFile
*/
@Mapper
public interface OssFileMapper {

    int deleteByPrimaryKey(String id);

    int insert(OssFile record);

    int insertSelective(OssFile record);

    OssFile selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OssFile record);

    int updateByPrimaryKey(OssFile record);

}
