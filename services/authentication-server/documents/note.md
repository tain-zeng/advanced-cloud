1. Spring中角色是特殊的权限，带有前缀标识：ROLE_xxx权限。（校验逻辑的代码是在哪里?）\
2. /oauth/token 是获取token的接口：\
请求参数是:\
POST:/oauth/token?client_id=authority&client_secret=123&grant_type=authorization_code&redirect_uri=http://www.baidu.com&code=eAM-Sr\
返回结果是:\
org.springframework.security.oauth2.common.OAuth2AccessToken\
3. /ouath/check_token是核验token并获取用户信息的接口:\
请求参数是:\POST:/oauth/check_token?token=3X2zm9UXr_ZcPzJ8jeIN9I-JAsU\
返回结果是:\
org.springframework.security.oauth2.provider.OAuth2Authentication\
4. /oauth/authorize是获取授权码接口:\
GET:/oauth/authorize?client_id=authority&redirect_uri=xx&response_type=code&state=xxx\
若未登录，认证服务器应当跳转到登录页，这是认证服务器要去做的。
5. 获取token后对资源服务器发起请求，需要把token放在请求头里:\
Authorization: Bearer _eEpMSYOzj_RHa7pgLQLYNl3U1s\

6. 拿到token后，访问资源服务器，如果把认证服务器关闭，那么就无法通过校验。 即使在请求一次后再关闭认证服务器，后面的请求也无法通过，第一次拿了token并没有存起来.
7.  密码模式获取token: 
  /oauth/token?grant_type=password&username=zty&password=123&client_id=authority&client_secret=123\
  客户端密码错误时报错: invalid_client Bad client credentials\
  用户账号密码错误时报错：invalid_grant  Bad credentials
  