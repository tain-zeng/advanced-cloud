/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.179.128_3306
 Source Server Type    : MySQL
 Source Server Version : 80040 (8.0.40-0ubuntu0.22.04.1)
 Source Host           : 192.168.179.128:3306
 Source Schema         : advanced_cloud

 Target Server Type    : MySQL
 Target Server Version : 80040 (8.0.40-0ubuntu0.22.04.1)
 File Encoding         : 65001

 Date: 12/12/2024 20:52:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rel_custom_user_custom_authority
-- ----------------------------
DROP TABLE IF EXISTS `rel_custom_user_custom_authority`;
CREATE TABLE `rel_custom_user_custom_authority`  (
  `custom_user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `custom_authority_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`custom_user_id`, `custom_authority_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rel_custom_user_custom_authority
-- ----------------------------
INSERT INTO `rel_custom_user_custom_authority` VALUES ('admin', 'DATA_all');
INSERT INTO `rel_custom_user_custom_authority` VALUES ('admin', 'user_id');

SET FOREIGN_KEY_CHECKS = 1;
