/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.179.128_3306
 Source Server Type    : MySQL
 Source Server Version : 80040 (8.0.40-0ubuntu0.22.04.1)
 Source Host           : 192.168.179.128:3306
 Source Schema         : advanced_cloud

 Target Server Type    : MySQL
 Target Server Version : 80040 (8.0.40-0ubuntu0.22.04.1)
 File Encoding         : 65001

 Date: 12/12/2024 20:51:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for custom_authority
-- ----------------------------
DROP TABLE IF EXISTS `custom_authority`;
CREATE TABLE `custom_authority`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of custom_authority
-- ----------------------------
INSERT INTO `custom_authority` VALUES ('DATA_all', '可访问所有数据');
INSERT INTO `custom_authority` VALUES ('DATA_org', '可访问所在组织数据');
INSERT INTO `custom_authority` VALUES ('DATA_self', '仅可访问自己创建的数据');
INSERT INTO `custom_authority` VALUES ('user_id', '查找用户信息');

SET FOREIGN_KEY_CHECKS = 1;
