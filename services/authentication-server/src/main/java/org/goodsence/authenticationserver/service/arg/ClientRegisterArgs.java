package org.goodsence.authenticationserver.service.arg;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.goodsence.common.RegexPattern;

import java.util.Set;

/**
 * @author tain
 * @apiNote
 * @date 2024/9/25
 * @project modern-arranged-marriage
 */
@Data
public class ClientRegisterArgs {

    @NotBlank
    @Pattern(regexp = RegexPattern.Constant.USERNAME)
    private String clientId;

    @NotBlank
    @Pattern(regexp = RegexPattern.Constant.COMPLEX_1)
    private String clientSecret;

    @NotBlank
    private String clientName;

    private Set<String> redirectUris;

    private Set<String> clientAuthenticationMethods;

    private Set<String> scopes;

    private Set<String> grantTypes;

    private String responseType;

    private Set<String> postLogoutRedirectUris;

    /**
     * 负责人信息
     */
    @NotBlank
    @Pattern(regexp = RegexPattern.Constant.EMAIL)
    private String email;

    @NotBlank
    private String verificationCode;

}
