package org.goodsence.authenticationserver.service.impl;

import org.goodsence.authenticationserver.dao.CustomUserDao;
import org.goodsence.authenticationserver.dao.RelUserAuthorityDao;
import org.goodsence.authenticationserver.dao.domain.RelCustomUserCustomAuthority;
import org.goodsence.security.oncepassword.EncryptedMobileNotFoundException;
import org.goodsence.security.oncepassword.MobileUserDetails;
import org.goodsence.security.oncepassword.MobileUserDetailsService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author tain
 * @apiNote
 * @date 2024/12/2
 * @project advanced-cloud
 */
@Deprecated
//@Service
public class MobileUserDetailsServiceImpl implements MobileUserDetailsService {

    private final CustomUserDao customUserDao;

    private final RelUserAuthorityDao relUserAuthorityDao;

//    @Autowired
    public MobileUserDetailsServiceImpl(CustomUserDao customUserDao, RelUserAuthorityDao relUserAuthorityDao) {
        this.customUserDao = customUserDao;
        this.relUserAuthorityDao = relUserAuthorityDao;
    }

    @Override
    public MobileUserDetails loadUserByEncryptedMobile(String encryptedMobile) throws EncryptedMobileNotFoundException {
//        final CustomUser customUser = customUserDao.selectByEncryptedMobile(encryptedMobile);
//        if (customUser == null) {
//            throw new EncryptedMobileNotFoundException(encryptedMobile);
//        }
//        return new SecurityUser(customUser, getGrantedAuthoritiesByUsername(customUser.getId()), null);
        return null;
    }

    private Set<GrantedAuthority> getGrantedAuthoritiesByUsername(String username) {
        List<RelCustomUserCustomAuthority> customAuthorities = relUserAuthorityDao.selectAuthoritiesByUsername(username);
        return customAuthorities.stream()
                .filter(Objects::nonNull)
                .map(relCustomUserCustomAuthority -> new SimpleGrantedAuthority(relCustomUserCustomAuthority.getCustomAuthorityId()))
                .collect(Collectors.toSet());
    }
}
