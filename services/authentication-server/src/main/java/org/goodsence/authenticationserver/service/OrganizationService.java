package org.goodsence.authenticationserver.service;

import org.goodsence.authenticationserver.dao.domain.Organization;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/11
 * @project advanced-cloud
 */
public interface OrganizationService {

    void create(Organization organization);

    void delete(Integer id);
}
