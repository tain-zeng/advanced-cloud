package org.goodsence.authenticationserver.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.goodsence.authenticationserver.dao.domain.CustomAuthority;

import java.util.List;

@Mapper
public interface CustomAuthorityMapper {
    int deleteByPrimaryKey(String authority);

    int insertSelective(CustomAuthority entity);

    CustomAuthority selectByPrimaryKey(String authority);

    int updateByPrimaryKeySelective(CustomAuthority entity);

    CustomAuthority selectByAuthorityName(@Param("authorityName") String authorityName);

    List<CustomAuthority> selectByUsername(String username);
}