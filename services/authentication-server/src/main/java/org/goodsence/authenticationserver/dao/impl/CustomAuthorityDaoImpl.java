package org.goodsence.authenticationserver.dao.impl;

import org.goodsence.authenticationserver.dao.CustomAuthorityDao;
import org.goodsence.authenticationserver.dao.domain.CustomAuthority;
import org.goodsence.authenticationserver.dao.mapper.CustomAuthorityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zty
 * @apiNote
 * @user nav1c
 * @date 2023/12/10 1:14
 * @project advanced-cloud
 */
@Repository
public class CustomAuthorityDaoImpl implements CustomAuthorityDao {

    private final CustomAuthorityMapper customAuthorityMapper;

    @Autowired
    public CustomAuthorityDaoImpl(CustomAuthorityMapper customAuthorityMapper) {
        this.customAuthorityMapper = customAuthorityMapper;
    }

    @Override
    public void insertSelective(CustomAuthority entity) {
        customAuthorityMapper.insertSelective(entity);
    }

    @Cacheable(value = "CustomAuthority", key = "#authority")
    @Override
    public CustomAuthority selectByPrimaryKey(String authority) {
        return customAuthorityMapper.selectByPrimaryKey(authority);
    }

    @Caching(evict = {
            @CacheEvict(value = "CustomAuthority", key = "#key")
    })
    @Override
    public void delete(String key) {
        customAuthorityMapper.deleteByPrimaryKey(key);
    }

    @Caching(evict = {
            @CacheEvict(value = "CustomAuthority", key = "#entity.id")
    })
    @Override
    public void updateByPrimaryKeySelective(CustomAuthority entity) {
        customAuthorityMapper.updateByPrimaryKeySelective(entity);
    }

    /**
     * @deprecated 用 {@link this#selectByName(String)}
     */
    @Deprecated
    @Override
    public CustomAuthority selectByAuthorityName(String authorityName) {
        return customAuthorityMapper.selectByAuthorityName(authorityName);
    }

    @Override
    public List<CustomAuthority> selectByUsername(String username) {
        return customAuthorityMapper.selectByUsername(username);
    }
}
