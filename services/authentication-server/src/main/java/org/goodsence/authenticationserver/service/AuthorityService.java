package org.goodsence.authenticationserver.service;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.goodsence.authenticationserver.service.arg.AuthoritiesAddDto;
import org.goodsence.authenticationserver.service.dto.AuthoritiesDto;
import org.goodsence.authenticationserver.service.arg.AuthoritiesUpdateDto;
import org.goodsence.common.model.ByKeyOperations;

/**
 * @User nav1c
 * @Author zty
 * @Date 2023/2/23 19:50
 * @ProjectName modern-arranged-marriage
 * @Version 1.0.0
 */
public interface AuthorityService extends ByKeyOperations<String> {
    void create(@NotNull AuthoritiesAddDto authoritiesAddDto);

    AuthoritiesDto get(@NotBlank String authorityId);

    void update(@NotNull AuthoritiesUpdateDto authoritiesUpdateDto);
}
