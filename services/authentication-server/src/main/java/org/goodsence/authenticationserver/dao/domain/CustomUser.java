package org.goodsence.authenticationserver.dao.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.goodsence.common.model.StringKeyEntity;

import java.io.Serial;

/**
 * authority_user
 * @author zty
 */
@EqualsAndHashCode(callSuper = true)
@Data //使用Data后会自动生成getter setter toString hashCode equals
public class CustomUser extends StringKeyEntity {

    /**
     * 密码
     * transient 不持久化到存储中
     */
    private transient String password;

    /**
     * 邮箱 用作 联系方式
     */
    private String email;

    /**
     * 手机号 加密手机号
     */
    @Deprecated
    private transient String encryptedMobile;

    @Serial
    private static final long serialVersionUID = 1L;
}