package org.goodsence.authenticationserver.service.arg;

import lombok.Data;

/**
 * @author zty
 * @apiNote 用户注册参数
 * @user nav1c
 * @date 2023/12/9 13:05
 * @project advanced-cloud
 */
@Data
public class RegisterByPasswordArgs {

    private String username;

    private String password;

    private String contractWay;
}
