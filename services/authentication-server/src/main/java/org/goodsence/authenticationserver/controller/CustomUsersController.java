package org.goodsence.authenticationserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.intercept.aopalliance.MethodSecurityInterceptor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tain
 * @apiNote
 * @date 2024/11/26
 * @project advanced-cloud
 */
@RestController
@RequestMapping("/custom-users")
public class CustomUsersController {

    private final UserDetailsManager userDetailsManager;

    @Autowired
    public CustomUsersController(UserDetailsManager userDetailsManager) {
        this.userDetailsManager = userDetailsManager;
    }

    /**
     * {@link MethodSecurityInterceptor}
     */
    @PreAuthorize("hasAuthority('user_id')")
    @GetMapping("/{username}")
    public UserDetails getUser(@PathVariable String username) {
        return userDetailsManager.loadUserByUsername(username);
    }
}
