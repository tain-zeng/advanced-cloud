package org.goodsence.authenticationserver.core;

import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenClaimsContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;

import java.util.ArrayList;
import java.util.Collection;

public class CompositeOAuth2TokenCustomizer implements OAuth2TokenCustomizer<OAuth2TokenClaimsContext> {

    private final Collection<OAuth2TokenCustomizer<OAuth2TokenClaimsContext>> customizers;

    public CompositeOAuth2TokenCustomizer() {
        this.customizers = new ArrayList<>();
    }

    public CompositeOAuth2TokenCustomizer(Collection<OAuth2TokenCustomizer<OAuth2TokenClaimsContext>> customizers) {
        this.customizers = customizers;
    }

    public void addCustomizer(OAuth2TokenCustomizer<OAuth2TokenClaimsContext> customizer) {
        this.customizers.add(customizer);
    }

    public void addCustomizers(Collection<OAuth2TokenCustomizer<OAuth2TokenClaimsContext>> customizers) {
        this.customizers.addAll(customizers);
    }

    @Override
    public void customize(OAuth2TokenClaimsContext context) {
        for (OAuth2TokenCustomizer<OAuth2TokenClaimsContext> customizer : customizers) {
            customizer.customize(context);
        }
    }
}
