package org.goodsence.authenticationserver.config;

import org.goodsence.authenticationserver.dao.CustomAuthorityDao;
import org.goodsence.authenticationserver.dao.CustomUserDao;
import org.goodsence.authenticationserver.dao.OrganizationDao;
import org.goodsence.authenticationserver.dao.RelUserAuthorityDao;
import org.goodsence.authenticationserver.core.CustomUserDetailsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 用户详情管理器配置
 * @date 2024-01-26 17:55:46
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Configuration
public class UserDetailsManagerConfig {

    @Bean
    @Autowired
    public CustomUserDetailsManager userDetailsManager(CustomUserDao customUserDao, CustomAuthorityDao customAuthorityDao,
                                                RelUserAuthorityDao relUserAuthorityDao,
                                                OrganizationDao organizationDao,
                                                PasswordEncoder passwordEncoder){
        return new CustomUserDetailsManager(customUserDao, relUserAuthorityDao, customAuthorityDao, organizationDao, passwordEncoder);
    }

}
