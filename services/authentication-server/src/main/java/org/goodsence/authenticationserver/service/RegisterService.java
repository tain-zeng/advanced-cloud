package org.goodsence.authenticationserver.service;

import org.goodsence.authenticationserver.service.arg.RegisterByMobileArgs;
import org.goodsence.authenticationserver.service.arg.RegisterByPasswordArgs;
import org.goodsence.authenticationserver.service.arg.RegisterArgs;

/**
 * <p>
 * created at <b> 2024-04-04 03:17:28 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 注册服务
 */
public interface RegisterService {

    /**
     * 密码注册
     * @param args
     */
    @Deprecated
    void registerByPassword(RegisterByPasswordArgs args);

    /**
     * 手机号注册
     * @param args
     */
    @Deprecated
    void registerByMobile(RegisterByMobileArgs args);

    /**
     * 邮箱注册，用户名默认为邮箱
     * @param args
     */
    void registerByEmail(RegisterArgs args);

    @Deprecated
    void register(RegisterArgs args);
}
