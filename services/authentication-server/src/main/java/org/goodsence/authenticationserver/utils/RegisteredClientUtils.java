package org.goodsence.authenticationserver.utils;

import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author tain
 * @apiNote
 * @date 2024/11/19
 * @project advanced-cloud
 */
public class RegisteredClientUtils {

    private RegisteredClientUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static RegisteredClient.Builder with(OAuth2ClientProperties.Registration registration, PasswordEncoder passwordEncoder) {
        return RegisteredClient.withId(registration.getClientId())
                .clientId(registration.getClientId())
                .clientName(registration.getClientName())
                .clientSecret(passwordEncoder.encode(registration.getClientSecret()))
                .redirectUris(strings -> strings.addAll(fetchRedirectUris(registration)))
                .authorizationGrantTypes(authorizationGrantTypes -> authorizationGrantTypes.addAll(fetchGrantTypes(registration)))
                .scopes(strings -> strings.addAll(registration.getScope()))
                .clientAuthenticationMethods(clientAuthenticationMethods -> clientAuthenticationMethods.addAll(fetchClientAuthenticationMethods(registration)));
    }

    private static Collection<ClientAuthenticationMethod> fetchClientAuthenticationMethods(OAuth2ClientProperties.Registration registration) {
        String authorizationGrantType = registration.getClientAuthenticationMethod();
        if (authorizationGrantType == null) {
            return Collections.emptySet();
        }
        String[] grantTypes = StringUtils.delimitedListToStringArray(authorizationGrantType, ",");
        return Arrays.stream(grantTypes)
                .filter(StringUtils::hasText)
                .map(ClientAuthenticationMethod::new)
                .collect(Collectors.toSet());
    }

    private static Collection<String> fetchRedirectUris(OAuth2ClientProperties.Registration registration) {
        String redirectUri = registration.getRedirectUri();
        if (redirectUri == null) {
            return Collections.emptyList();
        }
        String[] parts = StringUtils.delimitedListToStringArray(redirectUri, ",");
        return Arrays.asList(parts);
    }

    private static Set<AuthorizationGrantType> fetchGrantTypes(OAuth2ClientProperties.Registration registration) {
        String authorizationGrantType = registration.getAuthorizationGrantType();
        if (authorizationGrantType == null) {
            return Collections.emptySet();
        }
        String[] grantTypes = StringUtils.delimitedListToStringArray(authorizationGrantType, ",");
        return Arrays.stream(grantTypes)
                .filter(StringUtils::hasText)
                .map(AuthorizationGrantType::new)
                .collect(Collectors.toSet());
    }
}
