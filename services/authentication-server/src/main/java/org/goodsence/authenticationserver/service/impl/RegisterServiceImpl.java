package org.goodsence.authenticationserver.service.impl;

import org.goodsence.authenticationserver.core.SecurityUser;
import org.goodsence.authenticationserver.service.arg.RegisterByMobileArgs;
import org.goodsence.authenticationserver.service.arg.RegisterByPasswordArgs;
import org.goodsence.authenticationserver.core.SecurityUserBuilder;
import org.goodsence.authenticationserver.service.arg.RegisterArgs;
import org.goodsence.authenticationserver.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

/**
 * <p>
 * created at <b> 2024-04-02 17:09:59 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
@Service
public class RegisterServiceImpl implements RegisterService {

    private final UserDetailsManager userDetailsManager;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegisterServiceImpl(PasswordEncoder passwordEncoder, UserDetailsManager userDetailsManager) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Deprecated
    @Override
    public void registerByPassword(RegisterByPasswordArgs args) {
//        CustomUser customUser = StringKeyEntityBuilder.withClass(CustomUser.class).build();
//        BeanUtils.copyProperties(args, customUser);
//        customUser.setPassword(passwordEncoder.encode(args.getPassword()));
//        userDetailsManager.createUser(customUser);
    }

    @Deprecated
    @Override
    public void registerByMobile(RegisterByMobileArgs args) {
//        SecurityUser customUser = StringKeyEntityBuilder.withClass(CustomUser.class).build();
//        BeanUtils.copyProperties(args, customUser);
//        String mobile = args.getPhone();
//        customUser.setId(passwordEncoder.encode(mobile));
//        customUser.setPassword(null);
//        customUser.setEncryptedMobile(passwordEncoder.encode(mobile));
//        userDetailsManager.createUser(customUser);
    }

    @Override
    public void registerByEmail(RegisterArgs args) {
        SecurityUser customUser = SecurityUserBuilder.withUsername(args.getUsername())
                .email(args.getEmail())
                .password(passwordEncoder.encode(args.getPassword()))
                .accountNonExpired(false)
                .accountNonLocked(false)
                .credentialsNonExpired(false)
                .enabled(false)
                .build();
        userDetailsManager.createUser(customUser);
    }

    @Deprecated
    @Override
    public void register(RegisterArgs args) {
//        CustomUser customUser = SecurityUserBuilder.withUsername(args.getUsername())
//                .password(passwordEncoder.encode(args.getPassword()))
//                .accountNonExpired(false)
//                .accountNonLocked(false)
//                .credentialsNonExpired(false)
//                .enabled(false)
//                .build();
//        userDetailsManager.createUser(customUser);
    }
}
