package org.goodsence.authenticationserver.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.authenticationserver.service.arg.ClientRegisterArgs;
import org.goodsence.authenticationserver.service.RegisteredClientService;
import org.goodsence.common.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;

/**
 * @author tain
 * @apiNote
 * @date 2024/9/24
 * @project modern-arranged-marriage
 */
@Slf4j
@Service
public class RegisteredClientServiceImpl implements RegisteredClientService {

    private final RegisteredClientRepository registeredClientRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegisteredClientServiceImpl(RegisteredClientRepository registeredClientRepository, PasswordEncoder passwordEncoder) {
        this.registeredClientRepository = registeredClientRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void registerClient(ClientRegisterArgs client) {
        String clientId = client.getClientId();
        RegisteredClient registeredClient = registeredClientRepository.findByClientId(clientId);
        if (registeredClient != null) {
            log.info("Registered client with id {} already exists", clientId);
            throw new BusinessException("客户端已存在！");
        }

        registeredClient = RegisteredClient.withId(clientId)
                .clientId(clientId)
                .clientName(client.getClientName())
                .clientSecret(passwordEncoder.encode(client.getClientSecret()))
                .redirectUris(strings -> strings.addAll(client.getRedirectUris()))
                .authorizationGrantTypes(
                        authorizationGrantTypes ->
                                client.getGrantTypes().forEach(authorizationGrant ->
                                        authorizationGrantTypes.add(new AuthorizationGrantType(authorizationGrant)))
                )
                .clientAuthenticationMethods(
                        clientAuthenticationMethods ->
                                client.getClientAuthenticationMethods().forEach(s ->
                                        clientAuthenticationMethods.add(new ClientAuthenticationMethod(s)))
                )
                .clientSecretExpiresAt(Instant.now())
                .clientIdIssuedAt(Instant.now())
                .postLogoutRedirectUris(postLogoutRedirectUris -> postLogoutRedirectUris.addAll(client.getPostLogoutRedirectUris()))
                .scopes(scopes -> scopes.addAll(client.getScopes()))
                .tokenSettings(
                        TokenSettings.builder()
                                .accessTokenTimeToLive(Duration.ofMinutes(1))
                                .refreshTokenTimeToLive(Duration.ofHours(2))
                                .deviceCodeTimeToLive(Duration.ofHours(1))
                                .authorizationCodeTimeToLive(Duration.ofHours(1))
                                .accessTokenFormat(OAuth2TokenFormat.REFERENCE)//这里可能有问题
                                .build()
                )
                .clientSettings(
                        ClientSettings.builder()
                                //是否需要同意授权
                                //OAuth2AuthorizationConsentAuthenticationProvider 的逻辑有问题 必然报错
                                //不对，如果访问授权地址时没有带上scope=xxx时才会报错
                                .requireAuthorizationConsent(true)
                                .build()
                )
                .build();
        registeredClientRepository.save(registeredClient);
    }

}
