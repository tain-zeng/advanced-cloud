package org.goodsence.authenticationserver.config;

import org.goodsence.authenticationserver.core.RedisCache;
import org.goodsence.common.KvOperations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisOperations;

/**
 * @author zengty
 * @apiNote
 * @date 2025/1/3
 * @project advanced-cloud
 */
@Configuration
public class KvOperationsConfig {

    @Bean
    public KvOperations kvOperations(RedisOperations<String, Object> redisOperations) {
        return new RedisCache(redisOperations);
    }
}
