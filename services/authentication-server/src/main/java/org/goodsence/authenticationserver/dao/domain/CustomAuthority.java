package org.goodsence.authenticationserver.dao.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.goodsence.common.model.StringKeyEntity;

import java.io.Serial;

/**
 * authority_authorities
 * @author zty
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CustomAuthority extends StringKeyEntity {

    @Serial
    private static final long serialVersionUID = 1L;

}