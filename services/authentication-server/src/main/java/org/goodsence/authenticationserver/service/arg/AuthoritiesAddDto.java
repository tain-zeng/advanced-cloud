package org.goodsence.authenticationserver.service.arg;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * authority_authorities
 * @author zty
 */
@Data
public class AuthoritiesAddDto implements Serializable {

    /**
     * 权限标识
     */
    private String authority;

    /**
     * 权限名称
     */
    private String authorityName;

    @Serial
    private static final long serialVersionUID = 1L;
}