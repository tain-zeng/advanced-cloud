package org.goodsence.authenticationserver;

import org.goodsence.common.EnvironmentPrinter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.ConfigurableEnvironment;

@RefreshScope //全局刷新
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("org.goodsence.authenticationserver.dao.mapper")
@ComponentScan(basePackages = {
        "org.goodsence.authenticationserver.config",
        "org.goodsence.authenticationserver.dao.mapper",
        "org.goodsence.authenticationserver.dao.impl",
        "org.goodsence.authenticationserver.service.impl",
        "org.goodsence.authenticationserver.controller"
})
public class AuthenticationServerApplication {
    /**
     * 一个请求进来， 先按照顺序进入过滤器链{@link jakarta.servlet.Filter} 然后到达{@link jakarta.servlet.Servlet} 最后按照过滤器相反的顺序再走一遍
     * 请求入口 {@link jakarta.servlet.Servlet}
     * 请求的入口 {@link jakarta.servlet.http.HttpServlet}
     * 所有的请求都经过 {@link org.springframework.web.servlet.DispatcherServlet}
     * <p>
     *     启动创建的上下文是AnnotationConfigApplicationContext 或 GenericApplicationContext
     *{@link DefaultApplicationContextFactory#createDefaultApplicationContext()}
     * </p>
     *
     * {@link SpringApplication#prepareContext(DefaultBootstrapContext, ConfigurableApplicationContext, ConfigurableEnvironment, SpringApplicationRunListeners, ApplicationArguments, Banner)}
     * {@link SpringApplication#applyInitializers(ConfigurableApplicationContext)}
     * {@link org.springframework.boot.context.ContextIdApplicationContextInitializer#initialize(ConfigurableApplicationContext)}
     * {@link org.springframework.beans.factory.support.DefaultListableBeanFactory#registerSingleton(String, Object)}
     * {@link org.springframework.beans.factory.support.DefaultSingletonBeanRegistry#registerSingleton(String, Object)}
     * @param args String[]
     */
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AuthenticationServerApplication.class, args);
        EnvironmentPrinter.printInfo(context);
    }
}
