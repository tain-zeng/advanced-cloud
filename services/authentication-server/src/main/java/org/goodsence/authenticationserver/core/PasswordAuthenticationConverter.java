package org.goodsence.authenticationserver.core;

import jakarta.servlet.http.HttpServletRequest;
import org.goodsence.security.SecurityContextUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2ClientAuthenticationToken;
import org.springframework.security.web.authentication.AuthenticationConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-01-25 11:08:32
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public class PasswordAuthenticationConverter implements AuthenticationConverter {

    @Override
    public Authentication convert(HttpServletRequest request) {
        MultiValueMap<String, String> formParameters = ExtensiveOAuth2EndpointUtils.getFormParameters(request);
        final String grantType = formParameters.getFirst(OAuth2ParameterNames.GRANT_TYPE);
        if (!AuthorizationGrantType.PASSWORD.getValue().equals(grantType)) {
            return null;
        }
        String username = formParameters.getFirst(OAuth2ParameterNames.USERNAME);
        if (!StringUtils.hasText(username)){
            return null;
        }
        String password = formParameters.getFirst(OAuth2ParameterNames.PASSWORD);
        if (!StringUtils.hasText(password)){
            return null;
        }
        username = username.trim();
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(OAuth2ParameterNames.USERNAME, username);
        params.put(OAuth2ParameterNames.PASSWORD, password);
        //AbstractAuthenticationToken
        //为什么拿到的是Oauth2ClientAuthenticationToken 而不是 UserNamePasswordAuthenticationToken?
        Authentication clientPrincipal = SecurityContextUtils.getCurrentAuthentication();
        if (clientPrincipal instanceof OAuth2ClientAuthenticationToken oAuth2ClientAuthenticationToken){
            return new PasswordAuthenticationToken(oAuth2ClientAuthenticationToken, params);
        }
        throw new OAuth2AuthenticationException(OAuth2ErrorCodes.INVALID_REQUEST);
    }
}
