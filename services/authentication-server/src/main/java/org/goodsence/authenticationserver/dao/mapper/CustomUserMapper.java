package org.goodsence.authenticationserver.dao.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.goodsence.authenticationserver.dao.arg.ListUserInfoArgs;
import org.goodsence.authenticationserver.dao.domain.CustomUser;
import org.goodsence.authenticationserver.dao.dto.UserInfoPageDto;

import java.util.List;

@Mapper
public interface CustomUserMapper {

    int deleteByPrimaryKey(String username);

    int insertSelective(CustomUser entity);

    CustomUser selectByPrimaryKey(@Param("id") String username);

    int updateByPrimaryKeySelective(CustomUser entity);

    CustomUser selectByEncryptedMobile(@Param("encryptedMobile")String encryptedMobile);

    CustomUser selectByEmail(@Param("email")String email);

    List<UserInfoPageDto> listUserInfo(@Param("listUserInfoArgs")ListUserInfoArgs listUserInfoArgs);
}