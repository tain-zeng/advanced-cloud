package org.goodsence.authenticationserver.dao.dto;

import org.goodsence.authenticationserver.dao.domain.CustomUser;

/**
 * <p>
 * created at <b> 2024-04-04 03:23:00 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
public class UserInfoPageDto {

    private CustomUser customUser;
}
