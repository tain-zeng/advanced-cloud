package org.goodsence.authenticationserver.dao.impl;

import org.goodsence.authenticationserver.dao.OrganizationDao;
import org.goodsence.authenticationserver.dao.domain.Organization;
import org.goodsence.authenticationserver.dao.mapper.OrganizationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/11
 * @project advanced-cloud
 */
@Repository
public class OrganizationDaoImpl implements OrganizationDao {

    private final OrganizationMapper organizationMapper;

    @Autowired
    public OrganizationDaoImpl(OrganizationMapper organizationMapper) {
        this.organizationMapper = organizationMapper;
    }

    @Override
    public List<Organization> selectAllSubOrganizations(Integer id) {
        Organization organization = organizationMapper.selectByPrimaryKey(id);
        if (organization == null) {
            return Collections.emptyList();
        }
        String pattern = organization.getAncestors() + "," + organization.getId();
        return organizationMapper.selectOrganizationsWithAncestorsStartingWith(pattern);
    }

    @Override
    public List<Organization> selectDirectSubOrganizations(Integer id) {
        Organization organization = organizationMapper.selectByPrimaryKey(id);
        if (organization == null) {
            return Collections.emptyList();
        }
        String ancestors = organization.getAncestors() + "," + organization.getId();
        return organizationMapper.selectOrganizationsByAncestors(ancestors);
    }

    @Override
    public void insertSelective(Organization entity) {
        organizationMapper.insertSelective(entity);
    }

    @Caching(evict = {
            @CacheEvict(value = "Organization", key = "#entity.id"),
    })
    @Override
    public void updateByPrimaryKeySelective(Organization entity) {
        organizationMapper.updateByPrimaryKeySelective(entity);
    }

    @Cacheable(value = "Organization", key = "#key")
    @Override
    public Organization selectByPrimaryKey(Integer key) {
        return organizationMapper.selectByPrimaryKey(key);
    }

    @Override
    public Organization selectByName(String name) {
        return organizationMapper.selectByName(name);
    }

    @Caching(evict = {
            @CacheEvict(value = "Organization", key = "#id"),
    })
    @Override
    public void delete(Integer id) {
        organizationMapper.deleteByPrimaryKey(id);
    }
}
