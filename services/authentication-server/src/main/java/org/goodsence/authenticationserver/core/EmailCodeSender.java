package org.goodsence.authenticationserver.core;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.api.EmailServerApi;
import org.goodsence.api.param.EmailArgs;
import org.goodsence.security.AccountType;
import org.goodsence.security.oncepassword.DefaultOncePasswordSender;
import org.goodsence.security.oncepassword.OncePasswordStore;

/**
 * <p>
 * created at <b> 2024-04-05 17:28:40 </b>
 * with <b>IntelliJ IDEA</b>, belongs to <b>advanced-cloud</b>
 * <p>
 *
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 */
@Slf4j
@Deprecated
public class EmailCodeSender extends DefaultOncePasswordSender {

    private final EmailServerApi emailServerApi;

    public EmailCodeSender(EmailServerApi emailServerApi, OncePasswordStore oncePasswordStore) {
        super(oncePasswordStore, null);
        this.emailServerApi = emailServerApi;
    }

    @Override
    protected void send(AccountType accountType, String phone, String smsCode) {
        if (!"email".equals(accountType.getType())) {
            throw new UnsupportedOperationException("Only email password supported");
        }
        EmailArgs args = new EmailArgs();
        args.setEmail(phone);
        args.setSubject("短信验证码");
        args.setContent(smsCode);
        emailServerApi.sendEmail(args);
    }
}
