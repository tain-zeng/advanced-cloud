package org.goodsence.authenticationserver.dao;


import org.goodsence.authenticationserver.dao.domain.RelCustomUserCustomAuthority;
import org.goodsence.common.model.BaseDao;

import java.util.List;

public interface RelUserAuthorityDao extends BaseDao<RelCustomUserCustomAuthority, RelCustomUserCustomAuthority> {

    void deleteByUsername(String username);

    @Deprecated
    List<RelCustomUserCustomAuthority> selectAuthoritiesByUsername(String username);
}