package org.goodsence.authenticationserver.dao.impl;

import org.goodsence.authenticationserver.dao.domain.CustomUser;
import org.goodsence.authenticationserver.dao.CustomUserDao;
import org.goodsence.authenticationserver.dao.mapper.CustomUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

/**
 * @author zty
 * @apiNote
 * @user nav1c
 * @date 2023/12/10 1:16
 * @project advanced-cloud
 */
@Repository
public class CustomUserDaoImpl implements CustomUserDao {

    private final CustomUserMapper customUserMapper;

    @Autowired
    public CustomUserDaoImpl(CustomUserMapper customUserMapper) {
        this.customUserMapper = customUserMapper;
    }

    @Override
    public void insertSelective(CustomUser entity) {
        customUserMapper.insertSelective(entity);
    }

    @Cacheable(value = "CustomUser", key = "#username")
    @Override
    public CustomUser selectByPrimaryKey(String username) {
        return customUserMapper.selectByPrimaryKey(username);
    }

    @Caching(evict = {
            @CacheEvict(value = "CustomUser", key = "#key")
    })
    @Override
    public void delete(String key) {
        customUserMapper.deleteByPrimaryKey(key);
    }

    @Caching(evict = {
            @CacheEvict(value = "CustomUser", key = "#entity.id"),
    })
    @Override
    public void updateByPrimaryKeySelective(CustomUser entity) {
        customUserMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public CustomUser selectByEncryptedMobile(String encryptedPhone) {
        return customUserMapper.selectByEncryptedMobile(encryptedPhone);
    }

    @Override
    public CustomUser selectByEmail(String email) {
        return customUserMapper.selectByEmail(email);
    }

}
