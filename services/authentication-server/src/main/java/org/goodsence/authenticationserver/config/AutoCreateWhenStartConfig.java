package org.goodsence.authenticationserver.config;

import org.goodsence.authenticationserver.utils.RegisteredClientUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.security.provisioning.UserDetailsManager;

import java.time.Duration;
import java.util.Map;

/**
 * @author tain
 * @apiNote 让应用启动的时候自动创建一些Bean, 如管理员用户，Oauth2公开注册客户端。
 * @date 2024/9/25
 * @project modern-arranged-marriage
 * @deprecated 改用sql执行
 */
@Deprecated
public class AutoCreateWhenStartConfig implements InitializingBean {

    private final PasswordEncoder passwordEncoder;

    private final Map<String, OAuth2ClientProperties.Registration> registrationMap;

    private final UserDetailsManager userDetailsManager;

    private final RegisteredClientRepository registeredClientRepository;

    private final SecurityProperties.User user;

//    @Autowired
    public AutoCreateWhenStartConfig(PasswordEncoder passwordEncoder, OAuth2ClientProperties oAuth2ClientProperties, UserDetailsManager userDetailsManager, RegisteredClientRepository registeredClientRepository, SecurityProperties securityProperties) {
        this.passwordEncoder = passwordEncoder;
        this.registrationMap = oAuth2ClientProperties.getRegistration();
        this.userDetailsManager = userDetailsManager;
        this.registeredClientRepository = registeredClientRepository;
        this.user = securityProperties.getUser();
    }

    private UserDetails administrator() {
//        UserDetails userDetails = User.withUsername(user.getName())
//                .passwordEncoder(passwordEncoder::encode)
//                .password(user.getPassword())
//                .authorities(user.getRoles().toArray(new String[0]))
//                .accountNonExpired(false)
//                .accountNonLocked(false)
//                .credentialsNonExpired(false)
//                .enabled(false)
//                .build();
//        return CustomUserBuilder.withUserDetails(userDetails)
//                .encryptMobile(HashUtils.hash("10086"))
//                .email("nav1cat@outlook.com")
//                .build();
        return null;
    }

    private RegisteredClient fakePublicClient() {
        OAuth2ClientProperties.Registration registration = registrationMap.get("fake-public-client");
        return RegisteredClientUtils.with(registration, passwordEncoder)
                .tokenSettings(tokenSettings())
                .clientSettings(clientSettings())
                .build();
    }

    private ClientSettings clientSettings() {
        return ClientSettings.builder()
                .requireProofKey(false)
                .requireAuthorizationConsent(false)
                .build();
    }

    private TokenSettings tokenSettings() {
        return TokenSettings.builder()
                .accessTokenFormat(OAuth2TokenFormat.REFERENCE) //要配置不然无法生成token
                .accessTokenTimeToLive(Duration.ofHours(1))
                .build();
    }

    /**
     * 公用客户端
     */
    private RegisteredClient publicClient() {
        //token有效时间
        //是否重用刷新token false 会重新生成刷新token
        //用OAuth2TokenFormat.SELF_CONTAINED 无法在OAuth2AccessTokenGenerator#generate(OAuth2TokenContext context)中生成token?
        return RegisteredClient.withId("public-client")
                .clientName("公开客户端")
                .clientId("public-client")
                .clientAuthenticationMethod(ClientAuthenticationMethod.NONE) //不需要再验证客户端
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("https://www.baidu.com")
                .scope("openid")
                .clientSettings(ClientSettings.builder().requireProofKey(true).build())
                .tokenSettings(TokenSettings.builder().accessTokenTimeToLive(Duration.ofHours(1)).build())
                .build();
    }

    /**
     * 应用启动的时候自动添加用户
     */
    @Override
    public void afterPropertiesSet() {
        UserDetails administrator = administrator();
        userDetailsManager.deleteUser(administrator.getUsername());
        userDetailsManager.createUser(administrator);
        registeredClientRepository.save(fakePublicClient());
    }
}
