package org.goodsence.authenticationserver.service.impl;

import org.goodsence.authenticationserver.dao.CustomAuthorityDao;
import org.goodsence.authenticationserver.service.arg.AuthoritiesAddDto;
import org.goodsence.authenticationserver.service.dto.AuthoritiesDto;
import org.goodsence.authenticationserver.service.arg.AuthoritiesUpdateDto;
import org.goodsence.authenticationserver.dao.domain.CustomAuthority;
import org.goodsence.authenticationserver.service.AuthorityService;
import org.goodsence.common.BusinessException;
import org.goodsence.common.model.Status;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @User nav1c
 * @Author zty
 * @Date 2023/2/23 19:51
 * @ProjectName modern-arranged-marriage
 * @Version 1.0.0
 */
@Service
public class AuthorityServiceImpl implements AuthorityService {

    private final CustomAuthorityDao customAuthorityDao;

    @Autowired
    public AuthorityServiceImpl(CustomAuthorityDao customAuthorityDao) {
        this.customAuthorityDao = customAuthorityDao;
    }

    @Override
    public void create(AuthoritiesAddDto authoritiesAddDto) {
        CustomAuthority customAuthority = customAuthorityDao.selectByName(authoritiesAddDto.getAuthorityName());
        if (customAuthority != null) {
            throw new BusinessException("已存在：" + authoritiesAddDto.getAuthorityName());
        }
        customAuthority = new CustomAuthority();
        BeanUtils.copyProperties(authoritiesAddDto, customAuthority);
        customAuthority.setStatus(Status.ACTIVE.value());
        customAuthorityDao.insertSelective(customAuthority);
    }

    @Override
    public void delete(String authorityId) {
        customAuthorityDao.delete(authorityId);
    }

    @Override
    public AuthoritiesDto get(String authorityId) {
        AuthoritiesDto authoritiesDto = null;
        final CustomAuthority customAuthority = customAuthorityDao.selectByPrimaryKey(authorityId);
        if (customAuthority != null){
            authoritiesDto = new AuthoritiesDto();
            BeanUtils.copyProperties(customAuthority, authoritiesDto);
        }
        return authoritiesDto;
    }

    @Override
    public void update(AuthoritiesUpdateDto authoritiesUpdateDto) {
        final CustomAuthority customAuthority = customAuthorityDao.selectByPrimaryKey(authoritiesUpdateDto.getAuthority());
        if (customAuthority == null) {
            throw new BusinessException("不存在： " + authoritiesUpdateDto.getAuthority());
        }
        BeanUtils.copyProperties(authoritiesUpdateDto, customAuthority);
        customAuthorityDao.updateByPrimaryKeySelective(customAuthority);
    }

    @Override
    public void changeStatus(String id, Integer status) {
        // TODO document why this method is empty
    }
}
