package org.goodsence.authenticationserver.config;

import org.goodsence.authenticationserver.core.OncePasswordAuthenticationConverter;
import org.goodsence.authenticationserver.core.OncePasswordAuthenticationProvider;
import org.goodsence.authenticationserver.core.PasswordAuthenticationConverter;
import org.goodsence.authenticationserver.core.PasswordAuthenticationProvider;
import org.goodsence.common.KvOperations;
import org.goodsence.security.FlexibleUserDetailsService;
import org.goodsence.security.oncepassword.*;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.OAuth2Token;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/16
 * @project advanced-cloud
 */
@Deprecated
public class ExtensiveGrantTypeConfig {

    @Deprecated
    public OncePasswordStore oncePasswordStore(KvOperations kvOperations) {
        return new DefaultOncePasswordStore(kvOperations);
    }

    @Deprecated
    public OncePasswordSender oncePasswordSender(OncePasswordStore oncePasswordStore){
        return new DefaultOncePasswordSender(oncePasswordStore, null);
    }

    @Deprecated
    public OncePasswordAuthenticationConverter emailCodeAuthenticationConverter() {
        return new OncePasswordAuthenticationConverter();
    }

    @Deprecated
//    @Bean
    public PasswordAuthenticationConverter passwordAuthenticationConverter() {
        return new PasswordAuthenticationConverter();
    }

    @Deprecated
    public OncePasswordAuthenticationProvider emailCodeAuthenticationProvider(OAuth2AuthorizationService authorizationService,
                                                                              OAuth2TokenGenerator<? extends OAuth2Token> tokenGenerator,
                                                                              FlexibleUserDetailsService flexibleUserDetailsService,
                                                                              OncePasswordStore oncePasswordStore) {
        return new OncePasswordAuthenticationProvider(authorizationService, tokenGenerator, flexibleUserDetailsService, oncePasswordStore);
    }

    /**
     * @deprecated  千万不能用用Bean 否则会覆盖{@link org.springframework.security.authentication.dao.DaoAuthenticationProvider}
     */
    @Deprecated
//    @Autowired
//    @Bean
    public PasswordAuthenticationProvider passwordAuthenticationProvider(OAuth2AuthorizationService authorizationService,
                                                                         OAuth2TokenGenerator<? extends OAuth2Token> tokenGenerator,
                                                                         UserDetailsService userDetailsService, PasswordEncoder passwordEncoder){
        return new PasswordAuthenticationProvider(authorizationService, tokenGenerator, userDetailsService, passwordEncoder);
    }

}
