package org.goodsence.authenticationserver.core;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.OAuth2Token;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationGrantAuthenticationToken;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;

import java.util.Map;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-01-25 11:09:14
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public class PasswordAuthenticationProvider extends AbstractCustomAuthenticationProvider {

    private final DaoAuthenticationProvider daoAuthenticationProvider;

    public PasswordAuthenticationProvider(OAuth2AuthorizationService authorizationService,
                                          OAuth2TokenGenerator<? extends OAuth2Token> tokenGenerator,
                                          UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        super(AuthorizationGrantType.PASSWORD, authorizationService, tokenGenerator);
        this.daoAuthenticationProvider = new DaoAuthenticationProvider(passwordEncoder);
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
    }

    @Override
    protected UsernamePasswordAuthenticationToken createUsernamePasswordAuthenticationToken(Authentication authentication) {
        Map<String, Object> params =  ((OAuth2AuthorizationGrantAuthenticationToken)authentication).getAdditionalParameters();
        UsernamePasswordAuthenticationToken unauthenticated = UsernamePasswordAuthenticationToken.unauthenticated(params.get(OAuth2ParameterNames.USERNAME),
                params.get(OAuth2ParameterNames.PASSWORD));
        Authentication authenticate = daoAuthenticationProvider.authenticate(unauthenticated);
        return new UsernamePasswordAuthenticationToken(authenticate.getPrincipal(), authenticate.getCredentials(), authenticate.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return PasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
