package org.goodsence.authenticationserver.core;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationGrantAuthenticationToken;

import java.util.Map;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-01-25 11:22:11
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public class OncePasswordAuthenticationToken extends OAuth2AuthorizationGrantAuthenticationToken {

    public OncePasswordAuthenticationToken(Authentication clientPrincipal, Map<String, Object> additionalParameters) {
        super(ExtensiveAuthorizationGrantType.ONCE_PASSWORD, clientPrincipal, additionalParameters);
    }
}
