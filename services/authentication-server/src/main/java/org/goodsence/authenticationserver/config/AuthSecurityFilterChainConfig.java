package org.goodsence.authenticationserver.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.goodsence.authenticationserver.core.CustomClientAuthenticationFailureHandler;
import org.goodsence.authenticationserver.core.PasswordAuthenticationConverter;
import org.goodsence.authenticationserver.core.PasswordAuthenticationProvider;
import org.goodsence.security.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.OAuth2Token;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configurers.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 授权服务拦截器配置
 * @date 2023-12-18 14:57:48
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Configuration
public class AuthSecurityFilterChainConfig {

    private static final int AUTHORIZATION_SERVER_FILTER_ORDER = Constants.RESOURCE_SERVER_FILTER_ORDER - 1;

    private final PasswordAuthenticationProvider passwordAuthenticationProvider;

    @Autowired
    public AuthSecurityFilterChainConfig(OAuth2AuthorizationService authorizationService,
                                         OAuth2TokenGenerator<? extends OAuth2Token> tokenGenerator,
                                         UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        this.passwordAuthenticationProvider = new PasswordAuthenticationProvider(authorizationService, tokenGenerator, userDetailsService, passwordEncoder);
    }

    /**
     * <p>
     *     想看遍历了哪些过滤器可以debug {@link org.springframework.web.filter.OncePerRequestFilter#doFilterInternal}
     * </p>
     * <p>
     * 入口 {@link AuthenticationManager#authenticate(Authentication)}<br>
     * endpointsMatcher ---> {@link OAuth2AuthorizationServerConfigurer#init(HttpSecurity)}<br>
     * </p>
     * <p>
     *     {@link org.springframework.security.oauth2.server.authorization.web.OAuth2ClientAuthenticationFilter} 包含 <br>
     *    Ant [pattern='/oauth2/token', POST],Ant [pattern='/oauth2/introspect', POST],Ant [pattern='/oauth2/revoke', POST],Ant [pattern='/oauth2/device_authorization', POST]
     * </p>
     * <p>
     *     登录接口 /login --->
     *     {@link org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter}
     *     获取授权码:
     *     /oauth2/authorize  -->
     *     {@link org.springframework.security.oauth2.server.authorization.web.OAuth2AuthorizationEndpointFilter#doFilterInternal(HttpServletRequest, HttpServletResponse, FilterChain)}<br>
     *     /oauth2/device_verification   --> {@link org.springframework.security.oauth2.server.authorization.web.OAuth2DeviceVerificationEndpointFilter}<br>
     *     /oauth2/device_authorization  --> {@link org.springframework.security.oauth2.server.authorization.web.OAuth2DeviceAuthorizationEndpointFilter}<br>
     *     获取token:
     *     /oauth2/token -->
     *     先客户端认证:
     *     {@link org.springframework.security.oauth2.server.authorization.web.OAuth2ClientAuthenticationFilter}
     *     {@link org.springframework.security.oauth2.server.authorization.web.OAuth2TokenEndpointFilter}<br>
     *     核验token:
     *     /oauth2/introspect   --> {@link org.springframework.security.oauth2.server.authorization.web.OAuth2TokenIntrospectionEndpointFilter}<br>
     *     撤回token:
     *    /oauth2/revoke    --> {@link org.springframework.security.oauth2.server.authorization.web.OAuth2TokenRevocationEndpointFilter}<br>
     *    /**
     *    /oauth2/jwks  --> {@link org.springframework.security.oauth2.server.authorization.web.NimbusJwkSetEndpointFilter}<br>
     *     /.well-known/oauth-authorization-server   --> {@link org.springframework.security.oauth2.server.authorization.web.OAuth2AuthorizationServerMetadataEndpointFilter}  查看授权服务器信息<br>
     * </p>
     */
    @Bean
    @Order(AUTHORIZATION_SERVER_FILTER_ORDER)
    public SecurityFilterChain authFilterChain(HttpSecurity http) throws Exception {
        OAuth2AuthorizationServerConfigurer authorizationServerConfigurer = new OAuth2AuthorizationServerConfigurer();
        RequestMatcher endpointsMatcher = authorizationServerConfigurer.getEndpointsMatcher();

        http.securityMatchers(rmc ->
                        rmc.requestMatchers(endpointsMatcher)
                                .requestMatchers("/login", "/logout", "/once-password/**")
                )
                .authorizeHttpRequests(authorize ->
                        authorize.requestMatchers("/login", "/once-password/**")
                                .permitAll()
                                .anyRequest()
                                .authenticated()
                );

        http.csrf(csrf -> csrf.ignoringRequestMatchers(endpointsMatcher));

//        http.addFilterBefore(oncePasswordPreAuthenticatedProcessingFilter, AbstractPreAuthenticatedProcessingFilter.class);

        http.formLogin(Customizer.withDefaults());

        http.with(authorizationServerConfigurer, oaasConfiger ->
                oaasConfiger.tokenEndpoint(oAuth2TokenEndpointConfigurer ->
                                oAuth2TokenEndpointConfigurer.authenticationProvider(passwordAuthenticationProvider)
                                        .accessTokenRequestConverter(new PasswordAuthenticationConverter()))
                        .clientAuthentication(oacaConfigurer ->
                        oacaConfigurer.errorResponseHandler(new CustomClientAuthenticationFailureHandler())
                )
        );

        return http.build();
    }

}
