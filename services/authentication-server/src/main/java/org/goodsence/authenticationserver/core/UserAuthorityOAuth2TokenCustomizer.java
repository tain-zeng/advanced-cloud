package org.goodsence.authenticationserver.core;

import org.goodsence.security.CustomOAuth2TokenClaimNames;
import org.goodsence.security.CustomScopes;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenClaimsContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UserAuthorityOAuth2TokenCustomizer implements OAuth2TokenCustomizer<OAuth2TokenClaimsContext> {

    @Override
    public void customize(OAuth2TokenClaimsContext context) {
        Set<String> authorizedScopes = context.getAuthorizedScopes();
        if (authorizedScopes == null || !authorizedScopes.contains(CustomScopes.USER_AUTHORITY)){
            return;
        }
        if (context.getPrincipal() instanceof AbstractAuthenticationToken authenticationToken) { //UsernamePasswordA...?
            Collection<GrantedAuthority> authorityCollection = authenticationToken.getAuthorities();
            if (authorityCollection == null || authorityCollection.isEmpty()) {
                return;
            }
            Set<String> authorities = authorityCollection.stream()
                    .filter(Objects::nonNull)
                    .map(GrantedAuthority::getAuthority)
                    .filter(StringUtils::hasText)
                    .collect(Collectors.toUnmodifiableSet());
            context.getClaims().claim(CustomOAuth2TokenClaimNames.AUTHORITIES, authorities);
        }
    }
}
