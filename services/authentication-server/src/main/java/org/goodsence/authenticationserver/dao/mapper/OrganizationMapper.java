package org.goodsence.authenticationserver.dao.mapper;

import org.goodsence.authenticationserver.dao.domain.Organization;

import java.util.List;

/**
* @author mengtain
* @description 针对表【organization】的数据库操作Mapper
* @createDate 2024-12-11 21:01:29
* @Entity generator.domain.Organization
*/
public interface OrganizationMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Organization record);

    int insertSelective(Organization record);

    Organization selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Organization record);

    int updateByPrimaryKey(Organization record);

    List<Organization> selectOrganizationsWithAncestorsStartingWith(String pattern);

    Organization selectByName(String name);

    List<Organization> selectOrganizationsByAncestors(String ancestors);
}
