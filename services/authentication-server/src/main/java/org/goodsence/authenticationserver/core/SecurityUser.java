package org.goodsence.authenticationserver.core;

import lombok.Data;
import org.goodsence.authenticationserver.dao.domain.CustomUser;
import org.goodsence.common.model.Status;
import org.goodsence.security.DataAuthUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author zengty
 * @apiNote {@link org.springframework.security.core.userdetails.User}
 * @date 2024/12/16
 * @project advanced-cloud
 */
@Data
public class SecurityUser implements UserDetails, DataAuthUser, Serializable {

    /**
     * 组合优先于继承的设计原则
     */
    private CustomUser customUser;

    private Collection<? extends GrantedAuthority> authorities;

    private Collection<String> dataAuthorities;

    public SecurityUser() {
        this.customUser = new CustomUser();
        this.authorities = new HashSet<>();
        this.dataAuthorities = new HashSet<>();
    }

    public SecurityUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this(username, password, true, true, true, true, authorities, null, null);
    }
    
    public SecurityUser(String username, String password, boolean enabled, boolean accountNonExpired,
                        boolean credentialsNonExpired, boolean accountNonLocked,
                        Collection<? extends GrantedAuthority> authorities, String email,
                        Collection<String> dataAuthorities) {
        this.customUser = new CustomUser();
        customUser.setId(username);
        customUser.setPassword(password);
        customUser.setEmail(email);
        customUser.setStatus(enabled && accountNonExpired && credentialsNonExpired && accountNonLocked ?
                Status.ACTIVE.value() : Status.DEACTIVATED.value());
        this.authorities = authorities;
        this.dataAuthorities = dataAuthorities;
    }

    public void setEmail(String email) {
        customUser.setEmail(email);
    }

    @Override
    public String getPassword() {
        return this.customUser.getPassword();
    }

    @Override
    public String getUsername() {
        return this.customUser.getId();
    }

    @Override
    public boolean isEnabled() {
        return Status.ACTIVE.value().equals(customUser.getStatus());
    }

    @Serial
    private static final long serialVersionUID = 1L;
}
