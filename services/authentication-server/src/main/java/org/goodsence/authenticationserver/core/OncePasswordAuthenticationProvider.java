package org.goodsence.authenticationserver.core;

import org.goodsence.security.AccountType;
import org.goodsence.security.oncepassword.OncePasswordStore;
import org.goodsence.security.FlexibleUserDetailsService;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.OAuth2Token;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationGrantAuthenticationToken;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;

import java.util.Map;
import java.util.Objects;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-01-25 11:09:14
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public class OncePasswordAuthenticationProvider extends AbstractCustomAuthenticationProvider {

    private final OncePasswordStore oncePasswordStore;

    private final FlexibleUserDetailsService flexibleUserDetailsService;

    public OncePasswordAuthenticationProvider(OAuth2AuthorizationService authorizationService,
                                              OAuth2TokenGenerator<? extends OAuth2Token> tokenGenerator,
                                              FlexibleUserDetailsService flexibleUserDetailsService,
                                              OncePasswordStore oncePasswordStore) {
        super(ExtensiveAuthorizationGrantType.ONCE_PASSWORD, authorizationService, tokenGenerator);
        this.flexibleUserDetailsService = flexibleUserDetailsService;
        this.oncePasswordStore = oncePasswordStore;
    }

    @Override
    protected UsernamePasswordAuthenticationToken createUsernamePasswordAuthenticationToken(Authentication authentication) {
        Map<String, Object> params = ((OAuth2AuthorizationGrantAuthenticationToken)authentication).getAdditionalParameters();
        String email = (String) params.get(ExtensiveOauth2ParameterNames.ACCOUNT);
        String accountType = (String)params.get(ExtensiveOauth2ParameterNames.ACCOUNT_TYPE);
        String oncePassword = oncePasswordStore.getOncePassword(AccountType.of(accountType), email);
        boolean check = Objects.equals(oncePassword, Objects.toString(params.get(ExtensiveOauth2ParameterNames.ONCE_PASSWORD), ""));
        if (!check){
            throw new BadCredentialsException("一次性密码验证失败: " + accountType);
        }
        UserDetails userDetails = flexibleUserDetailsService.loadUserByAccount(AccountType.of(accountType), email);
        return UsernamePasswordAuthenticationToken.authenticated(userDetails.getUsername(),
                authentication.getCredentials(), userDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return OncePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
