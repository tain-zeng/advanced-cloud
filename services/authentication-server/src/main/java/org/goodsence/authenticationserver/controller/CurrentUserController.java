package org.goodsence.authenticationserver.controller;

import org.goodsence.security.SecurityContextUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tain
 * @apiNote
 * @date 2024/11/29
 * @project advanced-cloud
 */
@RestController
@RequestMapping("/current-user")
public class CurrentUserController {

    @GetMapping
    public Object currentUser() {
        return SecurityContextUtils.getCurrentAuthentication();
    }
}
