package org.goodsence.authenticationserver.service.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * authority_authorities
 * @author zty
 */
@Data
public class AuthoritiesDto implements Serializable {
    private Long authorityId;

    /**
     * 权限标识
     */
    private String authority;

    /**
     * 权限名称
     */
    private String authorityName;

    /**
     * 删除标志 0 正常 1删除
     */
    private Byte deleted;

    @Serial
    private static final long serialVersionUID = 1L;
}