package org.goodsence.authenticationserver.service.arg;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import org.goodsence.common.RegexPattern;

/**
 * @author tain
 * @apiNote
 * @date 2024/9/29
 * @project modern-arranged-marriage
 */
@Data
public class RegisterArgs {

    @NotBlank
    @Pattern(regexp = RegexPattern.Constant.USERNAME, message = "字母开头，且只能包含字母数字下划线")
    private String username;

    @NotBlank
    @Pattern(regexp = RegexPattern.Constant.EMAIL, message = "不是邮箱")
    private String email;

    @NotBlank
    @Pattern(regexp = RegexPattern.Constant.COMPLEX_1, message = "密码少于6位")
    private String password;

    @NotBlank
    private String smsCode;
}
