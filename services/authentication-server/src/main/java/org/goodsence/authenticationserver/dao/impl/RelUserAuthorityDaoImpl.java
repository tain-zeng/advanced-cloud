package org.goodsence.authenticationserver.dao.impl;

import org.goodsence.authenticationserver.dao.RelUserAuthorityDao;
import org.goodsence.authenticationserver.dao.domain.RelCustomUserCustomAuthority;
import org.goodsence.authenticationserver.dao.mapper.RelCustomUserCustomAuthorityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zty
 * @apiNote
 * @user nav1c
 * @date 2023/12/10 1:15
 * @project advanced-cloud
 */
@Repository
public class RelUserAuthorityDaoImpl implements RelUserAuthorityDao {

    private final RelCustomUserCustomAuthorityMapper relCustomUserCustomAuthorityMapper;

    @Autowired
    public RelUserAuthorityDaoImpl(RelCustomUserCustomAuthorityMapper relCustomUserCustomAuthorityMapper) {
        this.relCustomUserCustomAuthorityMapper = relCustomUserCustomAuthorityMapper;
    }

    @Override
    public void insertSelective(RelCustomUserCustomAuthority entity) {
        relCustomUserCustomAuthorityMapper.insertSelective(entity);
    }

    @Override
    public void updateByPrimaryKeySelective(RelCustomUserCustomAuthority entity) {
        throw new UnsupportedOperationException();
    }

    @Cacheable(value = "RelCustomUserCustomAuthority", key = "#key")
    @Override
    public RelCustomUserCustomAuthority selectByPrimaryKey(RelCustomUserCustomAuthority key) {
        return relCustomUserCustomAuthorityMapper.selectByPrimaryKey(key);
    }

    @Caching(evict = {
            @CacheEvict(value = "RelCustomUserCustomAuthority", key = "#key")
    })
    @Override
    public void delete(RelCustomUserCustomAuthority key) {
        relCustomUserCustomAuthorityMapper.deleteByPrimaryKey(key);
    }

    @Override
    public void deleteByUsername(String username) {
        relCustomUserCustomAuthorityMapper.deleteByUsername(username);
    }

    /**
     * @deprecated 转移到 {@link org.goodsence.authenticationserver.dao.CustomUserDao}
     */
    @Deprecated
    @Override
    public List<RelCustomUserCustomAuthority> selectAuthoritiesByUsername(String username) {
        return relCustomUserCustomAuthorityMapper.selectAuthoritiesByUsername(username);
    }
}
