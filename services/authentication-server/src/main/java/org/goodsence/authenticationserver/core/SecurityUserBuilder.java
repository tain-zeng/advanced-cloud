package org.goodsence.authenticationserver.core;

import org.goodsence.security.DataAuthUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @author tain
 * @apiNote
 * @date 2024/9/27
 * @project modern-arranged-marriage
 */
public final class SecurityUserBuilder {

    private String username;

    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    private String email;

    private Collection<String> dataAuthorities;

    private boolean enabled;

    public static SecurityUserBuilder builder() {
        return new SecurityUserBuilder();
    }

    public static SecurityUserBuilder withUsername(String username) {
        return builder().username(username);
    }

    public static SecurityUserBuilder withUserDetails(UserDetails userDetails) {
        SecurityUserBuilder userBuilder = withUsername(userDetails.getUsername())
                .password(userDetails.getPassword())
                .authorities(userDetails.getAuthorities())
                .accountNonExpired(userDetails.isAccountNonExpired())
                .accountNonLocked(userDetails.isAccountNonLocked())
                .credentialsNonExpired(userDetails.isCredentialsNonExpired())
                .enabled(userDetails.isEnabled());
        if(userDetails instanceof DataAuthUser dataAuthUser){
            userBuilder.dataAuthorities(dataAuthUser.getDataAuthorities());
        }
        return userBuilder;
    }

    public SecurityUserBuilder username(String username) {
        this.username = username;
        return this;
    }

    public SecurityUserBuilder enabled(boolean b) {
        this.enabled = b;
        return this;
    }

    public SecurityUserBuilder credentialsNonExpired(boolean b) {
        return enabled(b);
    }

    public SecurityUserBuilder authorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
        return this;
    }

    public SecurityUserBuilder dataAuthorities(Collection<String> authorities) {
        this.dataAuthorities = authorities;
        return this;
    }

    public SecurityUserBuilder email(String email) {
        this.email = email;
        return this;
    }

    public SecurityUserBuilder accountNonLocked(boolean b) {
        return enabled(b);
    }

    public SecurityUserBuilder accountNonExpired(boolean b) {
        return enabled(b);
    }

    public SecurityUserBuilder password(String password) {
        this.password = password;
        return this;
    }

    public SecurityUser build(){
        return new SecurityUser(username, password, enabled, true, true, true, authorities, email,
                dataAuthorities);
    }
}
