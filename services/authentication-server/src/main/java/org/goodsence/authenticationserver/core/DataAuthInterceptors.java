package org.goodsence.authenticationserver.core;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.goodsence.security.SecurityContextUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 数据权限mybatis拦截器
 * <p>
 * 数据不能通过用户依赖组织 而是直接依赖组织, 万一用户组织变更呢?
 */
@Intercepts({
        @Signature(
                type = StatementHandler.class, //org.apache.ibatis.executor.Executor 别搞错
                method = "prepare", //这是Executor的方法，点进去可以看到
                args = {Connection.class, Integer.class} //这是query的参数，不能错。
        )
})
@Slf4j
//@Component //有有些数据不包含orgid
public class DataAuthInterceptors implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        final Object target = invocation.getTarget();
        if (target instanceof StatementHandler statementHandler) {
            Collection<? extends GrantedAuthority> authorities = SecurityContextUtils.getAuthorities();
            if(authorities.contains(new SimpleGrantedAuthority("DATA_all"))) {
                return invocation.proceed();
            }
            BoundSql boundSql = statementHandler.getBoundSql();
            String originalSql = boundSql.getSql();
            Field field = boundSql.getClass().getDeclaredField("sql");
            String modifiedSql;
            String str = SecurityContextUtils.getDataAuthorities()
                    .stream()
                    .filter(StringUtils::hasText)
                    .collect(Collectors.joining(","));
            if (!str.isEmpty()) {
                modifiedSql = "SELECT t.* FROM (" + originalSql + ") t WHERE t.org_id in (" + str +")";
            }else {
                String username = SecurityContextUtils.getCurrentUsername();
                modifiedSql = "SELECT t.* FROM (" + originalSql + ") t WHERE t.created_by = '" + username +"'" ;
            }
            field.setAccessible(true);
            field.set(boundSql, modifiedSql);
            try {
                return invocation.proceed();
            }catch (Exception e) {
                log.warn("数据鉴权发生错误", e);
            }
            field.setAccessible(true);
            field.set(boundSql, originalSql);
        }
        return invocation.proceed();
    }
}
