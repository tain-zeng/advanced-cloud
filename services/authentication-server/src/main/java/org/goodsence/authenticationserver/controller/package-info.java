/**
 * @apiNote 控制层
 * 他妈的路径不存在 放行了也会403
 * @author tain
 * @date 2024/11/26
 * @project advanced-cloud
 */
package org.goodsence.authenticationserver.controller;