package org.goodsence.authenticationserver.core;

import org.goodsence.authenticationserver.dao.domain.CustomAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/16
 * @project advanced-cloud
 */
public class SecurityAuthority implements GrantedAuthority {

    private final CustomAuthority customAuthority;

    public SecurityAuthority() {
        this(new CustomAuthority());
    }

    public SecurityAuthority(CustomAuthority customAuthority) {
        Assert.notNull(customAuthority, "customAuthority must not be null");
        this.customAuthority = customAuthority;
    }

    @Override
    public String getAuthority() {
        return customAuthority.getId();
    }
}
