package org.goodsence.authenticationserver.core;

import org.goodsence.security.DataAuthUser;
import org.goodsence.security.CustomOAuth2TokenClaimNames;
import org.goodsence.security.CustomScopes;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenClaimsContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;

import java.util.Collection;
import java.util.Set;

public class UserDataAuthorityOAuth2TokenCustomizer implements OAuth2TokenCustomizer<OAuth2TokenClaimsContext> {

    @Override
    public void customize(OAuth2TokenClaimsContext context) {
        Set<String> authorizedScopes = context.getAuthorizedScopes();
        if (authorizedScopes == null || !authorizedScopes.contains(CustomScopes.USER_DATA_AUTHORITY)){
            return;
        }
        if (context.getPrincipal() instanceof AbstractAuthenticationToken authenticationToken) {
            Collection<GrantedAuthority> authorityCollection = authenticationToken.getAuthorities();
            if (authorityCollection == null || authorityCollection.isEmpty()) {
                return;
            }
            if (authenticationToken.getPrincipal() instanceof DataAuthUser dataAuthUser) {
                Collection<String> dataAuthorities = dataAuthUser.getDataAuthorities();
                if(dataAuthorities == null) {
                    return;
                }
                context.getClaims().claim(CustomOAuth2TokenClaimNames.DATA_AUTHORITIES, dataAuthorities);
            }
        }
    }
}
