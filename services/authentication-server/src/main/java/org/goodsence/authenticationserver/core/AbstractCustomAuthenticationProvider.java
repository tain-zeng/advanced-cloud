
package org.goodsence.authenticationserver.core;

import org.goodsence.security.CustomScopes;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.core.*;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2TokenType;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AccessTokenAuthenticationToken;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2ClientAuthenticationToken;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.context.AuthorizationServerContextHolder;
import org.springframework.security.oauth2.server.authorization.token.DefaultOAuth2TokenContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenGenerator;

import java.security.Principal;
import java.util.Set;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 一次性密码
 * @date 2024-01-25 11:09:14
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public abstract class AbstractCustomAuthenticationProvider implements AuthenticationProvider {

    protected final AuthorizationGrantType authorizationGrantType;

    protected final OAuth2TokenGenerator<? extends OAuth2Token> tokenGenerator;

    protected final OAuth2AuthorizationService authorizationService;

    protected AbstractCustomAuthenticationProvider(AuthorizationGrantType authorizationGrantType, OAuth2AuthorizationService authorizationService,
                                         OAuth2TokenGenerator<? extends OAuth2Token> tokenGenerator) {
        this.authorizationGrantType = authorizationGrantType;
        this.authorizationService = authorizationService;
        this.tokenGenerator = tokenGenerator;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken authenticationToken = createUsernamePasswordAuthenticationToken(authentication);
        OAuth2ClientAuthenticationToken clientPrincipal = null;
        Object principal = authentication.getPrincipal();
        if (OAuth2ClientAuthenticationToken.class.isAssignableFrom(principal.getClass())) {
            clientPrincipal = (OAuth2ClientAuthenticationToken)principal;
        }

        if (clientPrincipal == null || !clientPrincipal.isAuthenticated()) {
            throw new OAuth2AuthenticationException(OAuth2ErrorCodes.INVALID_CLIENT);
        }
        RegisteredClient registeredClient = clientPrincipal.getRegisteredClient();
        if (registeredClient == null) {
            throw new OAuth2AuthenticationException("客户端不存在");
        }
        if (!registeredClient.getAuthorizationGrantTypes().contains(authorizationGrantType)) {
            throw new OAuth2AuthenticationException("不支持的授权模式:" + authorizationGrantType.getValue());
        }
        DefaultOAuth2TokenContext.Builder tokenContextBuilder = DefaultOAuth2TokenContext.builder()
                .tokenType(OAuth2TokenType.ACCESS_TOKEN)
                .authorizationGrantType(authorizationGrantType)
                .registeredClient(registeredClient)
                .principal(authenticationToken)
                .authorizationServerContext(AuthorizationServerContextHolder.getContext())
                .authorizationGrant(authentication)
                .authorizedScopes(Set.of(CustomScopes.USER_AUTHORITY, CustomScopes.USER_DATA_AUTHORITY));
        OAuth2TokenContext tokenContext = tokenContextBuilder.build();

        OAuth2Token generatedAccessToken = tokenGenerator.generate(tokenContext);
        assert generatedAccessToken != null;
        OAuth2AccessToken accessToken = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER,
                generatedAccessToken.getTokenValue(),
                generatedAccessToken.getIssuedAt(),
                generatedAccessToken.getExpiresAt(),
                tokenContext.getAuthorizedScopes()
        );
        OAuth2Authorization.Builder authorizationBuilder = OAuth2Authorization.withRegisteredClient(registeredClient)
                .authorizationGrantType(authorizationGrantType)
                .authorizedScopes(registeredClient.getScopes())
                .principalName(accessToken.getTokenValue()).attributes(stringObjectMap ->
                        stringObjectMap.put(Principal.class.getName(), authenticationToken));
        if (generatedAccessToken instanceof ClaimAccessor claimAccessor) {
            authorizationBuilder.token(accessToken, metadata ->
                    metadata.put(OAuth2Authorization.Token.CLAIMS_METADATA_NAME, claimAccessor.getClaims()));
        } else {
            authorizationBuilder.accessToken(accessToken);
        }

        OAuth2RefreshToken refreshToken = null;
        if (registeredClient.getAuthorizationGrantTypes().contains(AuthorizationGrantType.REFRESH_TOKEN)
                && !clientPrincipal.getClientAuthenticationMethod().equals(ClientAuthenticationMethod.NONE)) {
            tokenContext = tokenContextBuilder.tokenType(OAuth2TokenType.REFRESH_TOKEN).build();
            OAuth2Token generatedRefreshToken = tokenGenerator.generate(tokenContext);
            if (!(generatedRefreshToken instanceof OAuth2RefreshToken)) {
                OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.SERVER_ERROR,
                        "The token generator failed to generate the refresh token.",
                        "https://datatracker.ietf.org/doc/html/rfc6749#section-5.2");
                throw new OAuth2AuthenticationException(error);
            }

            refreshToken = (OAuth2RefreshToken)generatedRefreshToken;
            authorizationBuilder.refreshToken(refreshToken);
        }
        OAuth2Authorization authorization = authorizationBuilder.build();
        authorizationService.save(authorization); //如果不存入token，会导致token active = false
        return new OAuth2AccessTokenAuthenticationToken(registeredClient, authentication, accessToken, refreshToken);
    }

    protected abstract UsernamePasswordAuthenticationToken createUsernamePasswordAuthenticationToken(Authentication authentication);
}
