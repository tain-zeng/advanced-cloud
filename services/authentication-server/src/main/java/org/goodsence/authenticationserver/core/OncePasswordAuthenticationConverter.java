package org.goodsence.authenticationserver.core;

import jakarta.servlet.http.HttpServletRequest;
import org.goodsence.security.SecurityContextUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2ClientAuthenticationToken;
import org.springframework.security.web.authentication.AuthenticationConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote
 * @date 2024-01-25 11:08:32
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public class OncePasswordAuthenticationConverter implements AuthenticationConverter {

    @Override
    public Authentication convert(HttpServletRequest request) {
        MultiValueMap<String, String> formParameters = ExtensiveOAuth2EndpointUtils.getFormParameters(request);
        final String grantType = formParameters.getFirst(OAuth2ParameterNames.GRANT_TYPE);
        if (!ExtensiveAuthorizationGrantType.ONCE_PASSWORD.getValue().equals(grantType)) {
            return null;
        }
        String oncePasswordType = formParameters.getFirst(ExtensiveOauth2ParameterNames.ACCOUNT_TYPE);
        if (!StringUtils.hasText(oncePasswordType)) {
            return null;
        }
        String mobile = formParameters.getFirst(ExtensiveOauth2ParameterNames.ACCOUNT);
        if (!StringUtils.hasText(mobile)){
            return null;
        }
        String smsCode = formParameters.getFirst(ExtensiveOauth2ParameterNames.ONCE_PASSWORD);
        if (!StringUtils.hasText(smsCode)){
            return null;
        }
        mobile = mobile.trim();
        Map<String, Object> params = new LinkedHashMap<>();
        params.put(ExtensiveOauth2ParameterNames.ACCOUNT_TYPE, oncePasswordType);
        params.put(ExtensiveOauth2ParameterNames.ACCOUNT, mobile);
        params.put(ExtensiveOauth2ParameterNames.ONCE_PASSWORD, smsCode);
        //AbstractAuthenticationToken
        //为什么拿到的是Oauth2ClientAuthenticationToken 而不是 UserNamePasswordAuthenticationToken?
        Authentication clientPrincipal = SecurityContextUtils.getCurrentAuthentication();
        if (clientPrincipal instanceof OAuth2ClientAuthenticationToken authenticationToken){
            return new OncePasswordAuthenticationToken(authenticationToken, params);
        }
        throw new OAuth2AuthenticationException(OAuth2ErrorCodes.INVALID_REQUEST);
    }
}
