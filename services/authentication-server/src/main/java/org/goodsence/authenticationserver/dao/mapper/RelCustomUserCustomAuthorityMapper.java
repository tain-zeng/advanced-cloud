package org.goodsence.authenticationserver.dao.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.goodsence.authenticationserver.dao.domain.RelCustomUserCustomAuthority;

import java.util.List;
import java.util.Set;

@Mapper
public interface RelCustomUserCustomAuthorityMapper {
    int deleteByPrimaryKey(RelCustomUserCustomAuthority key);

    int insertSelective(RelCustomUserCustomAuthority entity);

    RelCustomUserCustomAuthority selectByPrimaryKey(RelCustomUserCustomAuthority relCustomUserCustomAuthority);

    void insertBatch(@Param("relUserAuthorityKeys") Set<RelCustomUserCustomAuthority> relCustomUserCustomAuthorities);

    void deleteByUsername(@Param("customUserId") String username);

    @Deprecated
    List<RelCustomUserCustomAuthority> selectAuthoritiesByUsername(@Param("customUserId")String username);
}