package org.goodsence.authenticationserver.core;

import org.goodsence.authenticationserver.dao.CustomAuthorityDao;
import org.goodsence.authenticationserver.dao.OrganizationDao;
import org.goodsence.authenticationserver.dao.RelUserAuthorityDao;
import org.goodsence.authenticationserver.dao.CustomUserDao;
import org.goodsence.authenticationserver.dao.domain.*;
import org.goodsence.common.BusinessException;
import org.goodsence.common.model.Status;
import org.goodsence.security.AccountType;
import org.goodsence.security.FlexibleUserDetailsService;
import org.goodsence.security.SecurityContextUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author zty
 * @Version 1.0.0
 * @Date 2023 02 14 16 07
 **/
public class CustomUserDetailsManager implements UserDetailsManager, FlexibleUserDetailsService {

    private final CustomUserDao customUserDao;

    private final RelUserAuthorityDao relUserAuthorityDao;

    private final CustomAuthorityDao customAuthorityDao;

    private final OrganizationDao organizationDao;

    private final PasswordEncoder passwordEncoder;

    private final Collection<AccountType> supportedAccountTypes;

    public CustomUserDetailsManager(CustomUserDao customUserDao, RelUserAuthorityDao relUserAuthorityDao,
                                    CustomAuthorityDao customAuthorityDao, OrganizationDao organizationDao,
                                    PasswordEncoder passwordEncoder) {
        this(customUserDao, relUserAuthorityDao, customAuthorityDao, organizationDao, passwordEncoder, Set.of(AccountType.USERNAME, AccountType.of("email")));
    }

    public CustomUserDetailsManager(CustomUserDao customUserDao, RelUserAuthorityDao relUserAuthorityDao,
                                    CustomAuthorityDao customAuthorityDao, OrganizationDao organizationDao,
                                    PasswordEncoder passwordEncoder, Collection<AccountType> supportedAccountTypes) {
        this.customUserDao = customUserDao;
        this.relUserAuthorityDao = relUserAuthorityDao;
        this.customAuthorityDao = customAuthorityDao;
        this.organizationDao = organizationDao;
        this.passwordEncoder = passwordEncoder;
        this.supportedAccountTypes = supportedAccountTypes;
    }

    @Override
    public UserDetails loadUserByAccount(AccountType accountType, String account){
        CustomUser customUser = switch (accountType.getType()) {
            case "username" -> customUserDao.selectByPrimaryKey(account);
            case "email" -> customUserDao.selectByEmail(account);
            default -> throw new UnsupportedOperationException("Unsupported once password type: " + accountType);
        };
        if (customUser == null) {
            throw new UsernameNotFoundException(account);
        }
        return SecurityUserBuilder.withUsername(customUser.getId())
                .password(customUser.getPassword())
                .enabled(Status.ACTIVE.value().equals(customUser.getStatus()))
                .authorities(getGrantedAuthoritiesByUsername(customUser.getId()))
                .dataAuthorities(getDataAuthorities(customUser.getOrgId()))
                .build();
    }

    @Override
    public Collection<AccountType> getSupportedAccountTypes() {
        return supportedAccountTypes;
    }

    private Set<String> getDataAuthorities(Integer id) {
        Organization organization = organizationDao.selectByPrimaryKey(id);
        List<Organization> organizations = organizationDao.selectAllSubOrganizations(id);
        organizations.add(organization);
        return organizations.stream()
                .filter(Objects::nonNull)
                .map(Organization::getId)
                .map(Objects::toString)
                .collect(Collectors.toSet());
    }

    private Set<GrantedAuthority> getGrantedAuthoritiesByUsername(String username) {
        List<CustomAuthority> customAuthorities = customAuthorityDao.selectByUsername(username);
        return customAuthorities.stream()
                .filter(Objects::nonNull)
                .map((Function<CustomAuthority, GrantedAuthority>) customAuthority -> customAuthority::getId)
                .collect(Collectors.toSet());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createUser(UserDetails userDetails) {
        String username = userDetails.getUsername();
        if (userExists(username)){
            throw new IllegalStateException("用户已存在");
        }
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        if (authorities != null) {
            Collection<RelCustomUserCustomAuthority> authoritiesToCreate = authorities.stream()
                    .filter(Objects::nonNull)
                    .map(authority -> new RelCustomUserCustomAuthority(username, authority.getAuthority()))
                    .toList();
            for (RelCustomUserCustomAuthority authority : authoritiesToCreate){
                RelCustomUserCustomAuthority relCustomUserCustomAuthority = relUserAuthorityDao.selectByPrimaryKey(authority);
                if (relCustomUserCustomAuthority == null){
                    relUserAuthorityDao.insertSelective(authority);
                }
            }
        }
        CustomUser customUser = new CustomUser();
        customUser.setId(userDetails.getUsername());
        customUser.setPassword(passwordEncoder.encode(userDetails.getPassword()));
        customUser.setStatus(Status.ACTIVE.value());
        customUserDao.insertSelective(customUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUser(UserDetails userDetails) {
        if (userDetails.getPassword() != null){
            throw new BusinessException("不能更改密码!");
        }
        String username = userDetails.getUsername();
        if (!userExists(username)){
            throw new IllegalStateException("用户不存在!");
        }
        CustomUser customUser = new CustomUser();
        customUser.setId(userDetails.getUsername());
        customUser.setPassword(passwordEncoder.encode(userDetails.getPassword()));
        customUser.setStatus(Status.ACTIVE.value());
        customUserDao.updateByPrimaryKeySelective(customUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteUser(String username) {
        relUserAuthorityDao.deleteByUsername(username);
        customUserDao.delete(username);
    }

    /**
     *  更改密码, 该方法不对新旧密码加密，建议上级调用者加密
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void changePassword(String oldPassword, String newPassword) {
        String username = SecurityContextUtils.getCurrentUsername();
        UserDetails userDetails = loadUserByUsername(username);
        if (!passwordEncoder.matches(oldPassword, userDetails.getPassword())) {
            throw new BusinessException("密码不对");
        }
        String encodedPassword = passwordEncoder.encode(newPassword);
        CustomUser customUser = new CustomUser();
        customUser.setId(username);
        customUser.setPassword(encodedPassword);
        customUserDao.updateByPrimaryKeySelective(customUser);
        SecurityContextUtils.getSecurityContext().setAuthentication(null);
        LogoutHandler logoutHandler = new SecurityContextLogoutHandler();
        logoutHandler.logout(null, null, null);
    }

    @Override
    public boolean userExists(String username) {
        CustomUser customUser = customUserDao.selectByPrimaryKey(username);
        return customUser != null;
    }
}
