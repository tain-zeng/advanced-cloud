package org.goodsence.authenticationserver.core;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.goodsence.web.Results;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import java.io.IOException;
import java.util.Objects;

/**
 * @author tain
 * @apiNote 自定义客户端授权失败处理器
 * @date 2024/11/28
 * @project advanced-cloud
 */
public class CustomClientAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        ResponseEntity<Object> unauthorized = Results.unauthorized();
        response.setStatus(unauthorized.getStatusCode().value());
        response.getWriter().write(Objects.toString(unauthorized.getBody()));
        HttpHeaders headers = unauthorized.getHeaders();
        headers.forEach((key, value) -> response.addHeader(Objects.toString(key), Objects.toString(value)));
        response.getWriter().flush();
    }
}
