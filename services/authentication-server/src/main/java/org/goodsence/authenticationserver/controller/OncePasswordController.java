package org.goodsence.authenticationserver.controller;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.common.BusinessException;
import org.goodsence.common.KvOperations;
import org.goodsence.security.AccountType;
import org.goodsence.security.oncepassword.OncePasswordSender;
import org.springframework.web.bind.annotation.*;

/**
 * @author tain
 * @apiNote
 * @date 2024/12/2
 * @project advanced-cloud
 */
@Slf4j
@RestController
@RequestMapping("/once-password")
public class OncePasswordController {

    private final KvOperations cache;

    private final OncePasswordSender oncePasswordSender;

    public OncePasswordController(KvOperations cache, OncePasswordSender oncePasswordSender) {
        this.cache = cache;
        this.oncePasswordSender = oncePasswordSender;
    }

    @GetMapping("/{type}") //todo 改为 POST
    public void sendCode(@PathVariable String type, @RequestParam String account) {
        if (cache.contains(account)) {
            throw new BusinessException("稍后再试!");
        }
        oncePasswordSender.send(AccountType.of(type), account);
        cache.put(account, 1, 60);
    }
}
