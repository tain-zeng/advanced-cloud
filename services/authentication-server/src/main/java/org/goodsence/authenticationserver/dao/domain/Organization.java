package org.goodsence.authenticationserver.dao.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.goodsence.common.model.Entity;

/**
 *
 * @TableName organization
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Organization extends Entity<Integer> {

    public Integer getParentId() {
        return getOrgId();
    }

    /**
     * 
     */
    private String ancestors;
}