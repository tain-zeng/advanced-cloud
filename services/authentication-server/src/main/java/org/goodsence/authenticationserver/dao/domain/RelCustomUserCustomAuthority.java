package org.goodsence.authenticationserver.dao.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.goodsence.common.model.Relation;

import java.io.Serial;

/**
 * authority_user_authorities
 * @author zty
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RelCustomUserCustomAuthority extends Relation<RelCustomUserCustomAuthority> {

    private String customUserId;

    private String customAuthorityId;

    public RelCustomUserCustomAuthority(String customUserId, String customAuthorityId) {
        this.customUserId = customUserId;
        this.customAuthorityId = customAuthorityId;
    }

    @Serial
    private static final long serialVersionUID = 1L;
}