package org.goodsence.authenticationserver.dao;

import org.goodsence.common.model.BaseDao;
import org.goodsence.authenticationserver.dao.domain.Organization;

import java.util.List;

public interface OrganizationDao extends BaseDao<Integer, Organization> {

    List<Organization> selectAllSubOrganizations(Integer id);

    List<Organization> selectDirectSubOrganizations(Integer id);
}
