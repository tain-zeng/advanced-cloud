package org.goodsence.authenticationserver.core;

import org.goodsence.common.CanNotInstantiateReason;

/**
 * @author tain
 * @apiNote {@link org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames}
 * @date 2024/9/25
 * @project modern-arranged-marriage
 */
@Deprecated
public final class ExtensiveOauth2ParameterNames {
    @Deprecated
    public static final String EMAIL = "email";
    @Deprecated
    public static final String EMAIL_CODE = "emailCode";
    /**
     * @deprecated 弃用短信验证码模式
     */
    @Deprecated
    public static final String MOBILE = "mobile";
    /**
     * @deprecated 弃用短信验证码模式
     */
    @Deprecated
    public static final String SMS_CODE = "smsCode";

    @Deprecated
    public static final String ACCOUNT = "account";

    @Deprecated
    public static final String ONCE_PASSWORD = "oncePassword";

    @Deprecated
    public static final String ACCOUNT_TYPE = "accountType";

    private ExtensiveOauth2ParameterNames() {
        throw new IllegalStateException(CanNotInstantiateReason.ENUM_CLASS.descriptions());
    }
}
