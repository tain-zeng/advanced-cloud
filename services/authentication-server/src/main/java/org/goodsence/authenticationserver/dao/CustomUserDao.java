package org.goodsence.authenticationserver.dao;


import org.goodsence.authenticationserver.dao.domain.CustomUser;
import org.goodsence.common.model.StringKeyDao;

public interface CustomUserDao extends StringKeyDao<CustomUser> {

    @Override
    default CustomUser selectByName(String name) {
        return selectByPrimaryKey(name);
    }

    CustomUser selectByEncryptedMobile(String encryptedPhone);

    CustomUser selectByEmail(String email);

}