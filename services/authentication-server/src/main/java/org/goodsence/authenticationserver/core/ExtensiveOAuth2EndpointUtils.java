package org.goodsence.authenticationserver.core;

import jakarta.servlet.http.HttpServletRequest;
import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * @author tain
 * @apiNote {@link org.springframework.security.oauth2.server.authorization.web.authentication.OAuth2EndpointUtils}
 * @date 2024/11/26
 * @project advanced-cloud
 */
@Deprecated
public class ExtensiveOAuth2EndpointUtils {

    private ExtensiveOAuth2EndpointUtils() {
        throw new IllegalStateException(CanNotInstantiateReason.UTILITY_CLASS.descriptions());
    }

    public static MultiValueMap<String, String> getFormParameters(HttpServletRequest request) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameterMap.forEach((key, values) -> {
            String queryString = StringUtils.hasText(request.getQueryString()) ? request.getQueryString() : "";
            if (!queryString.contains(key)) {
                for (String value : values) {
                    parameters.add(key, value);
                }
            }

        });
        return parameters;
    }
}
