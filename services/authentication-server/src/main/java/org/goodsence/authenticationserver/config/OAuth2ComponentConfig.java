package org.goodsence.authenticationserver.config;

import org.goodsence.authenticationserver.core.CompositeOAuth2TokenCustomizer;
import org.goodsence.authenticationserver.core.RedisOAuth2AuthorizationService;
import org.goodsence.authenticationserver.core.UserAuthorityOAuth2TokenCustomizer;
import org.goodsence.authenticationserver.core.UserDataAuthorityOAuth2TokenCustomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.security.oauth2.core.OAuth2Token;
import org.springframework.security.oauth2.server.authorization.*;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.token.*;

import java.util.Set;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote OAuth2组件配置
 * @date 2024-01-24 19:14:29
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Configuration
public class OAuth2ComponentConfig {

    /**
     * 配置客户端 项目启动时插入数据库
     * @return RegisteredClientRepository
     */
    @Bean
    @Autowired
    public RegisteredClientRepository registeredClientRepository(JdbcOperations jdbcOperations){
        return new JdbcRegisteredClientRepository(jdbcOperations);
    }

    @Bean
    @Autowired
    public OAuth2AuthorizationConsentService oAuth2AuthorizationConsentService(JdbcOperations jdbcOperations, RegisteredClientRepository registeredClientRepository){
        return new JdbcOAuth2AuthorizationConsentService(jdbcOperations, registeredClientRepository);
    }

    @Bean
    @Autowired
    public OAuth2AuthorizationService oAuth2AuthorizationService(RedisOperations<Object, Object> redisOperations){
        return new RedisOAuth2AuthorizationService(redisOperations);
    }

    @Bean
    public OAuth2TokenGenerator<OAuth2Token> oAuth2TokenGenerator() {
        OAuth2AccessTokenGenerator oAuth2AccessTokenGenerator = new OAuth2AccessTokenGenerator();
        oAuth2AccessTokenGenerator.setAccessTokenCustomizer(oAuth2TokenCustomizer());
        OAuth2RefreshTokenGenerator refreshTokenGenerator = new OAuth2RefreshTokenGenerator();
        return new DelegatingOAuth2TokenGenerator(oAuth2AccessTokenGenerator, refreshTokenGenerator);
    }

    private static OAuth2TokenCustomizer<OAuth2TokenClaimsContext> oAuth2TokenCustomizer() {
        return new CompositeOAuth2TokenCustomizer(Set.of(new UserAuthorityOAuth2TokenCustomizer(),
                new UserDataAuthorityOAuth2TokenCustomizer()));
    }
}
