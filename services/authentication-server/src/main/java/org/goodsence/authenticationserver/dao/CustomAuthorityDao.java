package org.goodsence.authenticationserver.dao;

import org.goodsence.authenticationserver.dao.domain.CustomAuthority;
import org.goodsence.common.model.StringKeyDao;

import java.util.List;

public interface CustomAuthorityDao extends StringKeyDao<CustomAuthority> {

    @Deprecated
    CustomAuthority selectByAuthorityName(String authorityName);

    List<CustomAuthority> selectByUsername(String username);
}