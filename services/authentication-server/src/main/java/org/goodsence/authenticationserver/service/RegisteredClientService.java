package org.goodsence.authenticationserver.service;

import org.goodsence.authenticationserver.service.arg.ClientRegisterArgs;

/**
 * @author tain
 * @apiNote 注册客户端服务
 * @date 2024/9/24
 * @project modern-arranged-marriage
 */
public interface RegisteredClientService {

    void registerClient(ClientRegisterArgs args);
}
