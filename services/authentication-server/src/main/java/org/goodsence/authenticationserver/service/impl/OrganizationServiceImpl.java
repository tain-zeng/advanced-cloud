package org.goodsence.authenticationserver.service.impl;

import org.goodsence.authenticationserver.service.OrganizationService;
import org.goodsence.common.BusinessException;
import org.goodsence.authenticationserver.dao.OrganizationDao;
import org.goodsence.authenticationserver.dao.domain.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/11
 * @project advanced-cloud
 */
@Service
public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationDao organizationDao;

    @Autowired
    public OrganizationServiceImpl(OrganizationDao organizationDao) {
        this.organizationDao = organizationDao;
    }

    @Override
    public void create(Organization organization) {
        Organization sentinel = organizationDao.selectByName(organization.getName());
        if (sentinel != null) {
            throw new BusinessException("Organization already exists");
        }
        Organization parent = organizationDao.selectByPrimaryKey(organization.getParentId());
        if (parent == null) {
            throw new BusinessException("Parent organization does not exist");
        }
        organization.setAncestors(organization.getAncestors() + "," + organization.getParentId());
        organization.setId(null);
        organizationDao.insertSelective(organization);
    }

    @Override
    public void delete(Integer id) {
        Organization organization = organizationDao.selectByPrimaryKey(id);
        if (organization == null) {
            return;
        }
        List<Organization> subOrganizations = organizationDao.selectAllSubOrganizations(id);
        if(subOrganizations != null && !subOrganizations.isEmpty()) {
            throw new BusinessException("subOrganizations not empty");
        }
        organizationDao.delete(id);
    }
}
