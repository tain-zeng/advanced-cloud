package org.goodsence.authenticationserver.service.arg;

import lombok.Data;

/**
 * @author zty
 * @apiNote 手机号注册参数
 * @user nav1c
 * @date 2023/12/9 13:07
 * @project advanced-cloud
 */
@Data
public class RegisterByMobileArgs {

    private String phone;

    private String contractWay;
}
