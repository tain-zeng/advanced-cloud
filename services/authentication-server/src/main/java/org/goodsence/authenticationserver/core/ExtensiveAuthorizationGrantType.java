package org.goodsence.authenticationserver.core;

import org.goodsence.common.CanNotInstantiateReason;
import org.springframework.security.oauth2.core.AuthorizationGrantType;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote {@link AuthorizationGrantType}
 * @date 2024-02-07 10:12:48
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
@Deprecated
public final class ExtensiveAuthorizationGrantType {

    public static final AuthorizationGrantType SMS_CODE = new AuthorizationGrantType("sms_code");
    public static final AuthorizationGrantType EMAIL_CODE = new AuthorizationGrantType("email");

    public static final AuthorizationGrantType ONCE_PASSWORD = new AuthorizationGrantType("once_password");

    private ExtensiveAuthorizationGrantType(){
        throw new IllegalStateException(CanNotInstantiateReason.ENUM_CLASS.descriptions());
    }
}
