package org.goodsence.messageserver.dispatcher.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.common.utils.SerializedUtils;
import org.goodsence.messageserver.dispatcher.core.CustomMessage;
import org.goodsence.messageserver.dispatcher.dao.domain.DefaultCustomMessage;
import org.goodsence.messageserver.dispatcher.service.MqService;
import org.goodsence.security.SecurityContextUtils;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.io.IOException;
import java.util.Date;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/26
 * @project advanced-cloud
 */
@Slf4j
@Service
public class MqServiceImpl implements MqService {

    private final AmqpTemplate amqpTemplate;

    private final Queue queue;

    private final Scheduler scheduler;

    @Autowired
    public MqServiceImpl(AmqpTemplate amqpTemplate, Queue queue, Scheduler scheduler) {
        this.amqpTemplate = amqpTemplate;
        this.queue = queue;
        this.scheduler = scheduler;
    }

    @Override
    public Mono<Void> sendMessage(DefaultCustomMessage messageArgs) {
        return Mono.fromCallable(() -> convertToMsg(messageArgs))
                .subscribeOn(scheduler)
                .doOnError(throwable -> log.error("转换消息实体失败: {}", throwable.getMessage(), throwable))
                .doOnSuccess(message -> log.info("消息: {}", message))
                .doOnSuccess(message -> amqpTemplate.convertAndSend(queue.getName(), message))
                .then();
    }

    private static Message convertToMsg(CustomMessage messageArgs) {
        DefaultCustomMessage message = new DefaultCustomMessage();
        message.setSender(SecurityContextUtils.getCurrentUsername());
        message.setCreatedAt(new Date());
        message.setRecipient(messageArgs.getRecipient());
        message.setContent(messageArgs.getContent());
        try {
            return new Message(SerializedUtils.objectToBytes(message));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
