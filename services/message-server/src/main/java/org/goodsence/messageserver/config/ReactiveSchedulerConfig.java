package org.goodsence.messageserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.Executor;

/**
 * @author zengty
 * @apiNote
 * @date 2025/1/2
 * @project advanced-cloud
 */
@Configuration
public class ReactiveSchedulerConfig {

    @Bean
    public Scheduler reactiveScheduler(Executor executor) {
        return Schedulers.fromExecutor(executor);
    }
}
