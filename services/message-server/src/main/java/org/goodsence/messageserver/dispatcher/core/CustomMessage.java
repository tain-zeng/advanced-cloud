package org.goodsence.messageserver.dispatcher.core;

/**
 * @author <a href="nav1cat@outlook.com">nav1c</a>
 * @apiNote 消息
 * @date 2023-12-16 19:43:23
 * @project advanced-cloud
 * @kit IntelliJ IDEA
 */
public interface CustomMessage{

    String getId();
    /**
     * @return 发送者
     */
    String getSender();

    /**
     * @return 接收者
     */
    String getRecipient();

    /**
     * @return 消息内容
     */
    String getContent();

    Integer getStatus();

    /**
     * @return 回执
     */
    boolean isReceived();

}
