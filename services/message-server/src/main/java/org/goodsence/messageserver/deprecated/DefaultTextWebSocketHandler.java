//package org.goodsence.messageserver.deprecated;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.lang.NonNull;
//import org.springframework.web.socket.CloseStatus;
//import org.springframework.web.socket.TextMessage;
//import org.springframework.web.socket.WebSocketMessage;
//import org.springframework.web.socket.WebSocketSession;
//import org.springframework.web.socket.handler.TextWebSocketHandler;
//
//import java.io.IOException;
//
///**
// * @author zty
// * @apiNote
// * @project_name the-revolt
// * @user tain
// * @create_at 2023/6/15 16:46
// * @create_vio IntelliJ IDEA
// */
//@Slf4j
//public class DefaultTextWebSocketHandler extends TextWebSocketHandler {
//
//    private final WebSocketSessionHolder webSocketSessionHolder;
//
//    public DefaultTextWebSocketHandler(WebSocketSessionHolder webSocketSessionHolder) {
//        this.webSocketSessionHolder = webSocketSessionHolder;
//    }
//
//    @Override
//    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
//        final String receivedMessage = message.getPayload();
//        session.sendMessage(new TextMessage(receivedMessage));
//    }
//
//    /**
//     * 建立连接后触发的回调
//     * @param session WebSocketSession
//     */
//    @Override
//    public void afterConnectionEstablished(@NonNull WebSocketSession session) {
//        webSocketSessionHolder.add(session);
//        log.info("有新连接加入！当前在线人数为:{}", webSocketSessionHolder.size());
//    }
//
//    /**
//     * 服务器收到消息时触发的回调
//     * @param session WebSocketSession
//     * @param message WebSocketMessage<?>
//     */
//    @Override
//    public void handleMessage(@NonNull WebSocketSession session, WebSocketMessage<?> message) {
//        log.info("收到新的消息！内容:{}", message.getPayload());
//    }
//
//    /**
//     * 发生异常，关闭连接
//     * @param session WebSocketSession
//     * @param exception Throwable
//     */
//    @Override
//    public void handleTransportError(@NonNull WebSocketSession session, @NonNull Throwable exception) {
//        webSocketSessionHolder.remove(session);
//        final String msg = exception.getMessage();
//        log.info("websocket发生异常:{}", msg);
//    }
//
//    /**
//     * 关闭连接
//     * @param session WebSocketSession
//     * @param closeStatus CloseStatus
//     */
//    @Override
//    public void afterConnectionClosed(@NonNull WebSocketSession session, @NonNull CloseStatus closeStatus) {
//        webSocketSessionHolder.remove(session);
//        log.info("webSocket关闭连接，状态：{}，当前连接数：{}", closeStatus, webSocketSessionHolder.size());
//    }
//
//}
