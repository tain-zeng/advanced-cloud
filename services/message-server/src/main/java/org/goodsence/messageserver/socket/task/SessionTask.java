package org.goodsence.messageserver.socket.task;

import org.goodsence.messageserver.socket.core.WebSocketSessionHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/26
 * @project advanced-cloud
 */
@Component
public class SessionTask {

    private final WebSocketSessionHolder webSocketSessionHolder;

    @Autowired
    public SessionTask(WebSocketSessionHolder webSocketSessionHolder) {
        this.webSocketSessionHolder = webSocketSessionHolder;
    }

    @Scheduled(fixedRate = 3*60*1000)
    public void sessionEvict(){
        webSocketSessionHolder.removeAllClosed();
    }
}
