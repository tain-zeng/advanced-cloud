package org.goodsence.messageserver.socket.core;


import org.springframework.web.reactive.socket.WebSocketSession;

import java.util.Collection;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author zty
 * @apiNote
 * @user nav1c
 * @date 2023/12/4 19:49
 * @project advanced-cloud
 */
public interface WebSocketSessionHolder {

    /**
     * 添加session
     * @param session WebSocketSession
     */
    void add(WebSocketSession session);

    default Supplier<String> idFetcher(WebSocketSession session) {
        return session::getId;
    }

    default String fetchId(WebSocketSession session) {
        return idFetcher(session).get();
    }

    default WebSocketSession findSession(String id) {
        return getSessions().stream().filter(session -> fetchId(session).equals(id)).findFirst().orElse(null);
    }

    default WebSocketSession findOpenedSession(String id) {
        return getOpenSessions().stream()
                .filter(session -> fetchId(session).equals(id))
                .findFirst().orElse(null);
    }

    default void remove(WebSocketSession session) {
        remove(fetchId(session));
    }

    /**
     * 移除session
     */
    void remove(String id);

    default boolean contains(WebSocketSession session) {
        return getSessions().contains(session);
    }

    /**
     * 是否包含
     * @return 是否包含
     */
    default boolean contains(String id) {
        return getSessions().stream()
                .anyMatch(session -> fetchId(session).equals(id));
    }

    default boolean isEmpty(){
        return size() == 0;
    }

    /**
     * session个数
     * @return session个数
     */
    default int size() {
        return getSessions().size();
    }

    default void removeAll(Collection<String> ids) {
        for (String id : ids) {
            remove(id);
        }
    }

    /**
     * 获取容器内所有session.
     * @return Set<WebSocketSession>
     */
    Collection<WebSocketSession> getSessions();

    default Collection<WebSocketSession> getOpenSessions() {
        return getSessions().stream()
                .filter(WebSocketSession::isOpen)
                .collect(Collectors.toSet());
    }

    /**
     * 移除关闭的session
     */
    default void removeAllClosed() {
        Set<String> closedSessions = getSessions().stream()
                .filter(webSocketSession -> !webSocketSession.isOpen())
                .map(this::fetchId)
                .collect(Collectors.toSet());
        removeAll(closedSessions);
    }

    /**
     * 清空所有session
     */
    void clear();
}
