package org.goodsence.messageserver;

import org.goodsence.common.EnvironmentPrinter;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@RefreshScope //全局刷新
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {
        "org.goodsence.messageserver.config",
        "org.goodsence.messageserver.route.config",
        "org.goodsence.messageserver.socket.config",
        "org.goodsence.messageserver.socket.controller",
        "org.goodsence.messageserver.socket.task",
        "org.goodsence.messageserver.dispatcher.config",
        "org.goodsence.messageserver.dispatcher.dao.impl",
        "org.goodsence.messageserver.dispatcher.service.impl",
        "org.goodsence.messageserver.dispatcher.controller",
        "org.goodsence.messageserver.dispatcher.listener"
})
public class MessageServerApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(MessageServerApplication.class, args);
        EnvironmentPrinter.printInfo(context);
    }
}
