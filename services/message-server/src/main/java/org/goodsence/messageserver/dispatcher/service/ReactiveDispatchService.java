package org.goodsence.messageserver.dispatcher.service;

import org.goodsence.messageserver.dispatcher.core.CustomMessage;
import reactor.core.publisher.Mono;

/**
 * @author zengty
 * @apiNote
 * @date 2025/1/1
 * @project advanced-cloud
 */
public interface ReactiveDispatchService {

    Mono<Void> dispatch(CustomMessage customMessage);
}
