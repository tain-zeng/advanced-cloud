package org.goodsence.messageserver.dispatcher.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.messageserver.dispatcher.core.CustomMessage;
import org.goodsence.messageserver.dispatcher.dao.MessageDao;
import org.goodsence.messageserver.dispatcher.dao.domain.DefaultCustomMessage;
import org.goodsence.messageserver.dispatcher.service.ReactiveDispatchService;
import org.goodsence.messageserver.route.RouteHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
@Slf4j
@Service
public class ReactiveDispatchServiceImpl implements ReactiveDispatchService {

    private final MessageDao messageDao;

    private final RouteHolder routeHolder;

    private final WebClient webClient;

    private final Scheduler scheduler;

    @Autowired
    public ReactiveDispatchServiceImpl(MessageDao messageDao, RouteHolder routeHolder, Scheduler scheduler) {
        this.messageDao = messageDao;
        this.routeHolder = routeHolder;
        this.scheduler = scheduler;
        this.webClient = WebClient.builder().build();
    }

    @Override
    public Mono<Void> dispatch(CustomMessage customMessage) {
        if (customMessage instanceof DefaultCustomMessage defaultSocketMessage) {
            return Mono.fromRunnable(() -> messageDao.insertSelective(defaultSocketMessage))
                    .subscribeOn(scheduler)
                    .then(Mono.fromCallable(() -> routeHolder.get(customMessage.getRecipient())).subscribeOn(scheduler))
                    .doOnError(throwable -> log.info("获取路由发生异常: {}", throwable.getMessage(), throwable))
                    //flatMap会等上一步操作完成才会执行并获取结果
                    .doOnSuccess(route -> {
                        log.info("路由: {}", route);
                        if (!StringUtils.hasText(route)) {
                            log.warn("路由为空，取消消息转发");
                        }
                    })
                    .filter(StringUtils::hasText)
                    //如果路由为null，后续操作都不会再调用
                    .flatMap(route ->
                            webClient.post()
                                    .uri(route)
                                    .bodyValue(defaultSocketMessage)
                                    .exchangeToMono(ClientResponse::toBodilessEntity)
                                    .doOnError(throwable -> log.error("webclient请求发送异常: {}", throwable.getMessage(), throwable))
                                    .map(ResponseEntity::getStatusCode)
                                    .doOnSuccess(httpStatusCode -> log.info("消息转发状态: {}", httpStatusCode))
                    )
                    .then();
        }
        return Mono.empty().doOnNext(o -> log.warn("Unsupported socket message: {}", customMessage)).then();
    }
}
