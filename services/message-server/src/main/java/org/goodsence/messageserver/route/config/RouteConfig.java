package org.goodsence.messageserver.route.config;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import org.goodsence.messageserver.route.CustomRoute;
import org.goodsence.messageserver.route.RedisRouteHolder;
import org.goodsence.messageserver.route.RouteHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.lang.NonNull;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
@Configuration
public class RouteConfig implements EnvironmentAware {

    private final NacosDiscoveryProperties nacosDiscoveryProperties;

    private Environment environment;

    @Autowired
    public RouteConfig(NacosDiscoveryProperties nacosDiscoveryProperties) {
        this.nacosDiscoveryProperties = nacosDiscoveryProperties;
    }

    @Override
    public void setEnvironment(@NonNull Environment environment) {
        this.environment = environment;
    }

    @Bean
    public CustomRoute customRoute() {
        String route = "http://" + nacosDiscoveryProperties.getIp() + ":"
                + environment.getProperty("server.port") + "/sockets";
        return () -> route;
    }

    @Bean
    public RouteHolder routeHolder(RedisOperations<String, String> redisOperations) {
        return new RedisRouteHolder(redisOperations);
    }
}
