//package org.goodsence.messageserver.deprecated;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.util.CollectionUtils;
//import org.springframework.web.socket.TextMessage;
//import org.springframework.web.socket.WebSocketSession;
//
//import java.io.IOException;
//import java.util.Collection;
//import java.util.Map;
//import java.util.Objects;
//
///**
// * @author zengty
// * @apiNote
// * @date 2024/12/25
// * @project advanced-cloud
// */
//@Slf4j
//public class DefaultMessageOperations implements MessageOperations {
//
//    private final WebSocketSessionHolder webSocketSessionHolder;
//
//    public DefaultMessageOperations(WebSocketSessionHolder webSocketSessionHolder) {
//        this.webSocketSessionHolder = webSocketSessionHolder;
//    }
//
//    @Override
//    public void sendMessage(String message, Map<String, Object> conditions) {
//        Collection<WebSocketSession> openSessions = webSocketSessionHolder.getOpenSessions();
//        for (WebSocketSession session : openSessions) {
//            if (!session.isOpen()) {
//                continue;
//            }
//            if (!CollectionUtils.isEmpty(conditions)){ //条件不为空
//                final Map<String, Object> attributes = session.getAttributes();
//                boolean shouldNotSend = conditions.entrySet()
//                        .stream()
//                        .anyMatch(entry -> !Objects.equals(entry.getValue(), attributes.get(entry.getKey())));
//                if (shouldNotSend){
//                    continue;
//                }
//            }
//            try {
//                session.sendMessage(new TextMessage(message));
//            } catch (IOException ignored) {
//                // Handle exception if necessary
//            }
//        }
//    }
//
//    @Override
//    public void sendPrivateMessage(String sessionId, String message) {
//        WebSocketSession session = webSocketSessionHolder.findOpenedSession(sessionId);
//        if (session == null) {
//            //todo 入库
//            log.warn("No opened websocket session found for session id {}", sessionId);
//            log.info("消息入库: {}, {}", sessionId, message);
//            return;
//        }
//        try {
//            session.sendMessage(new TextMessage(message));
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
//}
