package org.goodsence.messageserver.socket.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2TokenIntrospectionClaimNames;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

@Slf4j
public class DefaultWebSocketHandler implements WebSocketHandler {

    private final WebSocketSessionHolder webSocketSessionHolder;

    private final Scheduler scheduler;

    public DefaultWebSocketHandler(WebSocketSessionHolder webSocketSessionHolder, Scheduler scheduler) {
        this.webSocketSessionHolder = webSocketSessionHolder;
        this.scheduler = scheduler;
    }

    /**
     * 响应连接请求?
     */
    @NonNull
    @Override
    public Mono<Void> handle(@NonNull WebSocketSession session) {
        return ReactiveSecurityContextHolder.getContext()
                .doOnError(throwable -> log.error("获取登录用户失败: {}", throwable.getMessage(), throwable))
                .map(securityContext -> securityContext.getAuthentication().getName())
                .doOnError(throwable -> log.error("获取登录用户失败：{}", throwable.getMessage(), throwable))
                .doOnSuccess(username -> log.info("登录用户名: {}", username))
                .filter(StringUtils::hasText)
                .doOnSuccess(username -> session.getAttributes().put(OAuth2TokenIntrospectionClaimNames.USERNAME, username))
                .doOnSuccess(s -> webSocketSessionHolder.add(session))
                .doOnError(throwable -> log.error("保存session失败: {}", throwable.getMessage(), throwable))
                .doOnSuccess(s -> log.info("用户{}连接, 当前在线人数: {}", webSocketSessionHolder.fetchId(session), webSocketSessionHolder.size()))
                .doOnError(throwable -> log.error("发生异常: {}", throwable.getMessage(), throwable))
                .subscribeOn(scheduler)
                .then(
                        session.receive()
                                .map(msg -> session.textMessage("服务器已接收消息，但不作处理！"))
                                .flatMap(webSocketMessage -> session.send(Mono.just(webSocketMessage)))
                                .doFinally(signalType -> {
                                    log.info("连接结束, signalType: {}", signalType);
                                    webSocketSessionHolder.remove(session);
                                    log.info("用户{}断开连接, 当前在线人数: {}", webSocketSessionHolder.fetchId(session), webSocketSessionHolder.size());
                                })
                                .then()
                );
    }

}
