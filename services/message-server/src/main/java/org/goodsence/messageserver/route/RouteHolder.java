package org.goodsence.messageserver.route;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public interface RouteHolder {

    void add(String username, String routeId);

    String get(String username);

    void remove(String username);

    boolean contains(String username);

    int size();

    void clear();
}
