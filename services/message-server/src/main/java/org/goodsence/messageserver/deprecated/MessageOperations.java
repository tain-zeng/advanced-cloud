//package org.goodsence.messageserver.deprecated;
//
//import org.goodsence.messageserver.dispatcher.core.CustomMessage;
//
//import java.util.Collections;
//import java.util.Map;
//
///**
// * @author zengty
// * @apiNote
// * @date 2024/12/25
// * @project advanced-cloud
// */
//public interface MessageOperations {
//
//    default void sendMessage(final CustomMessage message) {
//        if (message.getRecipient() != null){
//            sendPrivateMessage(message.getRecipient(), message.getContent());
//            return;
//        }
//        broadcast(message.getContent());
//    }
//
//    default void broadcast(final String message) {
//        sendMessage(message, Collections.emptyMap());
//    }
//
//    /**
//     * 发消息 <br>
//     * @param message 消息
//     * @param conditions 条件
//     */
//    void sendMessage(String message, Map<String, Object> conditions);
//
//    void sendPrivateMessage(String sessionId, String message);
//}
