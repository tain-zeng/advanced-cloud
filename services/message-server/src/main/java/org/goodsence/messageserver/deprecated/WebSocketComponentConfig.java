//package org.goodsence.messageserver.deprecated;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
//import org.springframework.web.socket.WebSocketHandler;
//
///**
// * @author zengty
// * @apiNote
// * @date 2024/12/30
// * @project advanced-cloud
// */
//@Configuration
//public class WebSocketComponentConfig {
//
//    @Bean
//    public AuthHandshakeInterceptor authHandshakeInterceptor(OpaqueTokenIntrospector opaqueTokenIntrospector) {
//        return new AuthHandshakeInterceptor(opaqueTokenIntrospector);
//    }
//
//    @Bean
//    public WebSocketSessionHolder webSocketSessionHolder(RouteHolder routeHolder, CustomRoute customRoute) {
//        return new DefaultWebSocketSessionHolder(routeHolder, customRoute);
//    }
//
//    @Bean
//    public WebSocketHandler webSocketHandler(WebSocketSessionHolder webSocketSessionHolder){
//        return new DefaultTextWebSocketHandler(webSocketSessionHolder);
//    }
//
//    @Bean
//    public MessageOperations messageOperations(WebSocketSessionHolder webSocketSessionHolder) {
//        return new DefaultMessageOperations(webSocketSessionHolder);
//    }
//}
