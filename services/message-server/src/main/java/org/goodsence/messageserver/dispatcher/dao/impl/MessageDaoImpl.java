package org.goodsence.messageserver.dispatcher.dao.impl;

import org.goodsence.messageserver.dispatcher.dao.MessageDao;
import org.goodsence.messageserver.dispatcher.dao.domain.DefaultCustomMessage;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
@Repository
public class MessageDaoImpl implements MessageDao {

    @Override
    public void insertSelective(DefaultCustomMessage entity) {

    }

    @Override
    public void updateByPrimaryKeySelective(DefaultCustomMessage entity) {

    }

    @Override
    public DefaultCustomMessage selectByPrimaryKey(String key) {
        return null;
    }

    @Override
    public void delete(String id) {

    }

    @Override
    public List<DefaultCustomMessage> selectUnDispatch() {
        return List.of();
    }
}
