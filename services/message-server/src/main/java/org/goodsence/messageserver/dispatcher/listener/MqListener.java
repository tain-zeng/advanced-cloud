package org.goodsence.messageserver.dispatcher.listener;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.common.utils.SerializedUtils;
import org.goodsence.messageserver.dispatcher.core.CustomMessage;
import org.goodsence.messageserver.dispatcher.service.ReactiveDispatchService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@Slf4j
@Component
public class MqListener {

    private final ReactiveDispatchService reactiveDispatchService;

    private final Scheduler scheduler;

    @Autowired
    public MqListener(ReactiveDispatchService reactiveDispatchService, Scheduler scheduler) {
        this.reactiveDispatchService = reactiveDispatchService;
        this.scheduler = scheduler;
    }

    @RabbitListener(queues = "#{messagesQueue.name}")
    public void rabbitMQReceive(Message message) {
        //todo 观察异步线程会不会线程安全
        Mono.fromCallable(() -> (CustomMessage) SerializedUtils.bytesToObject(message.getBody()))
                .subscribeOn(scheduler)
                .doOnError(throwable -> log.error("序列化发生异常: {}", throwable.getMessage(), throwable))
                .doOnSuccess(customMessage -> log.info("消息: {}", customMessage))
                .flatMap(reactiveDispatchService::dispatch)
                .doOnError(throwable -> log.error("传发消息异常: {}", throwable.getMessage(), throwable))
                .subscribe();
    }
}
