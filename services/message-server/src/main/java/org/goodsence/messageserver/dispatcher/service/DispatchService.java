package org.goodsence.messageserver.dispatcher.service;

import org.goodsence.messageserver.dispatcher.core.CustomMessage;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
public interface DispatchService {

    void dispatch(CustomMessage customMessage);
}
