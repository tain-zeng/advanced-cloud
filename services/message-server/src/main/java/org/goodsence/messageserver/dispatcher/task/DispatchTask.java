package org.goodsence.messageserver.dispatcher.task;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.messageserver.dispatcher.dao.MessageDao;
import org.goodsence.messageserver.dispatcher.service.ReactiveDispatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/31
 * @project advanced-cloud
 */
@Slf4j
@Component
public class DispatchTask {

    private final MessageDao messageDao;

    private final ReactiveDispatchService reactiveDispatchService;

    private final Scheduler scheduler;

    @Autowired
    public DispatchTask(MessageDao messageDao, ReactiveDispatchService reactiveDispatchService, Scheduler scheduler) {
        this.messageDao = messageDao;
        this.reactiveDispatchService = reactiveDispatchService;
        this.scheduler = scheduler;
    }

    @Scheduled(fixedRate = 5*60*1000)
    public void dispatch() {
        Mono.fromCallable(messageDao::selectUnDispatch)
                .subscribeOn(scheduler)
                .flatMapMany(Flux::fromIterable)
                .filter(defaultCustomMessage ->
                        StringUtils.hasText(defaultCustomMessage.getRecipient())
                                && StringUtils.hasText(defaultCustomMessage.getContent())
                )
                .doOnNext(defaultCustomMessage -> log.info("当前处理消息: {}", defaultCustomMessage))
                .flatMap(reactiveDispatchService::dispatch)
                .subscribe();
    }
}
