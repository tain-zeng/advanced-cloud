package org.goodsence.messageserver.route;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisOperations;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
@Slf4j
public class RedisRouteHolder implements RouteHolder {

    private final HashOperations<String, String, String> hashOps;

    private final RedisOperations<String, String> redisOps;

    private static final String WEB_SOCKET_ROUTE = "WEB_SOCKET_ROUTE";

    public RedisRouteHolder(RedisOperations<String, String> redisOps) {
        this.hashOps = redisOps.opsForHash();
        this.redisOps = redisOps;
    }

    @Override
    public void add(String username, String routeId) {
        hashOps.put(WEB_SOCKET_ROUTE, username, routeId);
    }

    @Override
    public String get(String username) {
        return hashOps.get(WEB_SOCKET_ROUTE, username);
    }

    @Override
    public void remove(String username) {
        hashOps.delete(WEB_SOCKET_ROUTE, username);
    }

    @Override
    public boolean contains(String username) {
        return Boolean.TRUE.equals(hashOps.hasKey(WEB_SOCKET_ROUTE, username));
    }

    @Override
    public int size() {
        return Math.toIntExact(hashOps.size(WEB_SOCKET_ROUTE));
    }

    @Override
    public void clear() {
        redisOps.delete(WEB_SOCKET_ROUTE);
    }
}
