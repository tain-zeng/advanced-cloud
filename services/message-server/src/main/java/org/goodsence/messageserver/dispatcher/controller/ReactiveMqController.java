package org.goodsence.messageserver.dispatcher.controller;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.messageserver.dispatcher.dao.domain.DefaultCustomMessage;
import org.goodsence.messageserver.dispatcher.service.MqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

/**
 * @author zengty
 * @apiNote 消息控制器
 * @date 2024/12/25
 * @project advanced-cloud
 */
@Slf4j
@RestController
@RequestMapping("/mq")
public class ReactiveMqController {

    private final MqService mqService;

    private final Scheduler scheduler;

    @Autowired
    public ReactiveMqController(MqService mqService, Scheduler scheduler) {
        this.mqService = mqService;
        this.scheduler = scheduler;
    }

    @PostMapping
    public Mono<Void> sendMessage(@RequestBody @Validated DefaultCustomMessage messageArgs) {
        return ReactiveSecurityContextHolder.getContext()
                //不能用doOnNext 因为可能没有执行完拿不到
                .map(securityContext -> securityContext.getAuthentication().getName())
                .doOnError(throwable -> log.error("获取登录用户出错: {}", throwable.getMessage(), throwable))
                .doOnSuccess(username -> log.info("登录用户: {}", username))
                .filter(StringUtils::hasText)
                .doOnSuccess(messageArgs::setSender)
                .subscribeOn(scheduler)
                //then 是一个操作执行完后,忽略第一个操作的结果，执行另一个操作
                .then(mqService.sendMessage(messageArgs));
    }
}
