package org.goodsence.messageserver.dispatcher.service.arg;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
@Data
public class SocketArgs {

    @NotBlank(message = "接收人[recipient]不能为空!")
    private String recipient;

    @NotBlank(message = "内容[content]不能为空")
    private String content;
}
