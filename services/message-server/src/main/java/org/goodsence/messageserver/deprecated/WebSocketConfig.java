//package org.goodsence.messageserver.deprecated;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.socket.WebSocketHandler;
//import org.springframework.web.socket.config.annotation.EnableWebSocket;
//import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
//import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
//import org.springframework.web.socket.server.HandshakeInterceptor;
//
///**
// * @author zty
// * @apiNote websocket配置类
// * @project_name the-revolt
// * @user tain
// * @create_at 2023/6/15 16:44
// * @create_vio IntelliJ IDEA
// */
//@Configuration
//@EnableWebSocket
//public class WebSocketConfig implements WebSocketConfigurer {
//
//    private final WebSocketHandler webSocketHandler;
//
//    private final HandshakeInterceptor handshakeInterceptor;
//
//    @Autowired
//    public WebSocketConfig(WebSocketHandler webSocketHandler, HandshakeInterceptor handshakeInterceptor) {
//        this.webSocketHandler = webSocketHandler;
//        this.handshakeInterceptor = handshakeInterceptor;
//    }
//
//    @Override
//    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
//        registry.addHandler(webSocketHandler, "/ws")
//                .addInterceptors(handshakeInterceptor)
//                .setAllowedOrigins("*");//允许跨域访问
//    }
//}
