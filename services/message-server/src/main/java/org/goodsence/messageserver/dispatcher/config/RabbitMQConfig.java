package org.goodsence.messageserver.dispatcher.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author zty
 * @apiNote rabbitmq配置
 * @user nav1c
 * @date 2023/12/14 10:58
 * @project advanced-cloud
 */
@Configuration
public class RabbitMQConfig {

    @Value("${spring.rabbitmq.template.messages-messagesQueue:messages}")
    private String messagesQueue;

    /**
     * 队列，如果不存在则会创建
     * @return 队列
     */
    @Bean
    public Queue messagesQueue() {
        return QueueBuilder.durable(messagesQueue).build();
    }
}
