package org.goodsence.messageserver.route;

/**
 * @author zengty
 * @apiNote 路由
 * @date 2024/12/29
 * @project advanced-cloud
 */
public interface CustomRoute {

    String getRoute();
}
