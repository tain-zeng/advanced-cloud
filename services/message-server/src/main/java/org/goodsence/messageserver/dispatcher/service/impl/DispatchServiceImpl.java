package org.goodsence.messageserver.dispatcher.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.messageserver.route.RouteHolder;
import org.goodsence.messageserver.dispatcher.core.CustomMessage;
import org.goodsence.messageserver.dispatcher.dao.MessageDao;
import org.goodsence.messageserver.dispatcher.dao.domain.DefaultCustomMessage;
import org.goodsence.messageserver.dispatcher.service.DispatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
@Slf4j
@Service
public class DispatchServiceImpl implements DispatchService {

    private final MessageDao messageDao;

    private final RouteHolder routeHolder;

    private final WebClient webClient;

    @Autowired
    public DispatchServiceImpl(MessageDao messageDao, RouteHolder routeHolder) {
        this.messageDao = messageDao;
        this.routeHolder = routeHolder;
        this.webClient = WebClient.builder().build();
    }

    @Override
    public void dispatch(CustomMessage customMessage) {
        if (customMessage instanceof DefaultCustomMessage defaultSocketMessage) {
            messageDao.insertSelective(defaultSocketMessage);
            String recipient = customMessage.getRecipient();
            String route = routeHolder.get(recipient);
            if (!StringUtils.hasText(route)){
                log.warn("没有路由，不在线: {}", route);
                return;
            }
            try {
                WebClient.ResponseSpec retrieve = webClient.post()
                        .uri(route)
                        .bodyValue(defaultSocketMessage)
                        .retrieve();
                retrieve.toBodilessEntity().subscribe(
                        voidResponseEntity -> {
                            if (!voidResponseEntity.getStatusCode().is2xxSuccessful()){
                                throw new IllegalStateException("发送消息失败: " + voidResponseEntity.getStatusCode());
                            }
                        }
                );
            }catch (Exception e){
                log.error("消息发送失败: {}", e.getMessage(), e);
            }
            return;
        }
        log.warn("Unsupported socket message: {}", customMessage);
    }
}
