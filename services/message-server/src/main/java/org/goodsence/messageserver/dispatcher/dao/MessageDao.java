package org.goodsence.messageserver.dispatcher.dao;

import org.goodsence.common.model.BaseDao;
import org.goodsence.messageserver.dispatcher.dao.domain.DefaultCustomMessage;

import java.util.List;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
public interface MessageDao extends BaseDao<String, DefaultCustomMessage> {
    List<DefaultCustomMessage> selectUnDispatch();
}
