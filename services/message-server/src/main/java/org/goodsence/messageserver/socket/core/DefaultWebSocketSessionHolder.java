package org.goodsence.messageserver.socket.core;

import org.goodsence.messageserver.route.CustomRoute;
import org.goodsence.messageserver.route.RouteHolder;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.security.oauth2.core.OAuth2TokenIntrospectionClaimNames;
import org.springframework.util.Assert;
import org.springframework.web.reactive.socket.WebSocketSession;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/25
 * @project advanced-cloud
 */
public class DefaultWebSocketSessionHolder implements WebSocketSessionHolder, DisposableBean {

    private final Map<String, WebSocketSession> sessions;

    private final RouteHolder routeHolder;

    private final String route;

    private final int maxSize;

    public DefaultWebSocketSessionHolder(RouteHolder routeHolder, CustomRoute customRoute) {
        this(routeHolder, customRoute, Math.toIntExact(Math.round(Math.pow(2, 16))));
    }

    public DefaultWebSocketSessionHolder(RouteHolder routeHolder, CustomRoute customRoute, int maxSize) {
        Assert.state(maxSize > 0, "maxSize must be greater than 0");
        this.sessions = new ConcurrentHashMap<>(maxSize, 0.75f);
        this.route = customRoute.getRoute();
        this.routeHolder = routeHolder;
        this.maxSize = maxSize;
    }

    @Override
    public void add(WebSocketSession session) {
        String id = fetchId(session);
        if(contains(id)) {
            return;
        }
        if (size() >= maxSize){
            throw new IllegalStateException("Maximum size of " + maxSize + " reached");
        }
        sessions.put(id, session);
        routeHolder.add(id, route);
    }

    @Override
    public Supplier<String> idFetcher(WebSocketSession session) {
        return () -> session.getAttributes().get(OAuth2TokenIntrospectionClaimNames.USERNAME).toString();
    }

    @Override
    public void remove(String id) {
        sessions.remove(id);
        routeHolder.remove(id);
    }

    @Override
    public WebSocketSession findSession(String id) {
        if (!contains(id)){
            return null;
        }
        return sessions.get(id);
    }

    @Override
    public WebSocketSession findOpenedSession(String id) {
        WebSocketSession session = findSession(id);
        if (session == null) {
            return null;
        }
        return session.isOpen() ? session : null;
    }

    @Override
    public boolean contains(String id) {
        return sessions.containsKey(id);
    }

    @Override
    public Collection<WebSocketSession> getSessions() {
        return sessions.values();
    }

    @Override
    public int size() {
        return sessions.size();
    }

    @Override
    public void clear() {
        sessions.clear();
    }

    @Override
    public void destroy() {
        sessions.values().forEach(WebSocketSession::close);
        sessions.keySet().forEach(routeHolder::remove);
        sessions.clear();
    }
}
