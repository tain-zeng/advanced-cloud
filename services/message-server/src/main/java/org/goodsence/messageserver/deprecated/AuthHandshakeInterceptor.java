//package org.goodsence.messageserver.deprecated;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.server.ServerHttpRequest;
//import org.springframework.http.server.ServerHttpResponse;
//import org.springframework.lang.NonNull;
//import org.springframework.security.core.AuthenticatedPrincipal;
//import org.springframework.security.oauth2.core.OAuth2TokenIntrospectionClaimNames;
//import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionException;
//import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
//import org.springframework.util.CollectionUtils;
//import org.springframework.web.socket.WebSocketHandler;
//import org.springframework.web.socket.server.HandshakeInterceptor;
//
//import java.io.IOException;
//import java.io.OutputStream;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author zty
// * @apiNote
// * @user nav1c
// * @date 2023/11/26 17:39
// * @project advanced-cloud
// */
//@Slf4j
//public class AuthHandshakeInterceptor implements HandshakeInterceptor {
//
//    private final OpaqueTokenIntrospector opaqueTokenIntrospector;
//
//    public AuthHandshakeInterceptor(OpaqueTokenIntrospector opaqueTokenIntrospector) {
//        this.opaqueTokenIntrospector = opaqueTokenIntrospector;
//    }
//
//    /**
//     * 握手之前的操作
//     * @param request ServerHttpRequest
//     * @param response ServerHttpResponse
//     * @param wsHandler WebSocketHandler
//     * @param attributes attributes
//     * @return 是否握手成功
//     */
//    @Override
//    public boolean beforeHandshake(@NonNull ServerHttpRequest request, @NonNull ServerHttpResponse response,
//                                   @NonNull WebSocketHandler wsHandler, @NonNull Map<String, Object> attributes) {
//        //这里可以放置业务鉴权业务处理
//        log.info("准备握手-------");
//        final List<String> keys = request.getHeaders().get("token");//不能用Authorization, 会导致鉴权报错
//        if (CollectionUtils.isEmpty(keys)){
//            return false;
//        }
//        final String token = keys.get(0);
//        AuthenticatedPrincipal principal;
//        try {
//            principal = opaqueTokenIntrospector.introspect(token);
//        }catch (OAuth2IntrospectionException e){
//            response.setStatusCode(HttpStatus.UNAUTHORIZED);
//            return false;
//        }
//        if (principal == null){
//            response.setStatusCode(HttpStatus.UNAUTHORIZED);
//            return false;
//        }
//        String principalName = principal.getName();
//        if (principalName == null){
//            response.setStatusCode(HttpStatus.UNAUTHORIZED);
//            return false;
//        }
//
//        //注意，这个动作把认证信息放入了attributes;
//        attributes.put(OAuth2TokenIntrospectionClaimNames.USERNAME, principalName);
//        return true;
//    }
//
//    @Override
//    public void afterHandshake(@NonNull ServerHttpRequest request, @NonNull ServerHttpResponse response,
//                               @NonNull WebSocketHandler wsHandler, Exception exception) {
//        if (exception == null) {
//            log.info("握手成功！");
//            return;
//        }
//        final String failReason = "握手失败！原因:" + exception.getMessage();
//        log.warn(failReason);
//        response.setStatusCode(HttpStatus.BAD_REQUEST);
//        try(OutputStream out = response.getBody()) {
//            out.write(exception.getMessage().getBytes());
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
//}
