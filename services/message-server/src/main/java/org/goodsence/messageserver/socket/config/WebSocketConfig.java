package org.goodsence.messageserver.socket.config;

import org.goodsence.messageserver.route.CustomRoute;
import org.goodsence.messageserver.route.RouteHolder;
import org.goodsence.messageserver.socket.core.DefaultWebSocketHandler;
import org.goodsence.messageserver.socket.core.DefaultWebSocketSessionHolder;
import org.goodsence.messageserver.socket.core.WebSocketSessionHolder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import reactor.core.scheduler.Scheduler;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
public class WebSocketConfig {

    @Bean
    public WebSocketSessionHolder webSocketSessionHolder(RouteHolder routeHolder, CustomRoute customRoute){
        return new DefaultWebSocketSessionHolder(routeHolder, customRoute);
    }

    @Bean
    public WebSocketHandler webSocketHandler(WebSocketSessionHolder webSocketSessionHolder, Scheduler scheduler){
        return new DefaultWebSocketHandler(webSocketSessionHolder, scheduler);
    }

    @Bean
    public HandlerMapping webSocketHandlerMapping(WebSocketHandler myWebSocketHandler) {
        Map<String, WebSocketHandler> map = new HashMap<>();
        map.put("/wss", myWebSocketHandler);
        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
        handlerMapping.setUrlMap(map);
        handlerMapping.setOrder(1);
        return handlerMapping;
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }
}
