//package org.goodsence.messageserver.deprecated;
//
//import org.springframework.web.socket.WebSocketSession;
//
//import java.util.Collection;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.function.Supplier;
//
///**
// * @author zengty
// * @apiNote
// * @date 2024/12/25
// * @project advanced-cloud
// */
//public class DefaultWebSocketSessionHolder implements WebSocketSessionHolder {
//
//    private final Map<String, WebSocketSession> sessions;
//
//    private final RouteHolder routeHolder;
//
//    private final String route;
//
//    public DefaultWebSocketSessionHolder(RouteHolder routeHolder, CustomRoute customRoute) {
//        this.routeHolder = routeHolder;
//        this.sessions = new ConcurrentHashMap<>();
//        this.route = customRoute.getRoute();
//    }
//
//    @Override
//    public void add(WebSocketSession session) {
//        String id = fetchId(session);
//        sessions.put(id, session);
//        routeHolder.add(id, route);
//    }
//
//    @Override
//    public Supplier<String> idFetcher(WebSocketSession session) {
//        return () -> session.getAttributes().get("username").toString();
//    }
//
//    @Override
//    public void remove(String id) {
//        sessions.remove(id);
//        routeHolder.remove(id);
//    }
//
//    @Override
//    public WebSocketSession findSession(String id) {
//        if (!contains(id)){
//            return null;
//        }
//        return sessions.get(id);
//
//    }
//
//    @Override
//    public WebSocketSession findOpenedSession(String id) {
//        WebSocketSession session = findSession(id);
//        if (session == null) {
//            return null;
//        }
//        return session.isOpen() ? session : null;
//    }
//
//    @Override
//    public boolean contains(String id) {
//        boolean b = sessions.containsKey(id);
//        boolean b1 = routeHolder.contains(id);
//        return b && b1;
//    }
//
//    @Override
//    public Collection<WebSocketSession> getSessions() {
//        return sessions.values();
//    }
//
//    @Override
//    public int size() {
//        return sessions.size();
//    }
//
//    @Override
//    public void clear() {
//        sessions.clear();
//    }
//}
