package org.goodsence.messageserver.dispatcher.service;

import org.goodsence.messageserver.dispatcher.dao.domain.DefaultCustomMessage;
import reactor.core.publisher.Mono;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/26
 * @project advanced-cloud
 */
public interface MqService {
    Mono<Void> sendMessage(DefaultCustomMessage messageArgs);
}
