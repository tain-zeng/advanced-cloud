package org.goodsence.messageserver.dispatcher.dao.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.goodsence.common.model.StringKeyEntity;
import org.goodsence.messageserver.dispatcher.core.CustomMessage;

import java.io.Serial;
import java.util.Objects;

/**
 * @author zty
 * @apiNote
 * @user nav1c
 * @date 2023/12/10 19:18
 * @project advanced-cloud
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DefaultCustomMessage extends StringKeyEntity implements CustomMessage {

    @Serial
    private static final long serialVersionUID = 122L;

    private String recipient;

    private String content;

    public void setSender(String sender){
        setCreatedBy(sender);
    }

    @Override
    public String getSender(){
        return getCreatedBy();
    }

    @Override
    public boolean isReceived() {
        return Objects.equals(8, getStatus());
    }
}
