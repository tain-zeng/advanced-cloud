package org.goodsence.messageserver.socket.controller.arg;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/29
 * @project advanced-cloud
 */
@Data
public class SocketArgs {

    @NotBlank(message = "接收人不能为空")
    private String recipient;

    @NotBlank(message = "发送内容不能为空")
    private String content;
}
