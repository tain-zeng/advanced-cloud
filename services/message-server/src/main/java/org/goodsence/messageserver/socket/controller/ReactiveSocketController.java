package org.goodsence.messageserver.socket.controller;

import lombok.extern.slf4j.Slf4j;
import org.goodsence.messageserver.socket.controller.arg.SocketArgs;
import org.goodsence.messageserver.socket.core.WebSocketSessionHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

/**
 * @author zengty
 * @apiNote
 * @date 2024/12/30
 * @project advanced-cloud
 */
@Slf4j
@RequestMapping("/sockets")
@RestController
public class ReactiveSocketController {

    private final WebSocketSessionHolder webSocketSessionHolder;

    private final Scheduler scheduler;

    @Autowired
    public ReactiveSocketController(WebSocketSessionHolder webSocketSessionHolder, Scheduler scheduler) {
        this.webSocketSessionHolder = webSocketSessionHolder;
        this.scheduler = scheduler;
    }

    @PostMapping
    public Mono<Void> sendMessage(@RequestBody @Validated SocketArgs socketArgs){
        return Mono.fromCallable(() -> webSocketSessionHolder.findOpenedSession(socketArgs.getRecipient()))
                .subscribeOn(scheduler)
                .doOnError(throwable -> log.error("获取消息失败: {}", throwable.getMessage(), throwable))
                .doOnSuccess(session -> log.info("session内容: {}", session))
                .switchIfEmpty(Mono.error(new IllegalStateException("session 不可用!")))
                //flatMap
                .flatMap(session -> session.send(Mono.just(session.textMessage(socketArgs.getContent()))))
                .doOnError(throwable -> log.error("socket消息发送失败: {}", throwable.getMessage(), throwable));
    }
}
